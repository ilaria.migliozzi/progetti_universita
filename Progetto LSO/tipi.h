/*===============================================================COSTANTI=======================================================================================*/
#define RIGHE 21    /* Dimensione x Mappa*/
#define COLONNE 21  /* Dimensione y Mappa*/
#define FILEUTENTI "Utenti.txt" /* Nome del file degli utenti */
#define TITLELEN 31
#define BODYLEN 1025
#define STRINGLEN 2050
/* ============ STRUTTURE DATI ================================= */


struct argomenti_thread_t /* Struttura che contiene tutti gli argomenti da passare alla funzione test*/
{
    int* nuovo_socket_p;
    char ip_client[50];
};


struct user /*Struttura degli utenti. Se connection_status==1 => l'utente è collegato*/
{           /*Gli elementi presenti nella struttura sono elementi del file login.txt*/
	int id;
        int raggio;
        int connection_status;
        int x,y;
        char nickname[30];
        char password[30];
        struct notifica_richiesta* richiesta;
        struct notifica_risposta* risposta; 
};

struct idusers /*Struttura che indica quali utenti hanno avuto il permesso di leggere un messaggio privato (visibility==0)*/
{
	int id;
	struct idusers *next;
};

struct message{ /*Struttura dei messaggi geolocalizzati. Il singolo messaggio è privato se visibility==0*/
	int idmsg;
        char titolo[TITLELEN];
        char corpo[BODYLEN];
	time_t data_creazione;
	int visibility;
        int x,y;
        int indice_creatore;
	struct idusers *lettori;
	struct message *next;
};


/* La struct RISPOSTA contiene puntatori a tutti i tipi di dato ed un indicatore del tipo
   in questo modo Ã¨ possibile passare come risposta sempre la stessa struttura impostando a NULL
   i campi non relativi al tipo di dato che realmente si vuole trasmettere*/
struct risposta
{
	int tipoRisposta; // 1=STRINGA 2=CARATTERE 3=UTENTE 4=MESSAGGIO 5=LETTORE 6=LOGIN -1=ERRORE
	char stringa[STRINGLEN];
	char carattere;
	struct user utente;
	struct message messaggio;
	struct idusers lettore;
};


/* Struttura Elementare della cella matrice */
struct cella {

 pthread_mutex_t mutexcella;

 struct message* headmsg;
 struct idusers* headuser;

};

struct msgclient{
 int id;
 int x;
 int y;
 int id_utente;
 char titolo[TITLELEN];
 struct msgclient* next;
};

// Struttura delle lista notifiche 
struct notifica {

 int iddest;
 int idmit;
 int idmsg;
 struct notifica* next;

};


//Struttura delle liste contenente le notifiche da rispondere
struct notifica_richiesta{
  int id_mittente; //id dell'utente che ha chiesto di poter leggere il messaggio
  int id_messaggio;
  int coord_x;
  int coord_y;
  char titolo[TITLELEN];
  struct notifica_richiesta* next;
};

//Struttura notifica da leggere
struct notifica_risposta{
 int id_creatore; //id dell'utente che ha creato il messaggio, e che mi ha accettato/rifiutato la possibilità di lettura
 char nick_creatore[31];
 char titolo[TITLELEN];
 char ris;
 struct notifica_risposta* next;
};


struct notifica_client{
  int num;
  char titolo[TITLELEN];
  char nick[31];
  int creatore_msg;
  int coord_x;
  int coord_y;
  int id_mess;
  struct notifica_client* next;
};
