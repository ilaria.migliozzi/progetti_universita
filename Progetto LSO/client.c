#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>  /* in per "sockaddr_in" */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <wait.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/select.h> /* Per la select */

#include"tipi.h"  /* nostro header */

/*===========================VARIABILI GLOBALI=============================*/
int socketdes;

/*===================================== FUNZIONI===========================*/

void chiudi_socket();
int crea_socket(char* destinazione, int porta);
void spedisci_messaggio(struct risposta Messaggio);
void inserisci_messaggio();
void leggi_messaggio();
void leggi_messaggi_inviati();
void leggi_messsaggio_privato();
int cerca_messaggio(struct msgclient* app,int id,int *x,int *y,char titolo []);
struct msgclient* inserisci_msgclient(struct msgclient* testa,int id,int coord_x,int coord_y,int id_utente,char titolo[]);
struct notifica_client* inserisci_notifica(struct notifica_client* lista,char titolo[],char nick[],int i,int crea,int coord_x,int coord_y,int id_mess);
int cerca_notifica(struct notifica_client* lista,int id,char titolo[],int* id_creatoremsg,int* coord_x,int* coord_y,int* id_mess);
struct msgclient* cancella_messaggio(struct msgclient* testa,int id);
void ricevi_messaggio(struct risposta *risp);
void mostra_maschera_login();
void login();
void registrazione();
void mostra_maschera_menu();
void distruggi_lista(struct msgclient* testa);
void distruggi_lista_notifica(struct notifica_client* testa);
void rispondi_messaggi();
void elenca_notifiche();

void disconnetti(int sig);
void clean_up();

/*====================================== MAIN ===============================*/
int main(int argc,char* argv[])
{
  sigset_t insime_segnali;
  
  /* Controllo numero di parametri */
  if ( argc != 3 ) printf("Sintassi Corretta:  ./client.o IP Porta\n"), exit(1);

  /* Controllo segnali */
  
  sigfillset(&insime_segnali);       /* Inizializzo l'insime dei segnali con l'insime UNIVERSO */
  sigdelset(&insime_segnali,SIGHUP); /* Escludo dall'insieme dei segnali SIGHUP,SIGINT,SIGPIPE */
  sigdelset(&insime_segnali,SIGINT); 
  sigdelset(&insime_segnali,SIGPIPE);
  sigprocmask(SIG_SETMASK,&insime_segnali,NULL); /* Setto come insieme dei segnali bloccati "insime_segnali"*/
  signal(SIGHUP,disconnetti);
  signal(SIGINT,disconnetti); 
  signal(SIGPIPE,SIG_IGN);
  
  
  /* Creazione socket connect */
  socketdes=crea_socket(argv[1],atoi(argv[2]));
  
  /* Visualizzazione maschera del Login */
  mostra_maschera_login();

  /* Visualizzazione maschera menu */
  mostra_maschera_menu();

  /* Chiudo il socket. */
  chiudi_socket();

  return 0;
}

/*===================================END MAIN =======================================================*/


/*================================== FUNZIONI =======================================================*/


void ricevi_messaggio(struct risposta *risp){
	int quanti;
	
	int ret;
        struct timeval timeout;
        fd_set set; /* Insieme dei file descriptor che la select deve deve monitorare */
        
        FD_ZERO(&set);        /* Iniziallizza l'insime dei file descriptor */
        FD_SET(socketdes, &set); /* Aggiungo il socket da controllare nell'insieme */

        timeout.tv_sec = 3; /* aspetto 180 sec(3 min) e 0 millisecondi*/
        timeout.tv_usec = 0; /* millisecondi*/
        
        ret = select(socketdes+1, &set, NULL, NULL,&timeout);
        if(ret == -1)/* Se è avvenuto un errore disconnetto il client*/
        { 
           printf("Impossibile comunicare con il server riprovare in seguito...\n");
           clean_up();
        }
        else if(ret == 0) /* Se il tempo è scatuto disconnetto il client */
        {
          printf("Impossibile comunicare con il server riprovare in seguito...\n");
          clean_up();
        }
          
        else /* E' stato scritto qualcosa nella socket */
        {

	  if ((quanti=read(socketdes,risp,sizeof(struct risposta)))<0)
	  {
	      printf("Impossibile comunicare con il server riprovare in seguito...\n");
	      clean_up();
	  }
	}
}

/*----------------------------------------------------------------------------------------------------*/
void chiudi_socket()
{
  close(socketdes);
  return;
}

/*----------------------------------------------------------------------------------------------------*/

int crea_socket(char* destinazione, int porta)
{
  struct sockaddr_in temp; 
  
  /* Impostazione indirizzi */
  temp.sin_family=AF_INET;
  temp.sin_port=htons(porta);
  inet_aton(destinazione,&temp.sin_addr);

  /* Creazione socket. */
  socketdes=socket(AF_INET,SOCK_STREAM,0);
 
  if ( (connect(socketdes, (struct sockaddr*) &temp, sizeof(temp)) ) < 0 ) 
	{
		printf("Errore nella connect..\n");
		exit(1);
	}
  return socketdes;
}

/*----------------------------------------------------------------------------------------------------*/

void spedisci_messaggio(struct risposta messaggio)
{
  int ret;
  if ((ret=write(socketdes,&messaggio,sizeof(struct risposta)))<=0)
  {
    printf("Impossibile comunicare con il server riprovare in seguito...\n");
    chiudi_socket();
    exit(1);
  }  
  return;
}

/******************************************************************************************************
******************************************************************************************************/
 void inserisci_messaggio(){
  
  char buffer[2048];
  char vis,r;
  int letti;
  struct risposta risp;
  
  
/*Richiedo all'utente di inserire il titolo del messaggio*/  
  r='n';
  do{
  
    buffer[0]='\0';
    risp.messaggio.titolo[0]='\0';
    
    if(r=='n')
    system("clear");
    printf("Inserisci il titolo del messaggio: [MAX 30 caratteri]\n");
    if((letti=read(STDIN_FILENO,buffer,2048))<0){
     //VEDI COSA FARE IN CASO DI ERRORE
    }
  
    if(letti>TITLELEN-1)
      letti=TITLELEN-1;
      
    strncpy(risp.messaggio.titolo,buffer,letti);
    risp.messaggio.titolo[letti]='\0';    
    printf("Vuoi inserire un messaggio con questo titolo?[s/n]\n%s\n",risp.messaggio.titolo);
    scanf("\n%c",&r);
    
   }while(r!='s'); 
     


  r='n';
  do{
  
    buffer[0]='\0';
    risp.messaggio.corpo[0]='\0';
    
    if(r=='n')
    system("clear");
    printf("Inserisci il corpo del messaggio: [MAX 1024 caratteri]\n");
    if((letti=read(STDIN_FILENO,buffer,2048))<0){
     //VEDI COSA FARE IN CASO DI ERRORE
    }

    if(letti>BODYLEN-1)
      letti=BODYLEN-1;
  
    strncpy(risp.messaggio.corpo,buffer,letti);
    risp.messaggio.corpo[letti]='\0';    
    printf("Vuoi inserire un messaggio con questo corpo?[s/n]\n%s\n",risp.messaggio.corpo);
    scanf("\n%c",&r);
    
   }while(r!='s'); 
     


    
  printf("\nVuoi che il messaggio sia visibile a tutti? [s/n]: ");
  do{
    scanf("\n%c",&vis);
    
    if(vis!='s' & vis!='n')
      printf("\nVuoi che il messaggio sia visibile a tutti? [s/n]:");
  }while(vis!='s' & vis!='n');

  if(vis=='s')
    risp.messaggio.visibility=1;
  else
    risp.messaggio.visibility=0;

  spedisci_messaggio(risp);
  
}
 
 
 
/*----------------------------------------------------------------------------------------------------*/

void mostra_maschera_login()
{      
        int r;
	struct risposta richiesta;
	// Cicla finché non avviene una scelta compresa tra 1 e 3
	do
	{
		printf("\033[H\033[2J"); //pulire la schermata
		printf("----------------------------------------------------------------\n");
		printf(" 	   ENTRA NEL SERVIZIO DI SCAMBIO MESSAGGI	        \n");
		printf("----------------------------------------------------------------\n"); 
		printf("1- REGISTRATI\n");
		printf("2- ACCEDI\n");
		printf("3- ESCI\n");
		printf("SCELTA : ");

             
		if(scanf("\n%d",&r)!=1){
		  printf("Inserire un'intero!\n");
		  getchar();
		  r=4;
		}
	

	switch ( r )
	{
		case 1:
		        richiesta.carattere='r';
		        spedisci_messaggio(richiesta);
			registrazione();
			login();
		  break;

		case 2:
		        richiesta.carattere='l';
		        spedisci_messaggio(richiesta);
			login();
		  break;

		case 3:
			raise(SIGINT);
		  break;
		default:
		  
		  break;
	}
   }while(r<1 | r>3);	
}


/*----------------------------------------------------------------------------------------------------*/
// Procedura che processa la richiesta di registrazione mostrando la maschera adeguata
/*----------------------------------------------------------------------------------------------------*/
void registrazione ()
{
	struct risposta risp;
	char flag;
	char string[62];
	char dati[31];
	printf("\033[H\033[2J"); //puliamo la schermata

	do
	{

		
		printf("----------------------------------------------------------------\n");
		printf("			REGISTRAZIONE				\n");
		printf("----------------------------------------------------------------\n");
		
		printf("- Username: \n");
		do{
		  dati[0]='\0';
		  scanf("%s",dati);
		  if(strpbrk(dati,"$#")!=NULL){ /*Controllo che il nickname inserito non contenga i caratteri $ o #*/
		      printf("Inserisci un Username che non contenga i caratteri $ o #\n");
		  }
		}while(strpbrk(dati,"$#")!=NULL);

		string[0]='\0';		
                strcat(string,dati); 
		strcat(string,"$");
		
		printf("- Password: \n");
		do{
		  dati[0]='\0';
		  scanf("%s",dati);
		  if(strpbrk(dati,"$#")!=NULL){ /*Controllo che il nickname inserito non contenga i caratteri $ o #*/
		      printf("Inserisci una Password che non contenga i caratteri $ o #\n");
		  }
		}while(strpbrk(dati,"$#")!=NULL);
	
                strcat(string,dati); 
		strcat(string,"$");

		printf("Stringa da spedire %s \n",string);
		
		strcpy(risp.stringa,string);

		printf("Confermi i dati inseriti? s/n \n");
		do
		{
			scanf("\n%c",&flag);
                	if(flag!= 's' & flag!= 'n')
			   printf("Digita Soltanto 's' o 'n'\n");
		}
		while ( flag != 's' & flag != 'n'  );

		// Se L'utente conferma i dati inseriti invia il messaggio.
		if ( flag!='n' )
		{
			spedisci_messaggio(risp);

			printf("\nAttendere...\n\n");
			sleep(1);

			ricevi_messaggio(&risp);

			if ( risp.carattere == 'e' )
			{
				printf("ERRORE: utente gia' esistente!\n");
				flag='n';
			}
			else if(risp.carattere=='a')
			{
				printf("Registrazione completata, attendi per effettuare il login...\n");
				sleep(1);
			}else
			   if(risp.carattere=='r'){
			       printf("Impossibile effettuare la registrazione\n");
			       flag='s';
			       sleep(1);
			   }
		}
		else
		  printf("\033[H\033[2J"); //puliamo la schermata

	}while ( flag != 's' );
}



/*----------------------------------------------------------------------------------------------------*/
// 
/*----------------------------------------------------------------------------------------------------*/
void cambia_posizione()
{
	char flag=' ';
	struct risposta risp;

        printf("CLIENT HA RICHIESTO DI CAMBIARE POSIZIONE\n");
	sleep(2);
	system("clear");

	printf("- E' possibile muoversi solo di una cella -\n\n");
	printf("- UP: u\n");
	printf("- DOWN: d\n");
	printf("- RIGHT: r\n");
	printf("- LEFT: l\n");
	printf("Comando: ");

	do
	{
		scanf("\n%c",&flag);
		if(flag!= 'u' & flag!= 'd' & flag!= 'r' & flag!= 'l')
		printf("\nDigita Soltanto le lettere indicate\n");
	}
	while ( flag!= 'u' & flag!= 'd' & flag!= 'r' & flag!= 'l'  ) ;

	risp.carattere=flag;
	risp.tipoRisposta=2;

	spedisci_messaggio(risp);

	printf("Attendere..\n");
	sleep(2);
}


/*----------------------------------------------------------------------------------------------------*/
// CLEAN UP - Procedura che chiude il socket e termina il client
/*----------------------------------------------------------------------------------------------------*/
void clean_up()
{
  chiudi_socket();
  printf("Client Terminato...\n");
  exit(1);
}



/*******************************************************************************************************
* Funzione che mostra la maschera del menù principale                                                  *
********************************************************************************************************/
void mostra_maschera_menu(){

  char ris;
  struct risposta comando;
  int flag;
  do{

  printf("*************************************************************************\n"
         "*                          MENU PRINCIPALE                              *\n"
         "*                                                                       *\n"
         "* a. Visualizza Utenti collegati nel tuo raggio d'azione                *\n" 
         "* b. Invia un messaggio Geolocalizzato nella tua posizione              *\n" 
         "* c. Cancella un messaggio Geolocalizzato nel tuo raggio d'azione       *\n"
         "* d. Leggi messaggi Pubblici e Autorizzati nel tuo raggio d'azione      *\n"
         "* e. Elenca messaggi Privati nel tuo raggio d'azione                    *\n"
         "* f. Controlla notifiche                                                *\n"
         "* g. Cambia posizione                                                   *\n" 
         "* x. Logout                                                             *\n" 
         "*************************************************************************\n");
   do{
     
     scanf("\n%c",&ris);
         if((ris< 'a' || ris> 'g') & ris!='x')
           printf("Comando Errato!\n Inserire caratteri [a-g]\n");
     }while((ris< 'a' || ris> 'g') & ris!='x');

   
   comando.tipoRisposta=2;
   comando.carattere=ris;

   spedisci_messaggio(comando);

   switch(ris){
    
   case 'a':/*CLIENT HA RICHIESTO DI VISUALIZZARE GLI UTENTI*/
          printf("GLI UTENTI NEL TUO RAGGIO D'AZIONE SONO:\n");
          do{
           
            ricevi_messaggio(&comando);

            if(comando.tipoRisposta==1 && strcmp(comando.stringa,"FINE")!=0)
              printf("%s",comando.stringa);

          }while(strcmp(comando.stringa,"FINE")!=0);
        
           break;
   case 'b': /*CLIENT HA RICHIESTO DI INVIARE UN MESSAGGIO*/
          inserisci_messaggio();
          ricevi_messaggio(&comando);
          if(comando.tipoRisposta==2){
             if(comando.carattere=='k')
               printf("\nErrore: Messaggio non inserito\n");
             else
               if(comando.carattere=='o')
                printf("\nMessaggio inserito con successo\n");
          }
          break;
   case 'c':/*CLIENT HA RICHIESTO DI CANCELLARE UN MESSAGGIO*/
            leggi_messaggi_inviati();
          break;
   case 'd':/*CLIENT HA RICHIESTO DI LEGGERE UN MESSAGGIO PUBLICO/PRIVATO AUTORIZZATO*/
          system("clear"); 
          leggi_messaggio();
          break;
   case 'e':
          /*CLIENT HA RICHIESTO DI LEGGERE UN MESSAGGIO PRIVATO NON AUTORIZZATO*/
          system("clear");
          leggi_messsaggio_privato();
   
        break;
   case 'f':/*CLIENT HA RICHIESTO DI CONTROLLARE LE NOTIFICHE*/
           system("clear");
            printf("*************************************************************************\n");
            printf("                              MENU NOTIFICHE                             \n");
            printf("l. Leggi notifiche dei messaggi autorizzati/rifiutati                    \n");
            printf("r. Rispondi a richieste di lettura dei tuoi messaggi privati             \n");
            printf("x. Torna al menù precendente                                             \n");
            printf("*************************************************************************\n");
            
            do{
            scanf("\n%c",&ris);
            if((ris!= 'l' & ris!= 'r') & ris!='x')
              printf("Comando Errato!\n Inserire caratteri l,r o x\n");
            }while((ris!= 'l' & ris!= 'r') & ris!='x');
            
            comando.tipoRisposta=2;
            comando.carattere=ris;

            spedisci_messaggio(comando);            
            
            switch(ris){
               case 'l':/*L'UTENTE HA RICHIESTO DI LEGGERE LE NOTIFICHE ACCETTATE/RIFIUTATE*/
                       elenca_notifiche();
                  break;
               case 'r':
                        rispondi_messaggi();
            
                 break;
               case 'x':
                        ris='q';
                  break;
               default:
                  break;
            }
            
        break;
   case 'g':
          /*CLIENT HA RICHIESTO DI CAMBIARE POSIZIONE*/
          cambia_posizione();
          break;
   case 'x': /*CLIENT HA RICHIESTO DI FARE IL LOGOUT*/
          printf("\nLOGOUT EFFETTUATO!\n");
          
          break;
   default:
          break;  
       
      }    
   
  }while(ris!='x');
}

/***********************************************************************************************/
/***********************************************************************************************/
void leggi_messaggio(){
  char titolo[TITLELEN],corpo[BODYLEN],nick[30],visib[25];
  int coord_x,coord_y,id_mess,vis,cont=0;
  struct risposta comando;
  char data[100];
  printf("I Messaggi abilitati alla lettura nel tuo raggio d'azione sono:\n");

       do{
           
        ricevi_messaggio(&comando);/*Ricevuto una stringa contenente: VISIBILITA'_MESSAGGIO$ID_MESSAGGIO$COORD_X$COORD_Y$TITOLO_MESSAGGIO$NICKNAME_UTENTE$CORPO 
                                                                                                                                                  $DATA_CREAZIONE*/

        if(comando.tipoRisposta==1 && strcmp(comando.stringa,"FINE")!=0){
               
           cont=1;
           vis=atoi(strtok(comando.stringa,"$"));
           id_mess=atoi(strtok(NULL,"$"));
           coord_x=atoi(strtok(NULL,"$"));
	   coord_y=atoi(strtok(NULL,"$"));
	       
	   titolo[0]='\0';
	   nick[0]='\0';
	   corpo[0]='\0';
	   data[0]='\0';
           strcpy(titolo,strtok(NULL,"$"));
           strcpy(nick,strtok(NULL,"$"));
           strcpy(corpo,strtok(NULL,"$"));
           strcpy(data,strtok(NULL,"$"));
               
           if(vis==1)
             strcpy(visib,"PUBBLICO");
           else
             strcpy(visib,"PRIVATO AUTORIZZATO");
               
           printf("\n*********************************************************\n");
           printf("ID:%d\nAUTORE: %s -  COORDINATE: [%d,%d] - DATA:%s\nTITOLO:%s[%s]\nCORPO:\n%s ",id_mess,nick,coord_x,coord_y,data,titolo,visib,corpo);
           printf("*********************************************************\n"); 
               
       }

     }while(strcmp(comando.stringa,"FINE")!=0);   
     
     if(cont==0)
      printf("Non ci sono messaggi abilitati alla lettura nel tuo raggio di azione\n");
     
}     


/***********************************************************************************************/
/***********************************************************************************************/
void leggi_messsaggio_privato()
{
  
  char titolo[TITLELEN],nick[30];
  int coord_x,coord_y,id_mess,id_utente;
  struct risposta comando;
  int cont=0,scelta;
  struct msgclient* testa=NULL;
  char data[100];
  
  printf("I Messaggi PRIVATI nel tuo raggio d'azione sono:\n");

       do{
           
        ricevi_messaggio(&comando);/*Ricevuto una stringa contenente: ID_MESSAGGIO$COORD_X$COORD_Y$TITOLO_MESSAGGIO$NICKNAME_UTENTE$DATA_CREAZIONE$ID_UTENTE  */

        
        if(comando.tipoRisposta==1 && strcmp(comando.stringa,"FINE")!=0){
               
           cont=1;
           id_mess=atoi(strtok(comando.stringa,"$"));
           coord_x=atoi(strtok(NULL,"$"));
	   coord_y=atoi(strtok(NULL,"$"));   
	   titolo[0]='\0';
	   nick[0]='\0';
	   data[0]='\0';
           strcpy(titolo,strtok(NULL,"$"));
           strcpy(nick,strtok(NULL,"$"));
           strcpy(data,strtok(NULL,"$")); 
           id_utente=atoi(strtok(NULL,"$"));
           testa=inserisci_msgclient(testa,id_mess,coord_x,coord_y,id_utente,titolo); /* Inserisco le informazione del messaggio in una lista locale */
               
           printf("\n*********************************************************\n");
           printf("ID:%d\nAUTORE: %s -  COORDINATE: [%d,%d] - DATA:%s\nTITOLO:%s",id_mess,nick,coord_x,coord_y,data,titolo);
           printf("*********************************************************\n"); 
               
       }

     }while(strcmp(comando.stringa,"FINE")!=0);
     
     if(cont==0)
     {
       printf("Non ci sono messaggi PRIVATI nel tuo raggio di azione\n");
       sprintf(comando.stringa,"Annulla");
       spedisci_messaggio(comando);       
       return;
     }
     
     cont=1;
     do
     {
        printf("Digita l'ID del messaggio PRIVATO di cui vuoi richiedere la lettura (Digita -1 per tornare al menù pricipale)\n");
        scanf("\n%d",&scelta);
        if((scelta==-1)|(cerca_messaggio(testa,scelta,&coord_x,&coord_y,titolo)==1))
          cont=0;
        else
         printf("Comando non valido/ID inesistente\n");
         
      }while(cont==1);
      
      if(scelta==-1)
      {
        sprintf(comando.stringa,"Annulla");
        spedisci_messaggio(comando);
        return;
      }   
      sprintf(comando.stringa,"%d$%d$%d$%d$%s",scelta,coord_x,coord_y,id_utente,titolo); //INVIO AL SERVER: ID_MESSAGGIO$COORD_X$COORD_Y$ID_CREATORE_MESSAGGIO$TIT
      spedisci_messaggio(comando);

     printf("La richiesta di lettura è stata inviata ed è in attesa di essere accetta\n");
     
     distruggi_lista(testa);
     testa=NULL;
}

/***********************************************************************************************/
/***********************************************************************************************/
void distruggi_lista(struct msgclient* testa)
{
  if((testa!=NULL)&&(testa->next!=NULL))
  {
    distruggi_lista(testa->next);
    free(testa); 
  } 

}



/***********************************************************************************************/
/***********************************************************************************************/
struct msgclient* inserisci_msgclient(struct msgclient* testa,int id,int coord_x,int coord_y,int id_utente,char titolo[]){

   struct msgclient* nuovo=(struct msgclient*)malloc(sizeof(struct msgclient));
   
   nuovo->id=id;
   nuovo->x=coord_x;
   nuovo->y=coord_y;
   nuovo->next=testa;
   nuovo->id_utente=id_utente;
   strcpy(nuovo->titolo,titolo);
   
   return nuovo;
}     



/***********************************************************************************************/
/***********************************************************************************************/
int cerca_messaggio(struct msgclient* app,int id,int *x,int *y,char titolo[]){
  
  *x=-1;
  *y=-1;
  while(app!=NULL){
  
    if(app->id==id){
       *x=app->x;
       *y=app->y;
       strcpy(titolo,app->titolo);
       return 1;
    }
     else
       app=app->next;
  }

  return 0;
}

/***********************************************************************************************/
/***********************************************************************************************/


/*----------------------------------------------------------------------------------------------------*/
// Metodo che processa la richiesta di login mostrando la maschera adeguata
/*----------------------------------------------------------------------------------------------------*/
void login()
{
	char flag='s';
	struct risposta risp;
        char string[70];
        char dati[30];
        int x,y;
        
	while( flag == 's')
	{
	        string[0]='\0';
		dati[0]='\0';
		printf("\033[H\033[2J"); //pulire la schermata
		printf("----------------------------------------------------------------\n");
		printf("				LOGIN				\n");
		printf("----------------------------------------------------------------\n");
		
		printf("- Username: \n");
		scanf("%s",dati);
		strcat(string,dati);
		strcat(string,"$");
		
		
		printf("- Password: \n");
		scanf("%s",dati);
		strcat(string,dati);
		strcat(string,"$");
				
					
		printf("- Posizione ascisse(riga):\n");
		while (scanf("%d",&x) != 1 || (x<0 || x>RIGHE ) ) {
		
		       if(x<0 || x>RIGHE )
		        printf("Puoi inserire valori compresi tra 0 e %d:\n",RIGHE);
		       else
			printf("Inserisci un'intero non una stringa:\n");
			
			// rimuovo eventuali caratteri rimasti nel buffer fino al newline
			while (getchar() != '\n');
		}
		sprintf(dati,"%d",x);
                strcat(string,dati);
		strcat(string,"$");
		
		printf("- Posizione ordinate(colonna):\n");
		while (scanf("%d", &y)!=1 || (y<0 || y>COLONNE ) ){
		
		       if(y<0 || y>COLONNE )
		        printf("Puoi inserire valori compresi tra 0 e %d:\n",COLONNE);
		       else
			printf("Inserisci un'intero non una stringa:\n");
			
			// rimuovo eventuali caratteri rimasti nel buffer fino al newline
			while (getchar() != '\n');
		}
		sprintf(dati,"%d",y);
                strcat(string,dati);
		strcat(string,"$");

		printf("- Portata di visibilita' :\n");
		while ((scanf("%d", &x) != 1) || (x<=0 || x>9)) {
		
		       if(x<=0 || x>9 )
		        printf("Puoi inserire valori compresi tra 0 e 9:\n");
		       else
			printf("Inserisci un'intero non una stringa:\n");
			
			// rimuovo eventuali caratteri rimasti nel buffer fino al newline
			while (getchar() != '\n');
		}
		sprintf(dati,"%d",x);
                strcat(string,dati);
		strcat(string,"$");
		
		printf("\nAttendere... \n\n");

		risp.tipoRisposta = 1; // Stiamo inviando una stringa
	        strcpy(risp.stringa,string);
	
		spedisci_messaggio(risp);

		sleep(1);
		ricevi_messaggio(&risp);
		/* Riceve dal server un carattere (TIPO: 2)*/
	
		/* Se il carattere è 'o' allora il LOGIN è completato con successo*/
		if ( risp.carattere == 'o' )
		{
			printf("Login effettuato con successo.\n");
                        flag='n';
		}
		else if ( risp.carattere == 'n' | risp.carattere=='l' )
		{
		        if(risp.carattere=='n')
			  printf("Dati di accesso errati. \n");
			else
			  printf("Utente gia' loggato.\n");
			  
			printf("Vuoi rieffettuare il login? s/n \n");
			do
			{
				scanf("%c",&risp.carattere);
                                if(risp.carattere!= 's' & risp.carattere!= 'n')
                                 printf("Digita Soltanto 's' o 'n'\n");
			}
			while ( risp.carattere!= 's' & risp.carattere!= 'n' ) ;

			// Se non si vuole riprovare si manda errore al server che si rimette in ascolto normalmente
                       flag=risp.carattere;
		       spedisci_messaggio(risp);
		       if ( risp.carattere== 'n' )
		          raise(SIGINT);
		}
	}
}




/*====================== HANDLER ====================================*/


/*******************************************************************************************************
* Informa il server che il terminale è stato chiuso                *
********************************************************************************************************/
void disconnetti(int sig)
{
 struct risposta msg;
 msg.tipoRisposta=2;
 msg.carattere='h';  /* il carattere h (hump) viene inviato quando si chiude il terminale */
 spedisci_messaggio(msg);
 clean_up();
 exit(0);
}



/***********************************************************************************************/
/***********************************************************************************************/
void leggi_messaggi_inviati(){
  char titolo[TITLELEN],corpo[BODYLEN],scelta;
  char butto[TITLELEN];
  int coord_x,coord_y,id_mess,flag,ret;
  struct risposta comando;
  struct msgclient* testa=NULL;
  
  
  printf("I Messaggi da te inviati sono:\n");

       do{
           
        ricevi_messaggio(&comando);/*Ricevuto una stringa contenente: ID_MESSAGGIO$COORD_X$COORD_Y$TITOLO_MESSAGGIO$CORPO  */

        if(comando.tipoRisposta==1 && strcmp(comando.stringa,"FINE")!=0){ /*suddividiamo la stringa in token per poi stampare i messaggi a video*/
               

           id_mess=atoi(strtok(comando.stringa,"$"));
           coord_x=atoi(strtok(NULL,"$"));
	   coord_y=atoi(strtok(NULL,"$"));
	       
	   titolo[0]='\0';
	   corpo[0]='\0';
           strcpy(titolo,strtok(NULL,"$"));
           strcpy(corpo,strtok(NULL,"$"));
               
           printf("\n*********************************************************\n");       
           printf("ID:%d -  COORDINATE: [%d,%d]\nTITOLO:%s\nCORPO:\n%s ",id_mess,coord_x,coord_y,titolo,corpo);
           printf("*********************************************************\n"); 
           
           testa=inserisci_msgclient(testa,id_mess,coord_x,coord_y,-1,"");    /*Inserisco il nuovo elemento alla lista di messaggi*/
           
 
       }

     }while(strcmp(comando.stringa,"FINE")!=0);   

/*RICHIEDO ALL'UTENTE QUALE MESSAGGIO CANCELLARE*/     
  if(testa!=NULL){   //Se l'utente ha scritto messaggi nell'attuale raggio d'azione, richiedo ciclicamente quali vuole cancellare
     do{
        printf("Selezionare il messaggio da cancellare tramite l'ID [Selezionare -1 per annullare l'operazione]:\n");
        
        comando.stringa[0]='\0';
        do{
         flag=1;
          if(scanf("\n%d",&id_mess)!=1){  
             printf("Inserire un intero!\n");
             flag=0;
             getchar();
           }
         }while(flag!=1);
        
        ret=cerca_messaggio(testa,id_mess,&coord_x,&coord_y,butto); //Verifico che l'id del messaggio sia presnete nella lista messaggi. Se è così restituisco le 
                                                              //coordinate del messaggio.
                                                              
        if(ret==1){ //Se ho trovato il messaggio, invio i dati al server nel formato: ID_MESS$COORD_X$COORD_Y
           
           comando.stringa[0]='\0';
           sprintf(comando.stringa,"%d$%d$%d",id_mess,coord_x,coord_y);
           
           spedisci_messaggio(comando);
           
           comando.carattere='\0';     
           ricevi_messaggio(&comando);      //Aspetto dal server 's' o 'n';  
           
           if(comando.carattere=='s'){// Dove 's' indica che il messaggio è stato trovato e cancellato
               testa=cancella_messaggio(testa,id_mess);
               printf("Operazione avvenuta: messaggio cancellato!\n");
           }else{ //Se ricevo 'n' allora so che il messaggio è scaduto
                printf("Operazione non avvenuta: messaggio scaduto!\n");
             }
           
           
        }else{
           printf("Il messaggio selezionato non esiste!\n");
        }
        
    /*se la lista non è vuota, chiedo all'utente se vuole eliminare un altro messaggio*/
       if(testa!=NULL){
         printf("Vuoi eliminare un altro messaggio?[s,n]\n");
        
         do
	 {
	  scanf("\n%c",&scelta);
          
          if(scelta!= 's' & scelta!= 'n')
             printf("Digita Soltanto 's' o 'n'\n");    //Controllo che venga inserito un carattere: s/n
	 }while ( scelta!= 's' & scelta!= 'n' ) ;
         
         if(scelta=='n'){ //Se l'utente non vuole ri-effettuare l'operazione, avverto il server.
	   strcpy(comando.stringa,"FINE");
	   spedisci_messaggio(comando);
	 }
       
       }else{ //Se la lista di messaggi inviati dall'utente è vuota, non chiedo all'utente se vuole eliminare un altro messaggio
	   strcpy(comando.stringa,"FINE");
	   spedisci_messaggio(comando);       
          }
          
         
     }while(testa!=NULL && strcmp(comando.stringa,"FINE")!=0 );
     
   }else{ //Se l'utente non ha messaggi da lui inviati nell'attuale raggio d'azione, avviso il server di terminare l'operazione di cancellazione ed avviso l'utente
      printf("Nel tuo raggio d'azione non sono presenti messaggi!\n");
      strcpy(comando.stringa,"FINE");
      spedisci_messaggio(comando);      
     }
}



/***********************************************************************************************/
/***********************************************************************************************/
struct msgclient* cancella_messaggio(struct msgclient* testa,int id){//QUESTA

   if(testa!=NULL){
      if(testa->id==id){
         struct msgclient* temp=testa;
         testa=testa->next;
         free(temp);
         return testa;
      }else
         testa->next=cancella_messaggio(testa->next,id);   
   }
}


/***********************************************************************************************/
/***********************************************************************************************/
void rispondi_messaggi()
{
  struct notifica_client* notifiche=NULL; 
  struct risposta comando;
  char nick[31],car;
  char titolo[TITLELEN];
  int flag=0,creatore_msg;
  int ret,risp;
  int coord_x,coord_y,id_mess;
  
   do{
        
        ricevi_messaggio(&comando);/*Ricevuto una stringa contenente:  */
       
        if(strcmp(comando.stringa,"FINE")!=0)/*suddividiamo la stringa in token */
        { 
           
           if(strcmp(comando.stringa,"cancellato")!=0){
            flag=flag+1;	   
	    titolo[0]='\0';
	    nick[0]='\0';
            strcpy(nick,strtok(comando.stringa,"$"));
            strcpy(titolo,strtok(NULL,"$"));
            creatore_msg=atoi(strtok(NULL,"$"));
            coord_x=atoi(strtok(NULL,"$"));  
            coord_y=atoi(strtok(NULL,"$")); 
            id_mess=atoi(strtok(NULL,"$"));   
            printf("[NOTIFICA %d] L'utente %s ha richiesto di poter leggere il messaggio con titolo:%s\n",flag,nick,titolo);
            notifiche=inserisci_notifica(notifiche,titolo,nick,flag,creatore_msg,coord_x,coord_y,id_mess);//Inserisco il nuovo elemento alla lista di messaggi  
            
           }
         }

     }while(strcmp(comando.stringa,"FINE")!=0); 
     
     if(flag==0)
     {
       printf("Non ci sono richieste di lettura\n");
       comando.carattere='G';
       spedisci_messaggio(comando);
     }else{
       
       printf("A quale notifica vuoi rispondere? [digita -1 per annullare]\n");
       
       do{
       
       scanf("\n%d",&risp);
       
         ret=cerca_notifica(notifiche,risp,titolo,&creatore_msg,&coord_x,&coord_y,&id_mess);
         if(ret==0)
           printf("La notifica digitata non esiste\n");
            
       }while(ret==0 & risp>0);
       
     
      if(risp>0 && ret==1){
         printf("\n Vuoi permettere all'utente di leggere il tuo messaggio? [s/n]\n");
         do{
           scanf("\n%c",&car);
           if(car!='s' & car!='n')
            printf("Digitare solo s/n\n");
         }while(car!='s' & car!='n');
         
         sprintf(comando.stringa,"%s$%d$%c$%d$%d$%d",titolo,creatore_msg,car,coord_x,coord_y,id_mess);
         comando.carattere='o';
         spedisci_messaggio(comando);
      }else{
         comando.carattere='n';
         spedisci_messaggio(comando);   
      }
     
     }
     
     distruggi_lista_notifica(notifiche);
}
     

/***********************************************************************************************
***********************************************************************************************/
void distruggi_lista_notifica(struct notifica_client* testa)
{
    if((testa!=NULL)&&(testa->next!=NULL))
    {
      distruggi_lista_notifica(testa->next);
      free(testa); 
  } 
}


/***********************************************************************************************
***********************************************************************************************/
struct notifica_client* inserisci_notifica(struct notifica_client* lista,char titolo[],char nick[],int i,int crea,int coord_x,int coord_y,int id_mess){
  struct notifica_client* nuovo=(struct notifica_client*)malloc(sizeof(struct notifica_client));
  
  nuovo->next=lista;
  nuovo->num=i;
  strcpy(nuovo->titolo,titolo);
  strcpy(nuovo->nick,nick);
  nuovo->creatore_msg=crea;
  nuovo->coord_x=coord_x;
  nuovo->coord_y=coord_y;
  nuovo->id_mess=id_mess;
  
  return nuovo;
  
}


/***********************************************************************************************
***********************************************************************************************/
int cerca_notifica(struct notifica_client* lista,int id,char titolo[],int* id_creatoremsg,int* coord_x,int* coord_y,int* id_mess){
 int ret=0;
   if(lista!=NULL){
      if(lista->num==id){
         strcpy(titolo,lista->titolo);
         *id_creatoremsg=lista->creatore_msg;
         *coord_x=lista->coord_x;
         *coord_y=lista->coord_y;
         *id_mess=lista->id_mess;
         return 1;
      }else
         ret=cerca_notifica(lista->next,id,titolo,id_creatoremsg,coord_x,coord_y,id_mess);
   }
   return ret;
}


/***********************************************************************************************
***********************************************************************************************/
void elenca_notifiche(){
 int flag=0;
 char nick[31];
 char titolo[TITLELEN];
 char car[2];
 char stringa[500];
 struct risposta comando; 
                        
 do{
 
   ricevi_messaggio(&comando);
   if(strcmp(comando.stringa,"FINE")!=0){
      flag=1;
      nick[0]='\0';
      titolo[0]='\0';
      car[0]='\0';
      stringa[0]='\0';
      
      strcpy(nick,strtok(comando.stringa,"$"));
      strcpy(titolo,strtok(NULL,"$"));
      strcpy(car,strtok(NULL,"$"));
      
      if(car[0]=='s')
         sprintf(stringa,"L'utente %s ha accettato la tua richiesta di lettura del messaggio con titolo:\n%s",nick,titolo);
         
      if(car[0]=='n')
         sprintf(stringa,"L'utente %s NON ha accettato la tua richiesta di lettura del messaggio con titolo:\n%s",nick,titolo);
         
      
      if(car[0]=='c')
         sprintf(stringa,"Il messaggio dell'utente %s con titolo:%s è stato cancellato\n",nick,titolo);  
          
      printf("******************************************************************\n");
      printf("%s\n",stringa);
      printf("******************************************************************\n");
                                
   }
                           
   }while(strcmp(comando.stringa,"FINE")!=0);
}
