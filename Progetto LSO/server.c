#include <sys/socket.h>
#include <signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <wait.h>
#include <sys/un.h> 
#include <sys/time.h>
#include <netinet/in.h> /* "in" per "sockaddr_in" */
#include<fcntl.h>
#include<sys/stat.h>
#include<time.h>
#include <arpa/inet.h> /* per ntoa ip*/
#include"tipi.h"

#include <sys/select.h> /* Per la select */


/* ============================================================= GLOBAL VAR==================================================================================== */

int lastidmsg=-1;                                         /* Contatore per il calcolo dell'idmessaggio Univoco */
int descrittore_socket;                                   /* Socket descriptor del socket principale d'ascolto connessioni */
int numeroUtenti=0;                                       /*Numero degli utenti Registrati*/
pthread_mutex_t lastidmsgmutex=PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP; /* Mutex associato alla variabile lastidmsg per generare id dei messaggi */
pthread_mutex_t mutex_users=PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP; /* Mutex associato all'array users */
pthread_mutex_t mutex_file=PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP;/* Mutex associato al file Utenti.txt*/
pthread_mutex_t mutex_varUtenti=PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP;/* Mutex associato alla variabile contenente il numero di utenti registrati*/
pthread_key_t key_tsd;                                    /* Chiave TSD per l'ip*/
pthread_key_t key_indice;                                 /* Chiave TSD per l'indice del vettore utente*/
pthread_key_t key_socket;                                 /* Chiave TSD per l'indice del vettore utente*/
struct cella** mappa;                                     /* Matrice che rappresenta la Mappa dei Messaggi */
struct user *users;                                       /* Array di utenti               */



/* ============================================================= INTESTAZIONI FUNZIONI ======================================================================== */
int reverse_atoi(char num[]);        /*Funzione che gira un numero in formato stringa e lo converte da stringa ad intero*/
int crea_socket_server(int Porta);   /* Crea il socket */
void initmappa();                     /* Alloca ed inizializza la Mappa dei Messaggi*/
void* start(void* args);              /* Funzione di start del thread */

/*------------>>LOGIN<<*/
//DA AGGIUSTARE DA QUA....
void login();                                        /* Funzioni che effettuano Login/Registrazione*/
void registrazione();
void crea_array(int fd_utenti);
void clean_risp(struct risposta* risp);
void posizionamento();                      /* Funzione che posiziona l'utente appena loggato sulla Mappa */
void gestisci_richiesta();                  /* Funzione che gestisce il menu delle operazioni */
void elenca_utenti();                       /* Funzione di visualizzazione utenti nel raggio di azione*/
void stampa_users(struct idusers* lista);   /**/
void trova_messaggi();
void stampa_messaggi_inviati(struct message* lista,int id);
void verifica_cancella();
struct message* cancella_messaggio(struct message* lista,int id,int* flag);
void cambia_posizione();                    /* Funzione che effettua un cambio di posizione dell'utente*/
void elenca_messaggi();
void elenca_messaggi_privati();
void elenca_notifiche();
void genera_notifica(char stringa[]);
void elabora_risposta_notifiche();
struct notifica_richiesta* rispondi_notifica(struct notifica_richiesta* lista);
void stampa_messaggi(struct message* lista);
void stampa_messaggi_privati(struct message* lista);
void* start_thread_elimina_scaduti(void* args); /* Funzione che esegue il thread per l'eliminazione dei messaggi scaduti */
struct message* elimina_scaduti(struct message* testa,time_t time1);
struct notifica_richiesta* cancella_richiesta(struct notifica_richiesta* lista,int idmsg,int mitt);
struct idusers* inserisci_reder_testa(struct idusers* testa,int id_ut);
void sblocca_tutti_mutex();
void elimina_notifiche_visualizzate(struct notifica_risposta* testa);


/*----------->>FUNZIONI SU LISTE<< */
struct idusers* inserisci_testa_user(struct idusers* testa, int nuovo_id); /* Funzione che inserisce un utente alla lista di idUsers presente su ogni 
                                                                           /*   cella della mappa.*/
struct idusers* cancella_user(struct idusers* lista,int id);
void aggiungi_reader_msg(struct message* testa,int id_mess,int id_ut);

struct message* inserisci_testa_msg(struct message* testa, char titolo[TITLELEN], char corpo[BODYLEN], int visibility,int *flag);
int check_lettore(struct idusers* lettore);
int check_mess(struct message* lista,int id_mess);
struct notifica_richiesta* inserisci_testa_richiesta(struct notifica_richiesta* lista,int id_mess,int coord_x,int coord_y,int mio_id,char titolo[]);struct notifica_risposta* inserisci_testa_risposta(struct notifica_risposta* lista,int id_utente,char nick[],char titolo[],char risp);
/* ----------->>UTILITA GENERALE<< */
void printlog(const char* msg);                     /* Funzione che scrive sul file di log un evento*/
void logout();                                      /* Funzione che  effettua il logout di un client*/
char* get_ip_string();                              /* Funzione che ritorna l'ip del client in rappresentazione stringa*/
int get_idmsg();                                     /* Funzione che genera un id univoco per i messaggi*/
int get_indice();                                   /* Funzione che ritorna l'indice del vettore utente attualmente connesso */
int get_socket();                                   /* Funzione che ritorna il socket descriptor per la comunicazione con il client */    
void spedisci_messaggio(struct risposta Messaggio); /* Funzione di invio dati al client */
void ricevi_messaggio(struct risposta *risp);       /* Funzione di ricezione dati dal client */

/* ----------->>HANDLER<< */      
void clean_up(int sig);       /* Funzione che chiude in modo pulito il server */

/* ============================================================================================================================================================ */


/* =================================================================== MAIN ====================================================================================*/
int main(int argc,char* argv[])
{
  int fd_utenti;       /*File Descriptor del file Utenti.txt*/
  int nuovo_socket,porta=0;
  pthread_t tid;
  int letti;
  char num[11];
  char car[2];
  sigset_t insime_segnali;
  
  porta=atoi(argv[1]);
  printf("\nPorta:%d \n",porta);
  
  /* Gestione segnali */
  sigfillset(&insime_segnali);                   /* Inizializzo l'insime dei segnali con l'insime UNIVERSO */ 
  sigdelset(&insime_segnali,SIGINT);             /* Escludo dall'insieme dei segnali SIGINT */
  sigprocmask(SIG_SETMASK,&insime_segnali,NULL); /* Setto come insieme dei segnali bloccati "insime_segnali"*/  
  signal(SIGINT,clean_up);
  
  /* Controllo numero di parametri */
  if ( argc != 2 ) printf("Sintassi Corretta:  ./server.o Porta\n"), exit(1);

  struct sockaddr_in indirizzo_client;
  socklen_t dimensione=sizeof(indirizzo_client);

  /* inizializzazione chiave TSD IP*/
  if(pthread_key_create(&key_tsd,pthread_exit)<0)
  {
    printf("Impossibile inizializzare la chiave TSD dell'IP\n");
    raise(SIGINT);
  }

  /* inizializzazione chiave TSD dell'indice*/
  if(pthread_key_create(&key_indice,pthread_exit)<0)
  {
    printf("Impossibile inizializzare la chiave TSD dell'indice\n");
    raise(SIGINT);
  }

  /* inizializzazione chiave TSD del socket*/  
  if(pthread_key_create(&key_socket,pthread_exit)<0)
  {
    printf("Impossibile inizializzare la chiave TSD del socket\n");
    raise(SIGINT);
  }  

  /* Crea il socket + bind + listen */
  descrittore_socket=crea_socket_server(porta);
  
  /*Apro il file Utenti.txt contenente gli utenti*/  
  if((fd_utenti=open(FILEUTENTI,O_RDONLY))<0){
     perror("File Degli utenti non trovato"); 
     raise(SIGINT);
   }
 
  lseek(fd_utenti,-2,SEEK_END);
  num[0]='\0'; /*Inizializzazione Stringa num */
  
  do{
    read(fd_utenti,&car,1);
    
    car[1]='\0';
    if(car[0]!='#'){
      strcat(num,car);
      lseek(fd_utenti,-2,SEEK_CUR);
    }
  }while(car[0]!='#');
  
  numeroUtenti=reverse_atoi(num);
  
  // Alloca l' ARRAY UTENTI in base al numero di utenti iscritti
  users = (struct user *)malloc(sizeof(struct user) * numeroUtenti);
  
  lseek(fd_utenti,1,SEEK_SET); //Pongo l'offset all'inizio del file
  crea_array(fd_utenti); //Riempio il vettore Users andando a leggere id,nickname e password dal file Utenti.txt
  
  close(fd_utenti); // Chiusura del file

  /* Inizializza la Mappa dei Messaggi*/
  initmappa();
  
  /* Lancio del thread per la cancellazione dei messaggi scaduti*/
    if( (pthread_create(&tid,NULL,start_thread_elimina_scaduti,NULL)) !=0 ){
      printlog("Impossibile avvaire il thread per la rimozione dei messaggi\n");
      raise(SIGINT);
    }
    pthread_detach(tid);
  
  
  printlog("Server Attivato...");
  
  while (1)
  {
    
    /* Attendo richieste di connessione */
    if ((nuovo_socket=accept(descrittore_socket,(struct sockaddr *)&indirizzo_client,&dimensione))<0){ 
        printf("Errore connessione\n");
        continue; 
    } 
  
    /* Allocazione spazio argomenti */
    struct argomenti_thread_t* argomenti_thread=(struct argomenti_thread_t*)malloc(sizeof(struct argomenti_thread_t));
    if(argomenti_thread==NULL)
    {
      printlog("Impossibile allocare spazio\n");
      close(nuovo_socket);
      continue;
    }

    argomenti_thread->nuovo_socket_p=(int*)malloc(sizeof(int));
    if(argomenti_thread->nuovo_socket_p==NULL)
    {
      printlog("Impossibile allocare spazio\n");
      free(argomenti_thread);
      close(nuovo_socket);
      continue;
    }

    /* Assegnazione argomenti */
    *(argomenti_thread->nuovo_socket_p)=nuovo_socket;
    strcpy(argomenti_thread->ip_client,inet_ntoa(indirizzo_client.sin_addr));

    /* Creazione thread per la gestione connessione con i relativi parametri */
    if((pthread_create(&tid,NULL,start,(void*)argomenti_thread))!=0){
      printlog("Impossibile soddisfare la richiesta\n");
      free(argomenti_thread->nuovo_socket_p);
      free(argomenti_thread);
      close(nuovo_socket);
      continue;       
    }
    pthread_detach(tid);
    //pthread_setcancelstate();

    argomenti_thread=NULL;

  }
 
  return 0;
}

/*==================================================================== END MAIN ================================================================================*/


/*=================================================================== FUNZIONI =================================================================================*/


/********************************************************************************************************
*                                                                                                       *
********************************************************************************************************/
int reverse_atoi(char num[]){
   
   int i;
   char temp[strlen(num)+1];
  
   for(i=0;i<strlen(num);i=i+1){
       temp[i]=num[strlen(num)-(i+1)]; 
   }
   
   temp[strlen(num)]='\0';
   return atoi(temp);
}



/********************************************************************************************************
*                                                                                                       *
********************************************************************************************************/
int crea_socket_server(int porta)
{
  int sock,errore;
  struct sockaddr_in temp;

  /*Creazione socket*/
  sock=socket(AF_INET,SOCK_STREAM,0);

  /*Tipo di indirizzo*/
  temp.sin_family=AF_INET;
  temp.sin_addr.s_addr=INADDR_ANY;
  temp.sin_port=htons(porta);

  /*Bind del socket su uno specifico indirizzo */
  errore=bind(sock,(struct sockaddr*) &temp,sizeof(temp));

  /*Il sistema accetta fino a 10 richieste
  contemporanee le quali finiranno nella coda delle connessioni).*/
  errore=listen(sock,10);
 
  return sock;
}



/*******************************************************************************************************
* Funzione che inizializza i mutex della mappa e i puntatori                                           *
********************************************************************************************************/
void initmappa()
{
  int i,j;
  pthread_mutexattr_t attributi;
  pthread_mutexattr_init(&attributi);
  pthread_mutexattr_settype(&attributi,PTHREAD_MUTEX_ERRORCHECK_NP); /* Dichiaro il mutex di tipo ERROR-CHECKING*/
  
  
  mappa=(struct cella**)malloc(sizeof(struct cella*)*RIGHE);
  
  for(i=0;i<RIGHE;i++)
    mappa[i]=(struct cella*)malloc(sizeof(struct cella)*COLONNE);


  for(i=0;i<RIGHE;i++)
  {
    for(j=0;j<COLONNE;j++)
    {
      mappa[i][j].headmsg=NULL;
      mappa[i][j].headuser=NULL;

      pthread_mutex_init(&mappa[i][j].mutexcella,&attributi);

    }
  }
  
  pthread_mutexattr_destroy(&attributi);
}



/********************************************************************************************************
* Funzione Start del thread.      									*
********************************************************************************************************/
void* start(void* args){

 /* Copia sullo stack locale degli argomenti ricevuti*/
 struct risposta risp;
 struct argomenti_thread_t* arg=(struct argomenti_thread_t *)args; /* stuttura d'appoggio per seplificare il casting */
 char ip[50];
 int socket;
 sigset_t insime_segnali;
 
 /* Maschera dei segnali segnali bloccati per i thread */
  sigfillset(&insime_segnali);                   /* Inizializzo l'insime dei segnali con l'insime UNIVERSO */ 
  sigdelset(&insime_segnali,SIGINT);             /* Escludo dall'insieme dei segnali SIGINT */
  pthread_sigmask(SIG_SETMASK,&insime_segnali,NULL); /* Setto come insieme dei segnali bloccati "insime_segnali"*/  

 socket=*arg->nuovo_socket_p;
 free(arg->nuovo_socket_p);

 strcpy(ip,arg->ip_client);
 
 free(args);
 arg=NULL;
 args=NULL;

 /* Rendo l'ip del client un TSD (variabile globale del thread)*/
 if(pthread_setspecific(key_tsd,(void*)&ip)<0)
 {
   printlog("Impossibile associare la chiave tsd\n");
   close(socket);
   pthread_exit(NULL);
 }

 if(pthread_setspecific(key_socket,(void*)&socket)<0)
 {
   printlog("Impossibile associare la chiave tsd\n");
   close(socket);
   pthread_exit(NULL);
 }
 
 printlog("Ricevuta richiesta di connessione IP:");
 
 ricevi_messaggio(&risp);
 
 if(risp.carattere=='l')
  {
   login();
   }
 else if(risp.carattere=='r')
 {  
   registrazione();
   login();
  }
 else
   //chiama funzione che pulisce tutto prima di chiudere
   pthread_exit(NULL); 
 
 /*Posiziono l'utente sulla mappa*/
 posizionamento();

 gestisci_richiesta();

 printlog("Client Disconnesso IP:");

}


/********************************************************************************************************
* La funzione login gestisce la fase di login, ritorna 0 se il login ha avuto successo -1 altrimenti    *
*********************************************************************************************************/
void login()
{
   struct risposta risp;
   char messaggio[100];
   char nick[30],pass[30],coordx[3],coordy[3],raggio[2];
   int x,y,rag,i,*indice;
   int flag;
   risp.carattere='r';
  do{
  
   /*ricevo login e password*/
   ricevi_messaggio(&risp);

   /*Spacchetto la stringa ricevuta dal client*/
   strcpy(nick,strtok(risp.stringa,"$"));
   strcpy(pass,strtok(NULL,"$")); 
    
   strcpy(coordx,strtok(NULL,"$"));  
   x=atoi(coordx);  
   strcpy(coordy,strtok(NULL,"$")); 
   y=atoi(coordy);   
   strcpy(raggio,strtok(NULL,"$")); 
   rag=atoi(raggio);   	
   
   pthread_mutex_lock(&mutex_users);
   pthread_mutex_lock(&mutex_varUtenti);
   i=0;
   flag=0;
   while(i<numeroUtenti){
      if(strcmp(users[i].nickname,nick)==0)
        if(strcmp(users[i].password,pass)==0){
         if(users[i].connection_status==0){//Utente non loggato!
           users[i].connection_status=1;
           users[i].x=x;
           users[i].y=y;
           users[i].raggio=rag;
           flag=1;
           break;      
         }else{ //Utente già loggato!
            flag=2;
            break;
         }
        } 
     i=i+1;
   }
   pthread_mutex_unlock(&mutex_varUtenti);   
   pthread_mutex_unlock(&mutex_users);
   
   if(flag==0){//i dati inseriti dall'utente sono errati
     risp.carattere='n';
   }
   else
     if(flag==1){//l'utente si è loggato correttamente
       risp.carattere='o';
       sprintf(messaggio,"%s ha effettuato la log-in in (%d,%d) con raggio %d",nick,x,y,rag);
       printlog(messaggio);
     }
     else
       if(flag==2){//l'utente si è già loggato
         risp.carattere='l';
       }
   
   spedisci_messaggio(risp); //Invio al client i risultati della verifica credenziali
   
   if(risp.carattere=='n' | risp.carattere=='l'){
    ricevi_messaggio(&risp);
    if(risp.carattere=='n'){ 
      pthread_exit(NULL);
   }
  }
  }while(risp.carattere!='o');
  
  indice=(int*)malloc(sizeof(int)); // Copio il valore dell'indice nello heap
  *indice=i;
                                
  /* Rendo l'indice un TSD (variabile globale del thread)*/
  if(pthread_setspecific(key_indice,(void*)indice)<0)
  {
     perror("Impossibile associare la chiave tsd indice");
     close(get_socket());
     logout();
  }
}


/********************************************************************************************************
* Registrazione - Modulo di gestione delle richieste di registrazione                                   *
*													*
*********************************************************************************************************/
void registrazione()
{ 
  
  struct risposta risp;
  char messaggio[100];
  char nick[31] ;
  char pass[31] ;
  char stringa[90],add_id[11],car[2];
  int  i,trovato,n;
  int fd_file;
  struct user* temp;
  
  do
  {
      nick[0]='\0';
      pass[0]='\0';
      stringa[0]='\0';
      trovato=0;
      
      /*ricevo login e password*/
      ricevi_messaggio(&risp);
      
      /*Spacchetto la stringa ricevuta dal client*/
      strcpy(nick,strtok(risp.stringa,"$"));
      strcpy(pass,strtok(NULL,"$")); 
  
       /* Controllo se l'untente esiste gia'*/
       i=0;
       while((i<numeroUtenti)&(trovato==0))
       {
         if((strcmp(users[i].nickname,nick)==0)&(strcmp(users[i].password,pass)==0))
             trovato=1;
         i=i+1;
       }
     
       if(trovato==0) /*L'utente non esiste, effettuo una registrazione*/
       {
          pthread_mutex_lock(&mutex_file); /*Blocco il mutex del file Utenti.txt*/
          
          if((fd_file=open(FILEUTENTI,O_RDWR))<0){ /*Se il file Utenti.txt non esiste o non viene trovato, il server avverte il client che entra nel*/
             printf("ERRORE:File \"Utenti.txt\" non trovato\n");  /*menù di login*/
             pthread_mutex_unlock(&mutex_file);
             clean_risp(&risp);
             risp.carattere='r';
             spedisci_messaggio(risp); 
             return;              
          }
          else{
            pthread_mutex_lock(&mutex_varUtenti); /*Blocca il mutex della variabile contentente il numero degli utenti registrati*/

            sprintf(add_id,"%d",numeroUtenti); /*Copio in una stringa il vecchio numero degli utenti*/
             
            if(lseek(fd_file,-2,SEEK_END)<0){  /*Mi sposto alla fine del file "Utenti.txt"*/
             clean_risp(&risp);
             risp.carattere='r';
             spedisci_messaggio(risp);
             close(fd_file);
             pthread_mutex_unlock(&mutex_file);
             pthread_mutex_unlock(&mutex_varUtenti);  
             return;            
            }
            
            do{
               if(read(fd_file,&car,1)<0){   /*Leggo, a partire dalla fine del file, un carattere alla volta finquando non incontro il carattare '#'*/
                 clean_risp(&risp);
                 risp.carattere='r';
                 spedisci_messaggio(risp);
                 close(fd_file);
                 pthread_mutex_unlock(&mutex_file);
                 pthread_mutex_unlock(&mutex_varUtenti); 
                 return;           
              }
    
               car[1]='\0';
               if(car[0]!='#'){
                 if(lseek(fd_file,-2,SEEK_CUR)<0){
                      clean_risp(&risp);
                      risp.carattere='r';
                      spedisci_messaggio(risp);
                      close(fd_file);
                      pthread_mutex_unlock(&mutex_file);
                      pthread_mutex_unlock(&mutex_varUtenti);  
                      return;                 
                 }
              }
                
            }while(car[0]!='#');
            
            lseek(fd_file,-1,SEEK_CUR);  /*Mi sposto DOPO il carattere '#'*/
            
            strcat(stringa,add_id);  /*Costruisco la stringa secondo il formato: id_utente$nickname$password$#NUMERO_UTENTI_TOTALE$*/
            strcat(stringa,"$");
            strcat(stringa,nick);
            strcat(stringa,"$");
            strcat(stringa,pass);
            strcat(stringa,"$#");
            
            sprintf(add_id,"%d",numeroUtenti+1);
            strcat(stringa,add_id);
            strcat(stringa,"$");
            
            if(write(fd_file,stringa,strlen(stringa))<0){
             clean_risp(&risp);
             risp.carattere='r';
             spedisci_messaggio(risp);  
             close(fd_file);
             pthread_mutex_unlock(&mutex_file);              
             pthread_mutex_unlock(&mutex_varUtenti);              
             return;
            }else{  
             /*Aggiungo il nuovo utente al vettore Utenti*/
              pthread_mutex_lock(&mutex_users);
            
              temp=(struct user*)malloc(numeroUtenti+1*sizeof(struct user));

             if(temp!=NULL){
              numeroUtenti=numeroUtenti+1;             
              /*Copio i vecchi utenti*/
              for(n=0;n<numeroUtenti-1;n=n+1){
                temp[n].id=users[n].id;
                temp[n].raggio=users[n].raggio;
                temp[n].x=users[n].x;
                temp[n].y=users[n].y;
                temp[n].connection_status=users[n].connection_status;
                temp[n].richiesta=users[n].richiesta;
                temp[n].risposta=users[n].risposta;
                strcpy(temp[n].nickname,users[n].nickname);
                strcpy(temp[n].password,users[n].password);   
               }
            
             /*Aggiungo il nuovo utente*/
             temp[numeroUtenti-1].id=numeroUtenti-1;
             temp[numeroUtenti-1].raggio=0;
             temp[numeroUtenti-1].x=0;
             temp[numeroUtenti-1].y=0;
             temp[numeroUtenti-1].connection_status=0;
             temp[numeroUtenti-1].richiesta=NULL;
             temp[numeroUtenti-1].risposta=NULL;
             strcpy(temp[numeroUtenti-1].nickname,nick);
             strcpy(temp[numeroUtenti-1].password,pass);
           
             free(users);
             users=temp;
             temp=NULL;
             
             pthread_mutex_unlock(&mutex_users);   
             close(fd_file);
             pthread_mutex_unlock(&mutex_file);              
             pthread_mutex_unlock(&mutex_varUtenti);             
             clean_risp(&risp);
             risp.carattere='a';
             spedisci_messaggio(risp); 
             sprintf(messaggio,"Avvenuta Registrazione Utente:%s IP:",nick);
             printlog(messaggio);
            }
            else{
             clean_risp(&risp);
             risp.carattere='r';
             spedisci_messaggio(risp);  
             pthread_mutex_unlock(&mutex_users);
             close(fd_file);
             pthread_mutex_unlock(&mutex_file);              
             pthread_mutex_unlock(&mutex_varUtenti);   
             return;             
            } 
           }
          }
       }
     
       if(trovato==1)
       {
          //se esiste manda al client e;
          clean_risp(&risp);
          risp.carattere='e';
          spedisci_messaggio(risp);
       }    
   }while(trovato==1);
}



/********************************************************************************************************
 * CREA ARRAY - Funzione che crea gli array users e logins caricando i dati dai file                    *
*********************************************************************************************************/
void crea_array(int fd_utenti)
{
   char nickname[30],password[30];
   char car[2];
   int max,id_utente;
   
   max=(int)log10((double)numeroUtenti)+1;
   char id[max];
   

  while(read(fd_utenti,&car,1)>0){

    if(car[0]=='#')
      break;
      
    id[0]='\0';
    nickname[0]='\0';
    password[0]='\0';
    
    lseek(fd_utenti,-1,SEEK_CUR);
    
    while((read(fd_utenti,&car,1))>0)        // Leggo l'id dell'utente
    {
      car[1]='\0';
      if(car[0]!='$')
       strcat(id,car);
     else
      break;
    }  
    id_utente=atoi(id);
    users[id_utente].id=id_utente;
    
    while((read(fd_utenti,&car,1))>0)        //Leggo il nickname dell'utente
    {
     car[1]='\0';
     if(car[0]!='$')
       strcat(nickname,car);
     else
       break;
    }  
    strcpy(users[id_utente].nickname,nickname);
  
  
    while((read(fd_utenti,&car,1))>0)        //Leggo la password dell'utente
    {
      car[1]='\0';
      if(car[0]!='$')
        strcat(password,car);
      else
        break;
    } 
    strcpy(users[id_utente].password,password); 
 
    users[id_utente].connection_status=0;
    users[id_utente].x=0;
    users[id_utente].y=0;
    users[id_utente].raggio=0;
    users[id_utente].richiesta=NULL;
    users[id_utente].risposta=NULL;
   }
	
   /*int i; // Stampa di debug del vettore user
   for(i=0;i<numeroUtenti;i=i+1){
     printf("Utente id:%d, Nick:%s, pass:%s, x:%d, y:%d,raggio:%d,connection_status:%d\n",users[i].id,users[i].nickname,users[i].password,users[i].x,users   [i].y,users[i].raggio,users[i].connection_status);

   }	*/	
}


/********************************************************************************************************
 * CLEAN RISP - Procedura che resetta la risposta                                                       *
/********************************************************************************************************/
void clean_risp(struct risposta* risp)
{
	risp->tipoRisposta=0;
	risp->stringa[0]='\0';
	risp->carattere='\0';
}



/******************************************************************************************************
* Funzione che posiziona l'utente sulla matrice,cambiando il suo stato da Sconnesso a Connesso.       *
******************************************************************************************************/
void posizionamento(){
   
   int idusr=get_indice();  //Ricavo l'indice dell'utente attualmente connesso.
   int j,i;

   pthread_mutex_lock(&mutex_users);  /*Blocco il mutex per il vettore degli users*/
   i=users[idusr].x;
   j=users[idusr].y;

   pthread_mutex_unlock(&mutex_users); /*Sblocco il mutex per il vettore degli users*/  
   

   /*Gestisco mutex cella ed inserisco un nuovo utente nella lista users*/
   pthread_mutex_lock(&(mappa[i][j].mutexcella));

   mappa[i][j].headuser=inserisci_testa_user(mappa[i][j].headuser,idusr);
   
   pthread_mutex_unlock(&(mappa[i][j].mutexcella));
    
}



/*******************************************************************************************************
* Funzione che riceve le richieste del client                                                          *
********************************************************************************************************/
void gestisci_richiesta(){

 struct risposta risposta;
 struct risposta messaggio;
 char string_log[500];
 string_log[0]='\0';

 int socket=get_socket();
 
 int x,y,flag;
 
 do{
       pthread_mutex_lock(&mutex_users);
       x= users[get_indice()].x;
       y= users[get_indice()].y;
       pthread_mutex_unlock(&mutex_users);
       ricevi_messaggio(&risposta);

       if(risposta.tipoRisposta==2){
  
       switch(risposta.carattere){
    
       case 'a':
              elenca_utenti();
              messaggio.tipoRisposta=1;
              strcpy(messaggio.stringa,"FINE");
              spedisci_messaggio(messaggio); 
               sprintf(string_log,"L'utente %s ha chiesto di visualizzare gli utenti",users[get_indice()].nickname);
               printlog(string_log);
          break;
       case 'b':
              
              ricevi_messaggio(&risposta);
              pthread_mutex_lock(&mappa[x][y].mutexcella);
              mappa[x][y].headmsg=inserisci_testa_msg(mappa[x][y].headmsg,risposta.messaggio.titolo,risposta.messaggio.corpo,risposta.messaggio.visibility,&flag);
              pthread_mutex_unlock(&mappa[x][y].mutexcella);
              clean_risp(&risposta);
              risposta.tipoRisposta=2;     
                       
              if(flag==0){
                risposta.carattere='k';
              }else{
                risposta.carattere='o';              
               }
               
               spedisci_messaggio(risposta);
                sprintf(string_log,"L'utente %s ha chiesto di inviare un messaggio",users[get_indice()].nickname);
                printlog(string_log);
          break;
       case 'c':
              trova_messaggi();
              messaggio.tipoRisposta=1;
              strcpy(messaggio.stringa,"FINE");
              spedisci_messaggio(messaggio); 
              verifica_cancella();
              sprintf(string_log,"L'untente %s ha chiesto di cancellare un messaggio",users[get_indice()].nickname);
              printlog(string_log);
          break;
       case 'd':
              elenca_messaggi();              
              messaggio.tipoRisposta=1;
              strcpy(messaggio.stringa,"FINE");
              spedisci_messaggio(messaggio); 
              sprintf(string_log,"L'untente %s ha chiesto di vedere tutti i messaggi da lui visibili",users[get_indice()].nickname);
              printlog(string_log);
          break;
       case 'e':
             elenca_messaggi_privati();              
             messaggio.tipoRisposta=1;
             strcpy(messaggio.stringa,"FINE");
             spedisci_messaggio(messaggio);
             clean_risp(&messaggio);
             ricevi_messaggio(&messaggio);
             if(strcmp(messaggio.stringa,"Annulla")!=0) 
             {
                 genera_notifica(messaggio.stringa);
             }    
             sprintf(string_log,"L'untente %s ha chiesto di vedere tutti i messaggi da lui NON visibili",users[get_indice()].nickname);
             printlog(string_log);
          break;
       case 'f':
             sprintf(string_log,"L'untente %s ha chiesto di visualizzare le notifiche",users[get_indice()].nickname);
             printlog(string_log);
            ricevi_messaggio(&risposta);            
            
            switch(risposta.carattere){
               case 'l':/*L'UTENTE HA RICHIESTO DI LEGGERE LE NOTIFICHE ACCETTATE/RIFIUTATE*/
                        elenca_notifiche();
                  break;
               case 'r':/*L'UTENTE HA RICHIESTO DI RISPONDERE ALLE NOTIFICHE*/ 
                        
                        pthread_mutex_lock(&mutex_users);
                          
                        users[get_indice()].richiesta=rispondi_notifica(users[get_indice()].richiesta);
                       
                        pthread_mutex_unlock(&mutex_users);
                        
                        risposta.carattere='\0';
                        risposta.stringa[0]='\0';
                        strcpy(risposta.stringa,"FINE");
                        risposta.carattere='\0';
                        spedisci_messaggio(risposta);
                        
                        elabora_risposta_notifiche();
                        
                        /*BLOCCO DI NUOVO IL MUTEX USERS E CANCELLO LA RICHIESTA E INSERISCO LA NOTIFICA SU LISTA RISPOSTA DELL'UTENTE GET_INDICE*/
                 break;
               case 'x':
                        risposta.carattere='\0';
                  break;
               default:
                  break;
            }
            
          break;
       case 'g':
          sprintf(string_log,"L'utente %s ha chiesto di cambiare posizione",users[get_indice()].nickname);
          printlog(string_log);
          cambia_posizione();
          break;
       case 'x':
          /*CLIENT HA RICHIESTO DI EFFETTUARE IL LOGOUT\n*/
          close(get_socket());
          logout();
          break;
       default:
      
          break;  
       
      }
   
      }else{
      }
     
  }while(risposta.carattere!='x');
 
}



/*******************************************************************************************************
* Funzione che Elenca gli utenti presenti nel raggio d'azione dell'utente attualmente loggato.         *
********************************************************************************************************/
void elenca_utenti(){
 
 int raggio,i,j,parti,partj,arri,arrj,x,y;
 int idusr=get_indice();//Ricavo l'indice dell'utente attualmente connesso.
 char string[500];
 
 pthread_mutex_lock(&mutex_users);
 
 raggio=users[idusr].raggio;
 x=users[idusr].x;
 y=users[idusr].y; 

 
 if(x-raggio <=0)
   parti=0;
 else
  parti=x-raggio;

 if(y-raggio <=0)
   partj=0;
 else
  partj=y-raggio;
 
 if(x+raggio >=RIGHE)
   arri=RIGHE;
 else
  arri=x+raggio;

 if(y+raggio >=COLONNE)
   arrj=COLONNE;
 else
  arrj=y+raggio;

 for (i=parti;i<=arri;i=i+1){
    for(j=partj;j<=arrj;j=j+1){
      pthread_mutex_lock(&(mappa[i][j].mutexcella));
  
      stampa_users(mappa[i][j].headuser);

      pthread_mutex_unlock(&(mappa[i][j].mutexcella));
    }
 }
 
 pthread_mutex_unlock(&mutex_users);
 
}



/*----------------------------------------------------------------------------------------------------*/
// CAMBIA POSIZIONE - Procedura che cambia la posizione dell'utente sulla MAPPA
//-----------------------------------------------------------------------------------------------------
void cambia_posizione()
{
	int x,y,id;
	struct risposta risp;
	char c;

	id=get_indice();


        pthread_mutex_lock(&mutex_users);
        x=users[id].x;
        y=users[id].y;
       
		pthread_mutex_lock(&mappa[x][y].mutexcella);
		

		ricevi_messaggio(&risp);
		c=risp.carattere;
		mappa[x][y].headuser=cancella_user(mappa[x][y].headuser,id);
		switch(c){
			case 'u':
			{
			        if(x-1>0){
				users[id].x=users[id].x-1;
				mappa[x-1][y].headuser=inserisci_testa_user(mappa[x-1][y].headuser,id);
				
				}			

			}
			break;

			case 'd':
			{
			        if(x+1<RIGHE){
				users[id].x=users[id].x+1;
				mappa[x+1][y].headuser=inserisci_testa_user(mappa[x+1][y].headuser,id);
				
				}			

			}
			break;

			case 'r':
			{
			        if(y+1<COLONNE){
				users[id].y=users[id].y+1;
				mappa[x][y+1].headuser=inserisci_testa_user(mappa[x][y+1].headuser,id);
				
				}			
			}
			break;

			case 'l':
			{
			        if(y-1>0){
				users[id].y=users[id].y-1;
				mappa[x][y-1].headuser=inserisci_testa_user(mappa[x][y-1].headuser,id);
				}
			}
			break;

			default:
			break;
		}
                pthread_mutex_unlock(&mappa[x][y].mutexcella);
		pthread_mutex_unlock(&mutex_users);
		
	
}



/*******************************************************************************************************
*         *
********************************************************************************************************/
void elenca_messaggi(){
 
 int raggio,i,j,parti,partj,arri,arrj,x,y;
 int idusr=get_indice();//Ricavo l'indice dell'utente attualmente connesso.
 
 pthread_mutex_lock(&mutex_users);
 
 raggio=users[idusr].raggio;
 x=users[idusr].x;
 y=users[idusr].y; 
 
 
 if(x-raggio <=0)
   parti=0;
 else
  parti=x-raggio;

 if(y-raggio <=0)
   partj=0;
 else
  partj=y-raggio;
 
 if(x+raggio >=RIGHE)
   arri=RIGHE;
 else
  arri=x+raggio;

 if(y+raggio >=COLONNE)
   arrj=COLONNE;
 else
  arrj=y+raggio;

 for (i=parti;i<=arri;i=i+1){
    for(j=partj;j<=arrj;j=j+1){
      pthread_mutex_lock(&(mappa[i][j].mutexcella));
  
      stampa_messaggi(mappa[i][j].headmsg);

      pthread_mutex_unlock(&(mappa[i][j].mutexcella));
    }
 }
 
 pthread_mutex_unlock(&mutex_users);
 
}



/*******************************************************************************************************
*          *
********************************************************************************************************/
void elenca_messaggi_privati(){
 
 int raggio,i,j,parti,partj,arri,arrj,x,y;
 int idusr=get_indice();//Ricavo l'indice dell'utente attualmente connesso.
 
 pthread_mutex_lock(&mutex_users);
 
 raggio=users[idusr].raggio;
 x=users[idusr].x;
 y=users[idusr].y; 
 
 if(x-raggio <=0)
   parti=0;
 else
  parti=x-raggio;

 if(y-raggio <=0)
   partj=0;
 else
  partj=y-raggio;
 
 if(x+raggio >=RIGHE)
   arri=RIGHE;
 else
  arri=x+raggio;

 if(y+raggio >=COLONNE)
   arrj=COLONNE;
 else
  arrj=y+raggio;

 for (i=parti;i<=arri;i=i+1){
    for(j=partj;j<=arrj;j=j+1){
      pthread_mutex_lock(&(mappa[i][j].mutexcella));
  
      stampa_messaggi_privati(mappa[i][j].headmsg);

      pthread_mutex_unlock(&(mappa[i][j].mutexcella));
    }
 }
 
 pthread_mutex_unlock(&mutex_users);
}


/***************************************************************************************************************************************
*     										       *
****************************************************************************************************************************************/
void genera_notifica(char stringa[]){
  int id_mess,coord_x,coord_y,id_creatore;
  char titolo[TITLELEN];
  
  id_mess=atoi(strtok(stringa,"$"));
  coord_x=atoi(strtok(NULL,"$"));
  coord_y=atoi(strtok(NULL,"$"));
  id_creatore=atoi(strtok(NULL,"$"));
  strcpy(titolo,strtok(NULL,"$"));
  
  pthread_mutex_lock(&mutex_users);
  
  users[id_creatore].richiesta=inserisci_testa_richiesta(users[id_creatore].richiesta,id_mess,coord_x,coord_y,get_indice(),titolo);
  pthread_mutex_unlock(&mutex_users);  
}


/***************************************************************************************************************************************
* Funzione che data una lista di messaggi,	      										       *
****************************************************************************************************************************************/
struct notifica_richiesta* inserisci_testa_richiesta(struct notifica_richiesta* lista,int id_mess,int coord_x,int coord_y,int mio_id,char titolo[]){
 
 if(lista!=NULL)
 {
  
    if((lista->id_mittente!=mio_id) | (lista->id_messaggio!=id_mess)){
        lista->next=inserisci_testa_richiesta(lista->next,id_mess,coord_x,coord_y,mio_id,titolo);}
     else{
         return lista;
      }
 }else{
 
   struct notifica_richiesta* nuovo=(struct notifica_richiesta*)malloc(sizeof(struct notifica_richiesta));
  
   nuovo->next=lista;
   nuovo->id_mittente=mio_id;
   nuovo->id_messaggio=id_mess;
   nuovo->coord_x=coord_x;
   nuovo->coord_y=coord_y;
   strcpy(nuovo->titolo,titolo);
   
   return nuovo;
 }
 return lista;
}



/***************************************************************************************************************************************
* Funzione che data una lista di messaggi,	       *
****************************************************************************************************************************************/
void stampa_messaggi(struct message* lista){
  struct risposta messaggio;
  struct message* temp=lista;
  messaggio.tipoRisposta=1;
  int id=get_indice();
  
 while(temp!=NULL){
  
  if(temp->visibility==1){
    sprintf(messaggio.stringa,"%d$%d$%d$%d$%s$%s$%s$%s",temp->visibility,temp->idmsg,temp->x,temp->y,temp->titolo,users[temp->indice_creatore].nickname,
                                                                                                                        temp->corpo,ctime(&temp->data_creazione));
    spedisci_messaggio(messaggio);
  }else{
   if(check_lettore(temp->lettori)==1 | temp->indice_creatore==id ){ //Ritorna 1 se l'utente è abilitato
    sprintf(messaggio.stringa,"%d$%d$%d$%d$%s$%s$%s$%s",temp->visibility,temp->idmsg,temp->x,temp->y,temp->titolo,users[temp->indice_creatore].nickname,
                                                                                                                        temp->corpo,ctime(&temp->data_creazione));
    spedisci_messaggio(messaggio);
   }
   }

    temp=temp->next;
  }  
 
}

/***************************************************************************************************************************************
*	                                                                                                                               *
****************************************************************************************************************************************/
void stampa_messaggi_privati(struct message* lista){
  struct risposta messaggio;
  struct message* temp=lista;
  messaggio.tipoRisposta=1;
  int id=get_indice();
  
 while(temp!=NULL){
  
   if((temp->visibility==0)&(check_lettore(temp->lettori)==0)&(temp->indice_creatore!=id))
   {
     sprintf(messaggio.stringa,"%d$%d$%d$%s$%s$%s$%d",temp->idmsg,temp->x,temp->y,temp->titolo,users[temp->indice_creatore].nickname,
                                                                                                              ctime(&temp->data_creazione),temp->indice_creatore);
     spedisci_messaggio(messaggio);
   }
     temp=temp->next;
 }  
}



/***************************************************************************************************************************************
* Funzione che data una lista di messaggi,	       *
****************************************************************************************************************************************/
int check_lettore(struct idusers* lettore){

  int id=get_indice();
   
  while(lettore!=NULL){
    if(lettore->id==id){
        return 1;
    }
     else
       lettore=lettore->next;
  }

  return 0;   

}



/***************************************************************************************************************************************
* Funzione che data una lista di users, per ogni utente, spedisce un messaggio al client contenente una stringa che descrive l'utente  *
* presente. 															       *
****************************************************************************************************************************************/
void stampa_users(struct idusers* lista){
  struct risposta messaggio;
  struct idusers* temp=lista;
  messaggio.tipoRisposta=1;

  while(temp!=NULL){
    sprintf(messaggio.stringa,"Utente: %s in posizione:%d,%d\n",users[temp->id].nickname,users[temp->id].x,users[temp->id].y);
    
    spedisci_messaggio(messaggio);

    temp=temp->next;
  }   
}


struct notifica_richiesta* rispondi_notifica(struct notifica_richiesta* lista){
   struct risposta invia_notifica; 
   struct notifica_richiesta* temp=lista; 
   struct notifica_richiesta* prev=lista;
   char nick[31];
   char titolo[TITLELEN];
   int ret;
   int i=get_indice();
   /*INVIO AL CLIENT LE NOTIFICHE A CUI PUO' DECIDERE DI RISPONDERE*/
   
   while(temp!=NULL)
   {

      /*Controllo che il messaggio id_mess esiste ancora*/
      pthread_mutex_lock(&mappa[temp->coord_x][temp->coord_y].mutexcella);
    
      ret=check_mess(mappa[temp->coord_x][temp->coord_y].headmsg,temp->id_messaggio);
     
      pthread_mutex_unlock(&mappa[temp->coord_x][temp->coord_y].mutexcella);
       
      if(ret==1)
      {
       /*Ricavo il nickname di chi mi ha chiesto di leggere il messaggio*/
       nick[0]='\0';      
       strcpy(nick,users[temp->id_mittente].nickname);  
    
       invia_notifica.stringa[0]='\0';
       sprintf(invia_notifica.stringa,"%s$%s$%d$%d$%d$%d",nick,temp->titolo,temp->id_mittente,temp->coord_x,temp->coord_y,temp->id_messaggio);
       spedisci_messaggio(invia_notifica);
      
       temp=temp->next;
      }
      else{
       users[temp->id_mittente].risposta=inserisci_testa_risposta(users[temp->id_mittente].risposta,get_indice(),users[get_indice()].nickname,temp->titolo,'c');
           prev=temp->next;
           struct notifica_richiesta* app=temp;
           temp=temp->next;
           free(app);  /*aggiungi alla lista risposta associata all'utente, l'informazione per indicare che il mess è stato cancellato*/

           strcpy(invia_notifica.stringa,"cancellato");
           spedisci_messaggio(invia_notifica);    
      }
      
   }   
  
   return prev;
}



/*RITORNA 1  SE C'è, 0 altrimenti*/
int check_mess(struct message* lista,int id_mess){

int ret=0;

 if(lista!=NULL)
 {
   if(lista->idmsg==id_mess)
     return 1;
   else
     ret=check_mess(lista->next,id_mess);
 
 }
  
  return ret;
}
/********************************************************************************************************************************************
*   Funzione che inserisce un elemento di tipo struct idusers in testa alla lista passata. Nel caso in cui abbiamo un primo inserimento in  *
*   lista, passare il parametro testa=NULL.                                                                                                 *
********************************************************************************************************************************************/
struct idusers* inserisci_testa_user(struct idusers* testa, int nuovo_id){
  
  struct idusers* nuovo=(struct idusers*)malloc(sizeof(struct idusers));
  
  nuovo->next=testa;
  nuovo->id=nuovo_id;

  return nuovo;

} 


/********************************************************************************************************************************************
*   Funzione che elimina un elemento dalla lista di idusers                                                                                 *
********************************************************************************************************************************************/
struct idusers* cancella_user(struct idusers* lista,int id){

 if(lista!=NULL){
    if(lista->id==id){
       struct idusers* temp=lista;
       lista=lista->next;
       free(temp);
       
    }else
      lista->next=cancella_user(lista->next,id);
 }

 return lista;
}

/* ================ LISTE MESSAGGI ==================== */

/********************************************************************************************************************************************
*   Funzione che inserisce un elemento di tipo struct messaggi in testa alla lista passata. Nel caso in cui abbiamo un primo inserimento in  *
*   lista, passare il parametro testa=NULL.                                                                                                 *
********************************************************************************************************************************************/
struct message* inserisci_testa_msg(struct message* testa, char titolo[TITLELEN], char corpo[BODYLEN], int visibility,int* flag){
  
  struct message* nuovo=(struct message*)malloc(sizeof(struct message));
  *flag=0;
  if(nuovo!=NULL)
  {
	  *flag=1;
	  nuovo->next=testa;
	  strcpy(nuovo->titolo,titolo);
	  strcpy(nuovo->corpo,corpo);
	  nuovo->idmsg=get_idmsg();
	  time(&nuovo->data_creazione);
	  
	  pthread_mutex_lock(&mutex_users);
	  nuovo->x=users[get_indice()].x;
	  nuovo->y=users[get_indice()].y;
	  pthread_mutex_unlock(&mutex_users);
	    
	  nuovo->indice_creatore=get_indice();
	  
	  nuovo->lettori=NULL;
	  
	  nuovo->visibility=visibility;
	  
	  
	  return nuovo;
  }
  else
      return testa;
  
} 



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void elabora_risposta_notifiche(){
   struct risposta risp;

   char titolo[31];
   char car[2];
   int id_ut,coord_x,coord_y,id_mess;

   ricevi_messaggio(&risp);
   if(risp.carattere=='o'){   
   titolo[0]='\0';
   car[0]='\0';
   strcpy(titolo,strtok(risp.stringa,"$"));
   id_ut=atoi(strtok(NULL,"$"));
   strcpy(car,strtok(NULL,"$"));
   coord_x=atoi(strtok(NULL,"$"));
   coord_y=atoi(strtok(NULL,"$"));
   id_mess=atoi(strtok(NULL,"$"));
   
   pthread_mutex_lock(&mutex_users);
   users[get_indice()].richiesta=cancella_richiesta(users[get_indice()].richiesta,id_mess,id_ut);   
   users[id_ut].risposta=inserisci_testa_risposta(users[id_ut].risposta,get_indice(),users[get_indice()].nickname,titolo,car[0]);    
  
   pthread_mutex_unlock(&mutex_users);
   
   if(car[0]=='s'){
       pthread_mutex_lock(&mappa[coord_x][coord_y].mutexcella);
      
       aggiungi_reader_msg(mappa[coord_x][coord_y].headmsg,id_mess,id_ut);
      
       pthread_mutex_unlock(&mappa[coord_x][coord_y].mutexcella);
     }
   }
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void aggiungi_reader_msg(struct message* testa,int id_mess,int id_ut)
{

  if(testa!=NULL)
  {
    if(testa->idmsg==id_mess)
      testa->lettori=inserisci_reder_testa(testa->lettori,id_ut);
    else
      aggiungi_reader_msg(testa->next,id_mess,id_ut); 
  }

}


/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
struct idusers* inserisci_reder_testa(struct idusers* testa,int id_ut)
{
   struct idusers* nuovo=(struct idusers*)malloc(sizeof(struct idusers));
  
   nuovo->next=testa;
   nuovo->id=id_ut;
   return nuovo;

}


/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
struct notifica_risposta* inserisci_testa_risposta(struct notifica_risposta* lista,int id_utente,char nick[],char titolo[],char risp){
   struct notifica_risposta* nuovo=(struct notifica_risposta*)malloc(sizeof(struct notifica_risposta));
  
   nuovo->next=lista;
   nuovo->id_creatore=id_utente;
   strcpy(nuovo->nick_creatore,nick);
   strcpy(nuovo->titolo,titolo);
   nuovo->ris=risp;
   
   return nuovo;
   
}
/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
struct notifica_richiesta* cancella_richiesta(struct notifica_richiesta* lista,int idmsg,int mitt){

 if(lista!=NULL){
    if(lista->id_messaggio==idmsg && lista->id_mittente==mitt){
       struct notifica_richiesta* temp=lista;
       lista=lista->next;
       free(temp);
    }else
      lista->next=cancella_richiesta(lista->next,idmsg,mitt);
 }

 return lista;
}



/*******************************************************************************************************
* Funzione che stampa nel file di log il messaggio passato in input concatenandolo al timestamp        *
********************************************************************************************************/
void printlog(const char* msg)
{

 int logfile;
 time_t tim;
 char* ip_p=(char*)pthread_getspecific(key_tsd);
 char messaggio[100],string[200],ip[100];

 strcpy(messaggio,msg);
 strcat(messaggio," ");

 /* Apertura File di Log*/
  logfile=open("Log.txt",O_WRONLY|O_CREAT|O_APPEND,S_IRWXU|S_IRWXG|S_IRWXO);
  if(logfile<0)
     printf("Impossibile aprire/creare il file di log\n");
  else
  { 
    /* Formatto la stringa time stamp */
    time(&tim); 
    strcpy(string,ctime(&tim));
    string[strlen(string)-1]=' ';

    /* Concateno il messaggio al time stamp */
    strcat(string,messaggio);

    if(ip_p!=NULL)
    {
       strcpy(ip,ip_p);
       strcat(string,ip);
    }

    strcat(string,"\n");

    /* Scrivo timestamp + messaggio sul file di log */
    if(write(logfile,string,strlen(string))<0)
       perror("Errore scrittura file log\n");

    /* Stampo il messaggio a video */
    if(write(STDOUT_FILENO,string,strlen(string))<0)
       perror("Errore stampa a video file log\n"); 
    
  }

  close(logfile);

}



/*************************************
**************************************/
void logout()
{
   int id=get_indice();
   char messaggio[100];
   int x,y;
   sprintf(messaggio,"Richiesta di connessione annullata IP:");

   if(id!=-1){
   
     /*sblocco gli eventuali mutex lockati*/
     sblocca_tutti_mutex();
   
     pthread_mutex_lock(&mutex_users);
     x=users[id].x; 
     y=users[id].y; 
     users[id].connection_status=0;
     sprintf(messaggio,"L'utente %s si è disconnesso (ultima posizione(%d,%d))",users[get_indice()].nickname,x,y);
     pthread_mutex_unlock(&mutex_users);
   
     pthread_mutex_lock(&mappa[x][y].mutexcella);
   
     mappa[x][y].headuser=cancella_user(mappa[x][y].headuser,id);
    
     pthread_mutex_unlock(&mappa[x][y].mutexcella);

     int* temp=(int*)pthread_getspecific(key_indice);
     free(temp);    
    } 
    printlog(messaggio);

    pthread_exit(NULL);
  
}

/********************************************************************************************************
*********************************************************************************************************/
void sblocca_tutti_mutex()
{
  int i,j;
  
  /*Sblocco mutex matrice*/
  for(i=0;i<RIGHE;i=i+1)
      for(j=0;j<COLONNE;j=j+1)
          pthread_mutex_unlock(&mappa[i][j].mutexcella);
          
  pthread_mutex_unlock(&lastidmsgmutex);
  pthread_mutex_unlock(&mutex_users);
  pthread_mutex_unlock(&mutex_file);
  pthread_mutex_unlock(&mutex_varUtenti);
        
}


/********************************************************************************************************
* Ritorna una stringa contente l'idrizzo ip del client che ha generato il thread in cui viene chiamata  *
* la funzione                                                                                           *
********************************************************************************************************/                      
char* get_ip_string()
{   
 return (char*)pthread_getspecific(key_tsd);                                                                             
}



/*******************************************************************************************************
* Funzione che genera un identificativo univoco per i messaggi                                         *
********************************************************************************************************/
int get_idmsg() 
{

 int temp;
 pthread_mutex_lock(&lastidmsgmutex);

 lastidmsg=lastidmsg+1;
 temp=lastidmsg;

 pthread_mutex_unlock(&lastidmsgmutex); 

 return temp;

}



/******************************************************************************************************************************
* Restituisce l'indice del Vettore degli utenti in cui sono memorizzate le informazioni sull'utente che ha generato il thread *
*******************************************************************************************************************************/
int get_indice()
{
  int* temp=(int*)pthread_getspecific(key_indice);
  if(temp!=NULL)
    return *temp;
  else
    return -1;  
}



/******************************************************************************************************************************
* Restituisce il socket tramite cui comunicare con il client                                                                  *
*******************************************************************************************************************************/
int get_socket()
{
  int* temp=(int*)pthread_getspecific(key_socket);
  return *temp;  
}



/*******************************************************************************************************
* Chiamato alla ricezione di di un SIGINT, effettua una chiusura pulita del server                     *
********************************************************************************************************/
void clean_up(int sig)
{
  close(descrittore_socket);
  printlog("Server Terminato\n");
  exit(1);
}



/********************************************************************************************************
*                                                                                                       *
********************************************************************************************************/
void spedisci_messaggio(struct risposta messaggio)
{
  int socket=get_socket();
  if(write(socket,&messaggio,sizeof(struct risposta))<0)
  {
    printlog("Impossibile mandare il messaggio.\n");
    close(socket);
    logout();
  }  
  return;
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void ricevi_messaggio(struct risposta *risp){
	int quanti;
        int socket=get_socket();
        int ret;
        struct timeval timeout;
        fd_set set; /* Insieme dei file descriptor che la select deve deve monitorare */
        
        FD_ZERO(&set);        /* Iniziallizza l'insime dei file descriptor */
        FD_SET(socket, &set); /* Aggiungo il socket da controllare nell'insieme */

        timeout.tv_sec = 180; /* aspetto 180 sec(3 min) e 0 millisecondi*/
        timeout.tv_usec = 0; /* millisecondi*/
        
        ret = select(socket+1, &set, NULL, NULL,&timeout);
        if(ret == -1) /* Se è avvenuto un errore disconnetto il client*/
           logout();
        
        else if(ret == 0) /* Se il tempo è scatuto disconnetto il client */
          logout();
          
        else /* E' stato scritto qualcosa nella socket */
        {
	   if ((quanti=read(socket,risp,sizeof(struct risposta)))<0)
	   {
		printlog("Impossibile leggere il messaggio.\n");
                close(socket);
                logout();
	   }
           if((risp->tipoRisposta==2)&(risp->carattere=='h'))
           {
             close(socket);
             logout();
           }
        }
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void trova_messaggi(){  //QUESTA
  int i,j,x,y,raggio,arri,arrj,parti,partj;
  int id=get_indice();
  
 pthread_mutex_lock(&mutex_users);
 
 raggio=users[id].raggio;
 x=users[id].x;
 y=users[id].y; 

  pthread_mutex_unlock(&mutex_users);
  
 if(x-raggio <=0)
   parti=0;
 else
  parti=x-raggio;

 if(y-raggio <=0)
   partj=0;
 else
  partj=y-raggio;
 
 if(x+raggio >=RIGHE)
   arri=RIGHE;
 else
  arri=x+raggio;

 if(y+raggio >=COLONNE)
   arrj=COLONNE;
 else
  arrj=y+raggio;

 for (i=parti;i<=arri;i=i+1){
    for(j=partj;j<=arrj;j=j+1){  
      pthread_mutex_lock(&(mappa[i][j].mutexcella));
      
      stampa_messaggi_inviati(mappa[i][j].headmsg,id);
      
      pthread_mutex_unlock(&(mappa[i][j].mutexcella));
    }
  }
}




/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void verifica_cancella(){//QUESTA
  
  struct risposta comando;
  int id_mess,coord_x,coord_y,flag=0;
  
  do{
     ricevi_messaggio(&comando);

     
     if(strcmp(comando.stringa,"FINE")!=0){//Se il client ha inviato una stringa diversa da "FINE"

        id_mess=atoi(strtok(comando.stringa,"$"));
        coord_x=atoi(strtok(NULL,"$"));
        coord_y=atoi(strtok(NULL,"$"));
        
        pthread_mutex_lock(&mappa[coord_x][coord_y].mutexcella);
        
        //Verifico che il messaggio sia presente nella lista e se è così, lo cancello
        mappa[coord_x][coord_y].headmsg=cancella_messaggio(mappa[coord_x][coord_y].headmsg,id_mess,&flag);
        
        pthread_mutex_unlock(&mappa[coord_x][coord_y].mutexcella);
        
        if(flag==1)//Messaggio cancellato
          comando.carattere='s';
        else
          comando.carattere='n';
          
        spedisci_messaggio(comando);
     }
  
  }while(strcmp(comando.stringa,"FINE")!=0);
  
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
struct message* cancella_messaggio(struct message* lista,int id,int* flag){

 if(lista!=NULL){
    if(lista->idmsg==id){
       struct message* temp=lista;
       lista=lista->next;
       free(temp);
       *flag=1;
    }else
      lista->next=cancella_messaggio(lista->next,id,flag);
 }

 return lista;
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void stampa_messaggi_inviati(struct message* lista,int id){ ///QUESTA
  struct risposta messaggio;
  messaggio.tipoRisposta=1;  
  
  while(lista!=NULL){
  
     if(lista->indice_creatore==id){
        sprintf(messaggio.stringa,"%d$%d$%d$%s$%s",lista->idmsg,lista->x,lista->y,lista->titolo,lista->corpo);
        spedisci_messaggio(messaggio);  
     }
     
     lista=lista->next;

  }

}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void* start_thread_elimina_scaduti(void* args)
{
  int i,j;
  sigset_t insime_segnali;
  time_t oggi;
 
  /* Maschera dei segnali segnali bloccati per il thread */
  sigfillset(&insime_segnali);                   /* Inizializzo l'insime dei segnali con l'insime UNIVERSO */ 
  sigdelset(&insime_segnali,SIGINT);             /* Escludo dall'insieme dei segnali SIGINT */
  pthread_sigmask(SIG_SETMASK,&insime_segnali,NULL); /* Setto come insieme dei segnali bloccati "insime_segnali"*/ 
  
  time(&oggi);
  
  while(1)
  {
    
    sleep(300);
    for(i=0;i<RIGHE;i++)
    {
      for(j=0;j<COLONNE;j++)
      {
          pthread_mutex_lock(&(mappa[i][j].mutexcella));
     
            mappa[i][j].headmsg=elimina_scaduti(mappa[i][j].headmsg,oggi);
 
          pthread_mutex_unlock(&(mappa[i][j].mutexcella));
      }
    }
  }
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
struct message* elimina_scaduti(struct message* testa,time_t time1)
{
  if(testa!=NULL)
  {
    if(difftime(testa->data_creazione,time1)>=36000.0) /* differenza dei tempi in secondi*/
    {
       struct message* temp=testa;
       testa=testa->next;
       free(temp);
    }
    else
      testa->next=elimina_scaduti(testa->next,time1);
  }
  
  return testa;
}


/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void elenca_notifiche(){
   struct risposta risp;
   struct notifica_risposta* temp;
   
   pthread_mutex_lock(&mutex_users);
   temp=users[get_indice()].risposta;
      
   while(temp!=NULL){
      
      sprintf(risp.stringa,"%s$%s$%c",temp->nick_creatore,temp->titolo,temp->ris);
      spedisci_messaggio(risp);
      temp=temp->next;
   }
   
   elimina_notifiche_visualizzate(users[get_indice()].risposta);
   users[get_indice()].risposta=NULL;
   
   pthread_mutex_unlock(&mutex_users);
   
   strcpy(risp.stringa,"FINE");
   spedisci_messaggio(risp);  
}



/*************************************************************************************************************
*                                                                                                            *
**************************************************************************************************************/
void elimina_notifiche_visualizzate(struct notifica_risposta* testa)
{
   if((testa!=NULL)&&(testa->next!=NULL))
    {
      elimina_notifiche_visualizzate(testa->next);
      free(testa); 
  } 
     
}
