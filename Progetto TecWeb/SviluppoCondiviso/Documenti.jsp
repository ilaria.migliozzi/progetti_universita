<%@page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Documenti</title>

        <% 
              String id_aut = (String)session.getAttribute("id");
              String user = (String)session.getAttribute("username");
              ResultSet rs = (ResultSet)request.getAttribute("rs");
                    
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione � scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }
              if(rs == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","index.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }             
                %>
        <script>
            function chiamaVersioning(){
                var x = event.target.selectedIndex;
                var maxver = event.target.getAttribute("data-maxversion");
                var title = event.target.getAttribute("data-titolo");
                var id = event.target.getAttribute("data-iddoc");
                x++;
                document.getElementById("versione_richiesta").setAttribute("value", x);
                document.getElementById("maxversion").setAttribute("value",maxver);
                document.getElementById("titolodoc").setAttribute("value" ,title);
                document.getElementById("iddoc").setAttribute("value" ,id);
                document.getElementById("formVersion").submit();
            }
            
        </script>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Repository" />
         </jsp:include>
	  <div id="content">
              <a class="button" href="<%= response.encodeURL("/SviluppoCondiviso/NuovoDocumento.jsp")%>" style="float:right; margin-right: 9.2em"> + Nuovo Documento</a>
              <br/><br/>
              <%
                if(!rs.next()){%>
                    <p>Non hai documenti in repository</p>
                <%}else{
                %>
            <table cellspacing='0'> <!-- cellspacing='0' � importante, deve rimanere-->
                    <tr><th>Titolo</th><th>Ultima Versione</th><th>Propietario</th><th>Data Creazione</th><th>Operazioni</th></tr> <!-- Table Header -->
                    <%
                    int i = 1;
                    boolean posseduto = false;
                            
                    do{
                        posseduto = false;
                        if( rs.getString("IdCreatore").equals(session.getAttribute("id"))){
                            posseduto = true;
                        }
                       
                        if(i%2 == 0){%>
                            <tr>
                    <% }else{ %>
                            <tr class='even'>
                    <% } %>        
                        <td><%= rs.getString("Titolo") %></td><td><%= rs.getString("Versione") %></td><td><%= rs.getString("UsernameCreatore") %></td><td><%= rs.getString("DataCreazione") %></td> <td> <a href="<%= response.encodeURL("/SviluppoCondiviso/ApriInEditor?iddoc="+rs.getString("IdDocumento")+"&versione="+rs.getString("Versione"))%>">Apri</a><% if(posseduto){%><a  href="<%= response.encodeURL("/SviluppoCondiviso/AutoriMancanti?iddoc="+rs.getString("IdDocumento"))%>">Agg. Autore</a> <%}%> <label>Storico</label><select oninput="chiamaVersioning();" data-titolo="<%= rs.getString("Titolo") %>"  data-iddoc="<%= rs.getString("IdDocumento")%>" data-maxversion="<%=rs.getString("Versione")%>"> <%int k =Integer.parseInt(rs.getString("Versione")); int j; for(j=1;j<=k;j++){%><option value="<%=j%>"><%=j%></option><% } %></select> </td></tr>
                     <%
                        i++;
                    }while(rs.next()); %>                   
</table>     
            <% } %>

            <form id="formVersion" action="<%= response.encodeURL("/SviluppoCondiviso/GetVersion")%>">
                <input id="titolodoc" name="titolodoc" hidden />
                <input id="maxversion" name="maxversion" hidden />
                <input id="iddoc" name="iddoc" hidden />
                <input id="versione_richiesta" name="versione_richiesta" hidden />
            </form>
        <br/>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>