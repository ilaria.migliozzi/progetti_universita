<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Home</title>
</head>
<body>
            <% 
              String id_aut = id_aut = (String)session.getAttribute( "id" ); 
              String user = (String)session.getAttribute( "username" );
                %>
<div id="container" >
        <%@include file="elem/Header.html" %>     
                        <% if(id_aut != null) { %> <%-- fine scriptlet per inserire template statico --%>
                            <jsp:include page="elem/NavigationBar.jsp">
                                <jsp:param name="selected" value="Home" />
                            </jsp:include>
                        <% } //Fine if 
                          else{ %>
                           <%@include file="elem/NavigationBarNoLog.html" %>
                        <% } //Fine if %> <%-- Fine scriptlet --%>
	
	<div id="content">
              <% if(id_aut != null) { %> <%-- fine scriptlet per inserire template statico --%>
		<h2>Benvenuto <%= user %>!</h2>
              <% } //Fine if %>
		<p>Il sistema in descrizione permette di sviluppare un qualsiasi tipo di documento in maniera condivisa.
                    Pi&ugrave; utenti possono accedere ad esso e modificarne parti, senza contrastare l'uno il lavoro dell'altro, arrivando alla stesura definitiva del documento.
                   Il sistema permette di recuperare, in ogni momento, una qualsiasi versione precedente del file, dalla quale creare una nuova versione dello stesso.
                </p>
                <p><strong>Nota: </strong>Per utilizzare al meglio l'applicazione � consigliato abilitare nel proprio browser l'uso dei cookies.
                   Nel caso in cui i cookies non fossero accettati dal browser � comunque possibile utilizzare 
                   l'applicazione accedendo alle risorse esclusivamente tramite i link e i bottoni dell'interfaccia. 
               </p>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>