<!DOCTYPE html>
<html>
    <head>
        <title>Editor</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
<% 
              String id_aut = (String)session.getAttribute("id");
              String user = (String)session.getAttribute("username");
              String corpo = (String)request.getAttribute("corpo");
              String id_document = (String)request.getAttribute("id_document");
              String version = (String)request.getAttribute("version");              
              String wof = (String) request.getAttribute("openinwf");              
                  
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione � scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
                }
              if(corpo == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Documenti.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
                }
              if(wof==null){
                 wof="";
              }   
                                                        
                %>  
    <script>
        var id_aut = "<%= id_aut %>";
        var username = "<%= user %>";
        var version = "<%= version %>";
        var id_doc = "<%= id_document %>";
        var wof='<%=wof%>';
        var objDoc;
        var before_modify=[];
        var editorChlds;  
        var new_block=0;
        var text_add;
        var text_mod;
        var text_del;        
        var text_relation;
        var xmlhttp;
        var ok;
        var hiddenEdit;
        
     function initInterval(){
         ok=true;
         if (window.XMLHttpRequest){// codice per IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp=new XMLHttpRequest();
         }else{// codice per IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         }
  
  /*Setto l'intervallo della funzione che controlla il numero della versione attuale del documento*/
         window.setInterval(version_control, 120000);

         hiddenEdit=document.getElementById("hiddenEditor");
         hiddenEdit.innerHTML=wof; 

     }        
     
    function version_control(){
        var text;
        var span, div;
 
        xmlhttp.onreadystatechange=function(){
             if(xmlhttp.readyState==4 && xmlhttp.status==200){ //controllo che sia arrivata la risposta e che il server abbia
                                                                         //risposto OK
             resp=xmlhttp.responseText;
             if(resp!="ko" && resp!=version){
                 ok=false;
                 text=document.createTextNode(resp);
                 span=document.getElementById("num_version");
                 span.replaceChild(text, span.firstChild);
                 div=document.getElementById("ajax");
                 if(div.style.display=="none"){
                     div.style.display="inline";
                 }
             }
           }    
       }
       xmlhttp.open("GET","/SviluppoCondiviso/VersionCheck?idDoc="+id_doc,true);
       xmlhttp.send();
    }
        
/*
  Funzione che parte quando � premuto il tasto MODIFICA. In tale funzione
  creo un array associativo (riferendosi tramite id blocco) contenente
  l'intero documento inviato nella risposta HTTP, quindi il documento nella sua
  forma originaria. 
  Per cui, ogni elemento dell'array contiene il testo di un singolo blocco
  a cui ci si pu� riferire tramite l'id.
*/        
        function initDoc(){

            text_add=document.getElementById("added");
            text_mod=document.getElementById("modified");
            text_del=document.getElementById("deleted");
            text_relation=document.getElementById("relationship");
            
            text_add.value="<body>";
            text_mod.value="<body>";
            text_del.value="<body>";
            text_relation.value="<body>";
            
            objDoc=document.getElementById("editor");
  /*Chiamo la funzione che mi riempie il vettore associativo*/            
            if(wof==""){
                editorChlds=objDoc.childNodes;
                
            }else{
                editorChlds=hiddenEdit.childNodes; 
            }
            
            before_modify=createAssociativeArray(editorChlds);
            editorChlds=objDoc.childNodes;
            if(wof!="")
                set_editable(editorChlds);
 /*Se l'utente clicca su "Modify", si presentano i tasti salva e commit, modify viene nascosto*/           
            document.getElementById("modifyButton").style.display="none";
            document.getElementById("saveButton").style.display="inline";
            document.getElementById("commitButton").style.display="inline";
  


        }

/*Funzione usata nel caso in cui l'editor apre un file da workingFolder. in quel caso la funzione 
 *Create Associative array lavora sul contenuto della form nascosta (usata per memorizzare la versione originale del file
 *per permettere il commit).
 *Quindi sar� set_editable a porre i div nel div: "editor" pari a true.*/
    function set_editable(nodes){
        var j;
           for(j=0;j<nodes.length;j++){
             if(nodes[j].tagName=="DIV"){  
            /*Rendo tutti i paragrafi Editabili*/                  
                  nodes[j].setAttribute("contenteditable","true");
                  
 /*Setto l'attributo Color della propriet� CSS outline a verde, per distinguere visivamente i paragrafi dell'autore corrente */         
                  if(nodes[j].getAttribute("data-id_autore")==id_aut){
                     nodes[j].style.outlineColor='#00ff00';               
                  }
             }
               if(nodes[j].nodeType==3){
                   nodes[j].parentNode.removeChild(nodes[j]);
               }
         
         }
      }
 
    function keypressed(){ 
        var newnode;
        var is_conflict;
        var id,def_pre,def_post,title,id_autore;  
        
        if(event.keyCode==38 && event.shiftKey){//Scendi Su nell'editor
            if(event.target.previousSibling!=null){ 
              if(event.target.previousSibling.style.display!="none") //Se il fratello precedente ha display != none, do il focus a lui
                event.target.previousSibling.focus();
              else
                  if(event.target.previousSibling.previousSibling!=null) //altrimenti, se ha preprefratello, do il focus a lui.
                      event.target.previousSibling.previousSibling.focus();
            }
        }
        
        if(event.keyCode==40 && event.shiftKey){ //Scendi Gi� nell'editor
            if(event.target.nextSibling!=null){ 
              if(event.target.nextSibling.style.display!="none") //Se il fratellosuccessivo ha display != none, do il focus a lui
                event.target.nextSibling.focus();
              else{
                  if(event.target.nextSibling.nextSibling!=null){ //altrimenti, se ha postpostfratello, do il focus a lui.
                      event.target.nextSibling.nextSibling.focus();
                      
                   }
              }
            }
        }
        
        if(event.keyCode==8){ //Cancellazione di un blocco
            if(event.target.innerHTML.length==0){ 
                
              if(event.target.previousSibling!=null)
                 event.target.previousSibling.focus();
              else
                 event.target.nextSibling.focus();

              id= event.target.id;
              title= event.target.title;
              id_autore=event.target.getAttribute("data-id_autore");
              if(id_autore==id_aut){
                  is_conflict="false";
                  objDoc.removeChild(event.target);
              }else{
                  is_conflict="true";
                  event.target.style.display="none";
              }
              def_pre=event.target.getAttribute("data-def_pre");
              def_post=event.target.getAttribute("data-def_post");                                                
              text_del.value +="<div id="+id+ " title="+title+" contenteditable=\"true\" data-id_autore="+id_autore+" data-is_conflict="+is_conflict+" data-def_pre="+def_pre+" data-def_post="+def_post+" onkeydown=\"if(event.keyCode==13 || event.keyCode==8 || event.keyCode==38 || event.keyCode==40) keypressed()\"></div>";

            }
        }
     if(event.keyCode=="13" && event.shiftKey && event.target.title!=username){
         event.preventDefault();       
         
        if(event.target.getAttribute("data-id_autore")!=id_aut){
     
    /*Controllo che il blocco su cui � stato generato l'evento � l'ultimo o se il fratello � stato creato da un'autore
     *diverso dall'autore corrente. In questi casi creo un nuovo blocco.
     *Ma se il blocco corrente ha come fratello un blocco dello stesso autore, l'unica azione che faccio � dare 
     *il focus al prossimo fratello. (come se avesse premuto Shift + Down)*/ 
          if(event.target.nextSibling==null || event.target.nextSibling.getAttribute("data-id_autore")!=id_aut){  
            new_block=new_block+1;
            newnode=document.createElement('div');
            document.getElementById("editor").insertBefore(newnode,event.target.nextSibling);
            newnode.setAttribute("id", "new_block"+new_block);
            newnode.setAttribute("contenteditable","true");
            newnode.setAttribute("title",username);
            newnode.setAttribute("data-type","added");
            newnode.setAttribute("data-id_autore",id_aut);
            newnode.addEventListener("keydown", keypressed);
            newnode.style.outlineColor='#00ff00';        
            newnode.focus();
          }else{
              event.target.nextSibling.focus();
          }//Chiusura Else
        }//Chiusura if che controlla se il blocco che ha generato l'evento � l'ultimo.
      }//Chiusura if che controlla il tipo di evento (Shift + Invio).
   
  }//Chiusura Funzione KeyPressed();       

    function parseDocument(){
           var i;
           var id;     
           var innerValue,def_pre,def_post,title,id_autore,is_conflict;
           
           for(i=0;i<editorChlds.length;i++){
            if(editorChlds[i].tagName=="DIV" && editorChlds[i].id){ //Scarto eventuali figli inutili (Commenti,Spazi,nl) e nl all'interno del blocco

    /*Aggiorno il next e il prev di ogni blocco, rispetto alla struttura del file al momento del salvataggio*/   
              if(editorChlds[i].previousSibling!=null)
                 editorChlds[i].setAttribute("data-def_pre", editorChlds[i].previousSibling.id);
                   else
                 editorChlds[i].setAttribute("data-def_pre", "0");
                   
              if(editorChlds[i].nextSibling!=null)
                 editorChlds[i].setAttribute("data-def_post", editorChlds[i].nextSibling.id);
                   else
                 editorChlds[i].setAttribute("data-def_post", "0");
            
              if(event.target.id=="commitButton"){ //OPERAZIONI DA FARE SOLO SE HA PREMUTO COMMIT!!!
  /*Per ogni blocco attualmente esistente nell'editor, prendo tutte le sue informazioni*/ 
                 
                 id=editorChlds[i].id;
                 innerValue=editorChlds[i].innerHTML;
                 title= editorChlds[i].title;
                 id_autore=editorChlds[i].getAttribute("data-id_autore");
                 def_pre=editorChlds[i].getAttribute("data-def_pre");
                 def_post=editorChlds[i].getAttribute("data-def_post");
                 
            
        /*Controllo se il blocco corrente � un "nuovo blocco*/  
                 if(editorChlds[i].getAttribute("data-type")=="added" ){
                   
                     text_add.value +="<div id="+id+ " title="+username+" contenteditable=\"true\" data-id_autore="+id_autore+" data-def_pre="+def_pre+" data-def_post="+def_post+">"+innerValue+"</div>";

                 }else{
                  
                   if(innerValue!="" && before_modify[id]!=innerValue){
                     if(id_autore!=id_aut)
                        is_conflict="true";
                     else
                        is_conflict="false";

                     text_mod.value +="<div id="+id+" title="+title+" data-is_conflict="+is_conflict+" contenteditable=\"true\" data-id_autore="+id_autore+" data-def_pre="+def_pre+" data-def_post="+def_post+">"+innerValue+"</div>";
      
                    }//Chiusura if
                   
                 }//Chiusura Else
              
                text_relation.value += "<div id="+id+" data-def_pre="+def_pre+" data-def_post="+def_post+"></div>";
              }//Chiusura controllo (CommitButton)
          
            }//Chiusura primo if (Scarto eventuali figli inutili)

         }//Chiusura For
      if(event.target.id=="commitButton"){
         if(ok==false){
             var messaggio="La versione che hai moficato � obsoleta.Se effettui il commit non � garantito il salvataggio di tutte le modifiche!\n\nPremi 'Ok' per continuare o 'Annulla' per poter scaricare manualmente la nuova versione!";
             if (confirm(messaggio) == true) {
               commit();
             }
         }else
           if(text_add.value == "<body>" && text_mod.value == "<body>" && text_del.value == "<body>")
              alert("Per effettuare il commit devi modificare il documento!");
           else{
             commit();
           } 
      }else
         saveDocument();
 }
    
/*Funzione che, passato un vettore di Nodi, ne estrae quelli con tagName=Div e 
 *memorizza il Text (testo del blocco) in un array associativo, a cui ci si pu�
 *riferire tramite l'id del blocco.
*/
       function createAssociativeArray(nodes){
           
           var array=[];
           var j;
           for(j=0;j<nodes.length;j++){
               if(nodes[j].tagName=="DIV"){
                  array[nodes[j].id]=nodes[j].innerHTML; 
                  
            /*Rendo tutti i paragrafi Editabili*/                  
                  nodes[j].setAttribute("contenteditable","true");
                  
 /*Setto l'attributo Color della propriet� CSS outline a verde, per distinguere visivamente i paragrafi dell'autore corrente */         
                  if(nodes[j].getAttribute("data-id_autore")==id_aut){
                     nodes[j].style.outlineColor='#00ff00';
            
        }
               }
               if(nodes[j].nodeType==3){
                   nodes[j].parentNode.removeChild(nodes[j]);
               }
           } 
           return array;
       }
       
 
 function commit(){
     text_add.value+="</body>";
     text_mod.value+="</body>";
     text_del.value+="</body>";
     text_relation.value+="</body>";
     document.getElementById("compForm").submit(); 
 }
 
/*Pongo l'attributo contenteditable di ogni div pari a false. Salvo l'innerHtml dell'editor nell'input "text_add"
 *e lo invio al server. Il server provveder� a salvarlo in Working Folder*/ 
 function saveDocument(){
     var i;
     var innerHTML=document.getElementById("innerHTML");
 /*Pongo nuovamente i contentEditable pari a false*/    
     for(i=0;i<editorChlds.length;i++){
         editorChlds[i].setAttribute("contenteditable","false");
     }
     
     innerHTML.value="<body>"+objDoc.innerHTML+"</body>";     

     document.getElementById("WFForm").submit(); 
 }
     
    </script>
</head>
<body onload="initInterval()">
            

        <div id="container" >
	<%@include file="elem/Header.html" %>
	<jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Repository" />
         </jsp:include>
	<div id="content">
              <% if(id_aut != null) { %> <%-- fine scriptlet per inserire template statico --%>
        <form id="compForm" name="compForm" method="post" action="<%= response.encodeURL("/SviluppoCondiviso/Commit")%>"> <!-- Manca l'attributo "onsubmit" -->
            <!-- InputType nascosto. Necessario in quanto conterr� il codice XML del documento
              (precedentemente trasformato da HTML a XML dal parser), per passarlo nel corpo della richiesta HTTP-->
            <input id="deleted" type="hidden" name="deleted" />
            <input id="added" type="hidden" name="added"/>
            <input id="modified" type="hidden" name="modified"/>
            <input id="relationship" type="hidden" name="relationship"/>
            <input id="id_document" type="hidden" name="id_document" value="<%= id_document %>"/>
            <input id="version" type="hidden" name="version" value="<%= version %>"/>
            <div id="hiddenEditor" hidden></div>
            <div id="editor" contenteditable="false"><%=corpo%></div><br/>
            <div style="clear:both;">
              <input id="modifyButton" class="button" type="button" value="Modifica" onclick="initDoc();"/>
              <input id="saveButton" class="button" type="button" value="Salva" onclick="parseDocument();" style="display: none"/>
              <input id="commitButton" class="button" type="button" value="Commit" onclick="parseDocument();" style="display: none"/>
              <div id="ajax" style="float:right; margin-right: 1.9em; color:red; display:none" >La versione del documento che stai modificando/leggendo � obsoleta.<br/>Il documento si trova alla versione:<span id="num_version" style="color:red; font-size: 1.8em; margin-left:0.7em">1</span></div>                     <br/><br/>
            </div>
        </form>
            
            <form name="WFForm" id="WFForm" method="post" action="<%= response.encodeURL("/SviluppoCondiviso/WFManager")%>" hidden>
                <input id="innerHTML" type="hidden" name="innerHTML" />
                <input id="versione" type="hidden" name="versione" value="<%= version %>"/>
                <input id="iddoc" type="hidden" name="iddoc" value="<%= id_document %>"/>
                <input id="action"  name="action" type="hidden" value="save"/>
            </form>     
                
              <% } //Fine if %>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>