<%@page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Aggiungi Autori</title>
        <script>
           function invia(){
               document.getElementById("idwriters").setAttribute("value",document.getElementById("selected").value);
               document.getElementById("form").submit();
           } 
           
        </script>
        <%
          ResultSet rs = (ResultSet)request.getAttribute("rs");
        %>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Repository" />
         </jsp:include>
	  <div id="content">
		<div class="box"> 
		<div class="fcenter">
                        <h2>Aggiungi un nuovo autore al documento:</h2>    
                            <%  
                                if(!rs.next()){%>
                                
                                    <p>Non � possibile aggiungere altri autori al documento</p>
                                    
                                    <%
                                }else{%>
                                <select id="selected"><%
                                    do{ %>
                                    
                                        <option value="<%= rs.getString("IdAutore") %>"><%= rs.getString("Username") %></option>
                                        
                             <%     }while(rs.next()); %>
                                    </select>
                                    <input type="button" value="+ Aggiungi" onclick="invia()" />
                                 <%}%>
                        
                <form id="form" action="<%= response.encodeURL("/SviluppoCondiviso/AggiungiAutore")%>" method="post">
                    <input hidden id="iddoc" name="iddoc" value="<%= request.getAttribute("iddoc") %>" />
                    <input hidden id="idwriters" name="idwriters" />
                </form>
        </div>
	   </div>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>