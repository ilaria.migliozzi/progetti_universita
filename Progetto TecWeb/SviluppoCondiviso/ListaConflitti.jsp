<%@page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Lista Conflitti</title>
        <% 
              String id_aut = (String)session.getAttribute("id");
              ResultSet rs = (ResultSet)request.getAttribute("rs");
                    
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione � scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }
              if(rs == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","index.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }             
                %>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Conflitti" />
         </jsp:include>
	  <div id="content">
              <%
                if(!rs.next()){%>
                    <p>Non hai blocchi in conflitto</p>
                <%}else{
                %>
		
            <table cellspacing='0'> <!-- cellspacing='0' � importante, deve rimanere-->
                    <tr><th>Titolo</th><th>Testo del Blocco</th><th>Operazioni</th></tr> <!-- Table Header -->
                    <%
                    int i = 1;       
                    do{
                        if(i%2 == 0){%>
                            <tr>
                    <% }else{ %>
                            <tr class='even'>
                    <% } %>        
                        <td><%= rs.getString("Titolo") %></td><td><%= rs.getString("TestoOriginario") %></td><td><a href="<%= response.encodeURL("/SviluppoCondiviso/ResolveConflict?iddef="+rs.getString("IdDefinitivo")+"&testo="+rs.getString("TestoOriginario")+"&titolo="+rs.getString("Titolo")+"&iddocumento="+rs.getString("IdDocumento"))%>">Risolvi Conflitti</a></td></tr>
                     <%
                        i++;
                    }while(rs.next()); %>                   
</table>     
<% } %>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>