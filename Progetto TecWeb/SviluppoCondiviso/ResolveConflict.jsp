<%@page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Documenti</title>
        <% 
              String id_aut = (String)session.getAttribute("id");
              String id_def = (String)request.getAttribute("id_definitivo");
              String testo_def = (String)request.getAttribute("def_testo");
              String id_doc = (String) request.getAttribute("id_documento");
              ResultSet rs = (ResultSet)request.getAttribute("rs");
              String testo;
              
              boolean canc;
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione � scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }
          /*    if(rs_def == null || rs == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","index.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }*/

            if(!rs.next()){
                  request.setAttribute("msg","Problemi di comunicazione con il server");
                  request.setAttribute("time","3");
                  request.setAttribute("redirect","index.jsp");
                  RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                  dispatcher.forward(request,response);
                  return;                 
             }else{   
                                          
                %>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Notifiche" />
         </jsp:include>
         <div id="content">
                
		<p>Sono stati generati dei conflitti sul seguente blocco:</p>
                <p id="informations"><%=testo_def%></p>
            <table cellspacing='0'> <!-- cellspacing='0' � importante, deve rimanere-->
                    <tr><th>Testo Proposto</th><th>Autore del testo</th><th>Operazioni</th></tr> <!-- Table Header -->
                <% 
                   int i = 1; 
                   do{
                     if(rs.getString("Testo")==null){
                         canc=true;
                         testo="IL TUO BLOCCO E' STATO CANCELLATO";
                     }else{
                         canc=false;
                         testo=rs.getString("Testo");                                                 
                     }
                     if(i%2 == 0){%>
                            <tr>
                 <% }else{ %>
                            <tr class='even'>
                    <% } %>        
                        <td><%= testo %></td><td><%= rs.getString("Username") %></td><td> <a href="<%= response.encodeURL("/SviluppoCondiviso/ConflittoDB?iddoc="+id_doc+"&iddef="+id_def+"&idtemp="+rs.getString("IdTemporaneo")+"&canc="+canc) %>" >Scegli</a></td></tr>
                     <%
                        i++;
                    }while(rs.next()); %>                   
             </table> 
             <form method="Get" action="<%= response.encodeURL("/SviluppoCondiviso/ConflittoDB")%>" class="fcenter">
                 <input hidden name="iddoc" value="<%=id_doc%>"/>
                 <input hidden name="iddef" value="<%=id_def%>"/>
                 <input type="button" value="Rifiuta i conflitti" class="button" onclick="submit()" />  
<% } %>      </form> 
          <br/>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>