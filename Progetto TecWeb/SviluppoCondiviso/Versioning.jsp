<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
<% 
              String id_aut = (String)session.getAttribute("id");
              String user = (String)session.getAttribute("username");
              String corpo = (String)request.getAttribute("corpo");
              String id_document = (String)request.getAttribute("id_document");  
              String titolodoc = (String) request.getAttribute("titolodoc");
              String deleted = (String) request.getAttribute("deleted");
              int selected_version = Integer.parseInt((String)request.getAttribute("versione_richiesta"));
              int last_version = Integer.parseInt((String)request.getAttribute("maxversion"));
                                                                                           
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione è scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
                }
              if(corpo == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Documenti.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
                }             
                %>          
      <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
        <title>Versioning</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" /> 
        <style>
             #editor div:hover, #editor_cancellati div:hover{
 	       outline: #82C0FF solid 2px; /*Setto il bordo dei div su cui passa il mouse ad azzurro*/
 	     } 
             
             
        </style>
        <script>
           var l_version=<%= last_version %>;
           var s_version=<%= selected_version %>;

           
           function init(){
               var firstn= document.getElementById("editor").firstChild;

               set_informations(firstn);
     
           }

           function set_informations(node){
              var textnode;
              var text_block;
              var author_b;
              var ver_block; 
              var testo, autore, modifica, versione, ha_sovrascritto, chi;
              
               testo = node.innerHTML;
               autore=node.getAttribute("data-autore");
               modifica = node.getAttribute("data-tipo_mod");
               versione = node.getAttribute("data-versione");
               ha_sovrascritto=node.getAttribute("data-ha_sovrascritto");
               chi = node.getAttribute("data-chi");
            
               text_block = document.getElementById("text_block");
               author_b = document.getElementById("owner_block"); 
               ver_block = document.getElementById("version_block");
                              
               
               textnode = document.createElement('DIV');
               textnode.innerHTML=testo;
               text_block.replaceChild(textnode,text_block.firstChild);              

               textnode = document.createTextNode(autore);
               author_b.replaceChild(textnode,author_b.firstChild);

               showmod(modifica,ha_sovrascritto,chi);
               
               if(versione==null){ // Controllo per i blocchi cancellati che non hanno versione!
                   versione="<%=selected_version%>";  //vengono sempre "creati" dalla versione precedente del documento
                   versione--;
               }
               textnode = document.createTextNode(versione);
               ver_block.replaceChild(textnode,ver_block.firstChild);
           }

           function showmod(modifica,ha_sovrascritto,chi){
               var temp,textnode;
               var modif_block,over_block,aut_block;
               
               modif_block = document.getElementById("modif_block");               
               over_block=document.getElementById("over_block");
               aut_block=document.getElementById("aut_block");
               
               if(modifica=="CONFLITTO_RISOLTO"){
                  if(ha_sovrascritto=="none"){
                      temp="Cancellazione";
                      var autore_block=document.getElementById("aut_block_int");
                      textnode = document.createTextNode(chi);
                      autore_block.replaceChild(textnode,autore_block.firstChild);
                      aut_block.style.display="inline";                       
                  }else{ 
                      temp="Risoluzione di un conflitto";
                      textnode = document.createTextNode(ha_sovrascritto);
                      var overridden_block=document.getElementById("over_block_int");
                      overridden_block.replaceChild(textnode,overridden_block.firstChild);
                      over_block.style.display="inline";
                  }
               }
               
               if(modifica=="CANCELLATO"){
                  temp="Cancellazione";
                  var autore_block=document.getElementById("aut_block_int");
                  textnode = document.createTextNode(chi);
                  autore_block.replaceChild(textnode,autore_block.firstChild);
                  aut_block.style.display="inline";                   
               }
               
               if(modifica=="RISCRITTO"){
                   temp="Modifica da parte del proprietario";
               }
               if(modifica=="AGGIUNTO"){
                   temp="Aggiunta di testo";
               }               
               textnode = document.createTextNode(temp);
               modif_block.replaceChild(textnode,modif_block.firstChild);
           }
           
           
           function selectElement(){
               if(over_block.style.display)
                  over_block.style.display="none";
               
               if(aut_block.style.display)
                  aut_block.style.display="none";
            
               set_informations(event.target);              
            }
            
          function show_deleted(){
              if(event.target.checked==true){
                  document.getElementById("editor_cancellati").style.display="inherit";
              }else{
                  document.getElementById("editor_cancellati").style.display="none";
              }
          }  
            
        </script>
    </head>
<body onload="init();">
            

        <div id="container" >
	<%@include file="elem/Header.html" %>
	<jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Repository" />
         </jsp:include>
	<div id="content">
              <% if(id_aut != null) { %> <%-- fine scriptlet per inserire template statico --%>
          <p id="title_doc"><%=titolodoc%>, versione: <%= selected_version%></p>    
          <form id="open" action="<%= response.encodeURL("/SviluppoCondiviso/RipristinaVersione")%>">
              <input hidden name="iddoc" value="<%=id_document%>"/>
              <input hidden name="versione" value="<%=selected_version%>"/>
              <p style="float:left">Selezionando con un click il paragrafo, si ottengono ulteriori informazioni.</p> 
              <input id="modifyButton" type="button" class="button" name="modifyButton" value="Ripristina Questa Versione" onclick="submit()" style="margin-left:33.9em"/> 
              
          </form>           
          <br/>
          <div id="editor" contenteditable="false"><%=corpo%></div>
          <br/><br/>
          <div>Mostra testo eliminato dalla versione precedente: <input id="check" type="checkbox" onclick="show_deleted()"/></div>
          <div id="editor_cancellati" contenteditable="false" style="display:none"><%=deleted%></div>
          <br/><br/>
          <div id="informations" style="position:relative">
              <div id="text_block" style="text-align: center;">primo_figlio</div>
              <br/>
              <div class="external" style="width:550px; float:left; margin-left: 30px;">Autore del blocco: <div id="owner_block">primo_figlio</div></div>      
              <div class="external">Ottenuto tramite: <div id="modif_block">primo_figlio</div></div>
              <br/>
              <br/>
              <div style="text-align: center; clear:both;"> 
              <div id="aut_block"  class="external" style="display: none;" >Cancellato dall' autore:<div id="aut_block_int">primo_figlio</div></div>
              <div id="over_block" class="external" style="display: none;">Il blocco precedente aveva il seguente testo:<div id="over_block_int">primo_figlio</div></div>     
             </div>
              <br/>
             <div class="external" style="clear: both; text-align: center;">Il blocco è stato inserito/cambiato/cancellato nella versione <div id="version_block">primo_figlio</div></div>          
             
              
          </div>  
          <br/>    
              <% } /*Fine if*/ else{  %>
              <p>Per visitare questa pagina devi effettuare il log-in. Se non hai un account, <a href="<%= response.encodeURL("Registrazione")%>">clicca qui</a> per registrarti</p> 
              <%}%>
	</div>
        <br/><br/>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>