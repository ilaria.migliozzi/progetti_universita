<!DOCTYPE html>
<html>
    <head>
    	<title> Accesso Negato </title>
        <meta http-equiv="refresh" content="2;<%= response.encodeURL("index.jsp") %>">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
    </head>
    <body>
        <div id="container" >
            <%@include file="elem/Header.html" %>
            <div id="content">
                <div class="fcenter">
                <p><strong>Spiacente:</strong></p>
                <p><span class="important"> Non � possibile accedere a questa risorsa direttamente </span></p>
                <p><strong>Attendi, verrai ridirezionato automaticamente...</strong></p>	
                </div>
            </div>
            <%@include file="elem/Footer.html" %>
        </div>
    </body>
 </html>