<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - NuovoDocumento</title>
        <script src="js/MD5.js"></script>
        <script>
            var hex_username;
            var hex_password;
            function md5(){
               document.getElementById("nameform").value = document.getElementById("name").value;
               document.getElementById("hexpassword").value = hex_md5(document.getElementById("password").value); 
               document.getElementById("form").submit();
            }
        </script>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="Repository" />
         </jsp:include>
	  <div id="content">
		<div class="box"> 
		<div class="fcenter">
                 <form id="form" action="<%= response.encodeURL("/SviluppoCondiviso/NuovoDocumento")%>" method="post">
                        <h2>Inserisci il titolo del documento:</h2>    
                        <input  name="titolo" type="text" value="Titolo" onclick="this.value=''" class="fcenter"/>
                        <input type="submit" value="Crea Documento" />
                </form>

        </div>
	   </div>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>