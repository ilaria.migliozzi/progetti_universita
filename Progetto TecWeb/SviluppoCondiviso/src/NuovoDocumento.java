import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;


public class NuovoDocumento extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        Connection conn = null;
        String max;
        ResultSet rs;
        String titolo = (String)req.getParameter("titolo");
        String idAutore = (String)req.getSession().getAttribute("id");
        
        if(idAutore == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        if(titolo == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa risorsa direttamente.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        
        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return; 
        }
        
        
        
        try{
                PreparedStatement prst = conn.prepareStatement("SELECT MAX(IdDocumento) As Max FROM Documento");
                rs=prst.executeQuery();
                rs.next();
                max = ((Integer)(Integer.parseInt(rs.getString("Max"))+1)).toString();
                        
                
                
                prst = conn.prepareStatement("INSERT INTO `documento`(IdDocumento,`Versione`, `Titolo`, `DataCreazione`, `Creato_da`) VALUES (?,0,?,NOW(),?)");
                prst.setString(1,max);
                prst.setString(2,titolo);
                prst.setString(3,idAutore);
                prst.executeUpdate();
                
                req.setAttribute("msg","Nuovo documento è stato creato!");
                req.setAttribute("time","2");
                req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
        

        }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        /* Rilascio la connessione */
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        } catch (ConnectionPoolException ex) {
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        } 
     }
    
}
