import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class Login extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            Connection conn = null;
            String username;
            String password;
            
            
            /*Ottengo una connessione libera dal pool di connesione*/
            try {
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;
            }
            
            
            /* Estraggo lo username e la password passati dalla form di login e interrogo la tabella utenti
                per controllare se l'untente è registrato */
            username = req.getParameter("Username");
            password = req.getParameter("Password");
            
            try{
                PreparedStatement prst = conn.prepareStatement("SELECT IdAutore FROM AUTORE WHERE Username = ? AND Password = ?");
                prst.setString(1 , username);
                prst.setString(2 , password);
                ResultSet rs = prst.executeQuery();
                
                /* Se l'utente è registrato creo una nuova sessione in cui conservo il suo id e il username*/
                if(rs.next()){
                    
                    /* Creazione di una nuova sessione, con set di username e idutente */
                    HttpSession session = req.getSession(true);
                    session.setAttribute("id", rs.getString("IdAutore"));
                    session.setAttribute("username", username);
                    session.setMaxInactiveInterval(60*60);
                    res.sendRedirect(res.encodeRedirectURL("index.jsp"));
                }
                else{/* Altrimenti mostrao una pagina di errore e reindirizzo alla pagina di login*/
                   req.setAttribute("msg","Loggin Fallito!");
                   req.setAttribute("time","2");
                   req.setAttribute("redirect","Login.html");
                   RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                   dispatcher.forward(req,res);
                }
                   
                
            }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;
            }          
                        
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;
            }
        
    }
}
