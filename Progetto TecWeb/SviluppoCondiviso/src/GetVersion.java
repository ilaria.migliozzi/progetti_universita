import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class GetVersion extends HttpServlet{

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            Connection conn = null;
            String id_document = req.getParameter("iddoc");
            String maxversion = req.getParameter("maxversion");
            String versione_richiesta = req.getParameter("versione_richiesta");
            String titolodoc = req.getParameter("titolodoc");
            
            
            String inner = ""; //blocchi appartenenti al documento richiesto 
            String deleted = ""; //blocchi cancellati dalla versione precedente del documento.
            
        /*Controllo che la sessione sia ancora attiva ed allora recupero lo userid dell'utente,
        *in caso contrario l'utente viene reindirizzato alla pagina di login */  
        HttpSession session = req.getSession();
        String idUtente = (String)session.getAttribute("id");
        
        if(idUtente == null){
                   
              /*Nel caso in cui la sessione è¨ scaduta mostro un messaggio di errore e reindirizzo alla pagina
                *di login.*/
               req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
               req.setAttribute("time","4");
               req.setAttribute("redirect","Login.html");
               RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
               dispatcher.forward(req,res);
               return;
         }
        
        /* Controllo in caso in cui la servlet venga richiamata direttamente*/ 
           if(id_document == null){
                   /*Nel caso in cui la sessione Ã¨ scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa pagina direttamente.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
           }
             
            /*Ottengo una connessione libera dal pool di connesione*/
            try {
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }
            
           
            try{
                PreparedStatement prst = conn.prepareStatement("SELECT M.IdDocumento,M.Versione,M.IdDefinitivo,AA.Username AS ProprietarioBlocco,D.Testo,M.TipoModifica,M.VersioneGenerata,DD.Testo AS TestoSovrascritto,DDD.Testo AS TestoCancellato,A.Username AS AutoreModifica FROM (((((modifiche M left join bloccodoc B ON M.IdDocumento=B.IdDocumento AND M.Versione=B.Versione AND M.IdDefinitivo=B.IdDefinitivo) left join definitivo D ON M.IdDefinitivo=D.IdDefinitivo) left join definitivo DD ON M.HaSovrascritto=DD.IdDefinitivo) left join definitivo DDD ON D.IdDefinitivo=DDD.IdDefinitivo AND M.TipoModifica='CANCELLATO') left join autore A ON M.AutoreCancellatore=A.IdAutore) left join autore AA ON D.Scritto_da=AA.IdAutore WHERE M.IdDocumento=? AND M.Versione=?");
                prst.setString(1 , id_document);
                prst.setString(2 , versione_richiesta);
                ResultSet rs = prst.executeQuery();
                
               
                if(rs.next()){
                    do{
                        
                        if( ( rs.getString("TipoModifica").equals("CANCELLATO") ) || ( rs.getString("TipoModifica").equals("CONFLITTO_RISOLTO") && rs.getString("TestoSovrascritto") == null))
                        {
                          
                            deleted = deleted +"<div "+"id="+rs.getString("IdDefinitivo")+" data-autore=\""+ rs.getString("ProprietarioBlocco")+"\" data-tipo_mod=\""+rs.getString("TipoModifica")+"\" data-chi=\""+rs.getString("AutoreModifica")+"\" data-ha_sovrascritto=\"none\" onclick=\"selectElement();\">"+rs.getString("Testo")+"</div>";               
                        
                        }else{
                          
                            inner = inner + "<div "+"id=\""+rs.getString("IdDefinitivo")+"\" data-autore=\""+ rs.getString("ProprietarioBlocco")+"\" data-tipo_mod=\""+rs.getString("TipoModifica")+"\" data-versione=\""+rs.getString("VersioneGenerata")+"\" data-ha_sovrascritto=\""+rs.getString("TestoSovrascritto")+"\" data-chi=\"none\" onclick=\"selectElement();\">"+rs.getString("Testo")+"</div>";
                        }
                            
                      }while(rs.next());
                }
                else{/* Altrimenti mostra una pagina di errore e reindirizzo alla pagina home*/
                   req.setAttribute("msg","Problemi di connessioni con il server!");
                   req.setAttribute("time","2");
                   req.setAttribute("redirect","index.jsp");
                   RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                   dispatcher.forward(req,res);
                }
                
                
                System.out.println("CORPO:"+inner);
                System.out.println("DELETED:"+deleted);
                
                
                /*RISETTARE GLI STESSI ATTRIBUTI*/               
                req.setAttribute("id_document",id_document);
                req.setAttribute("maxversion",maxversion);
                req.setAttribute("versione_richiesta",versione_richiesta);
                req.setAttribute("titolodoc",titolodoc);
                req.setAttribute("deleted", deleted); 
                req.setAttribute("corpo", inner); 
                RequestDispatcher dispatcher = req.getRequestDispatcher("Versioning.jsp");
                dispatcher.forward(req,res); 
                
            }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }          
                        
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }
    }
}