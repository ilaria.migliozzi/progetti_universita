import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class Documenti extends HttpServlet{

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            String idUtente = null;
            /*Controllo che la sessione sia ancora attiva ed allora recupero lo userid dell'utente,
             *in caso contrario l'utente viene reindirizzato alla pagina di login */  
            HttpSession session = req.getSession();
            idUtente = (String)session.getAttribute("id");
            
            if(idUtente == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
            } 
            /* Ottengo una connessione libera dal pool di connessioni*/
            Connection conn = null;
            try{
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;  
                
            }
            
            
            /*Eseguo la query per recuperare tutti i documenti che 
             * l'autore loggato può modificare(i documenti da lui creati e quelli di cui è "writers") */
            try{
                PreparedStatement prst = conn.prepareStatement(  "SELECT MAX(D.Versione) as Versione, D.IdDocumento, D.Titolo, D.DataCreazione, A.Username AS UsernameCreatore, D.Creato_da AS IdCreatore "
                                                                + "FROM ewriter E JOIN documento D JOIN autore A "
                                                                + "WHERE E.IdDocumento = D.IdDocumento AND D.Creato_da = A.IdAutore AND E.IdAutore = ? "
                                                                + "GROUP BY D.IdDocumento");
                
                prst.setString(1,idUtente);
                
                ResultSet rs = prst.executeQuery();
                
                
                /* Eseguita la query passa il controllo Documenti.jsp che stampa *
                 * il risultatao della query in una tabella*/
                req.setAttribute("rs",rs);
                RequestDispatcher dispatcher = req.getRequestDispatcher("Documenti.jsp");
                dispatcher.forward(req,res);
    
            }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;     
            }
            
                   
            /* Rilascio la connessione ottenuta dal pool*/            
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return;  
            }
    }
}