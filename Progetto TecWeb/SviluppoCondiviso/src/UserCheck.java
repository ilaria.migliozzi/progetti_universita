import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class UserCheck extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            Connection conn = null;
            String username;
            PrintWriter out;
            
            /*Ottengo una connessione libera dal pool di connesione*/
            try {
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(UserCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
          /* Estraggo lo username passato dalla form di login e interrogo la tabella utenti
             per controllare se l'untente è già registrato, rispondendo alla richiesta Ajax */
            username = req.getParameter("Username");
            
            try{
                PreparedStatement prst = conn.prepareStatement("SELECT `IdAutore` FROM `autore` WHERE username=?");
                prst.setString(1 , username);
                ResultSet rs = prst.executeQuery();

                res.setContentType("text/xml");
                out=res.getWriter();

                /* Se l'utente è registrato invio una stringa si avviso*/
                if(rs.next()){ 
                   out.print("Questo username � gi� in uso!");
                }
                else{/* altrimenti invio una stringa ad indicare che lo username è disponibile*/
                   out.print("ok");
                }
             /*Chiudo lo stream d'uscita*/
                out.close();
                
            }catch(SQLException ex){
                Logger.getLogger(UserCheck.class.getName()).log(Level.SEVERE, null, ex);
            }          
                        
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(UserCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
}