import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;


public class AggiungiAutore extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        Connection conn = null;
        ResultSet rs;
        String iddoc= (String)req.getParameter("iddoc");
        String idwrites= (String)req.getParameter("idwriters");
        String idAutore = (String)req.getSession().getAttribute("id");
        
        if(idAutore == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        if((iddoc == null)||(idwrites == null)){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa risorsa direttamente.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        
        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return;
        }
        
        
        try{
                PreparedStatement prst = conn.prepareStatement("INSERT INTO `ewriter`(`IdDocumento`, `IdAutore`) VALUES (?,?)");
                prst.setString(1,iddoc);
                prst.setString(2,idwrites);
                prst.executeUpdate();
                
                req.setAttribute("msg","Nuovo writers aggiunto!");
                req.setAttribute("time","2");
                req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
        

        }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        /* Rilascio la connessione */
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        } catch (ConnectionPoolException ex) {
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return;
        } 
     }
    
}

