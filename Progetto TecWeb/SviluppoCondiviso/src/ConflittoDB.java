import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class ConflittoDB extends HttpServlet{
    public synchronized void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        
        synchronized(CondVar.getCondVar()){
        System.out.println("1");
        
        String iddef= req.getParameter("iddef");
        String iddoc= req.getParameter("iddoc");
        String idtemp = req.getParameter("idtemp");
        String canc = req.getParameter("canc");
        PreparedStatement prst = null;
        ResultSet rs= null;
        Connection conn = null;
        
        System.out.println("2");
        /*if(controllo tutti null)*/
        
        
        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return;
        }
        
        System.out.println("3");
        
        try{
            /* recupero l'ultima versione del documento */
            prst = conn.prepareStatement("SELECT MAX(Versione) AS Versione FROM DOCUMENTO WHERE IdDocumento = ? ");
            prst.setString(1,iddoc);
            System.out.println("a");
            rs=prst.executeQuery();
            System.out.println("b");
            rs.next();
            System.out.println("c");
            String version = rs.getString("Versione");
            System.out.println("d");
            System.out.println(rs);
            System.out.println(iddoc);
            String nextVersion = ((Integer)(Integer.parseInt(rs.getString("Versione"))+1)).toString();
            
            System.out.println("4");
            
        if(idtemp != null){
            
            if(canc.equals("true")){
                System.out.println("######### CONFLITTO DB RISOLUZIONE CANCELLAZIONE######");
                this.AvanzaVersione(conn,iddoc,version);
                
                PreparedStatement prst_2;
                PreparedStatement prst_3;
                PreparedStatement prst_4;
                
                System.out.println("5");
                
                prst = conn.prepareStatement("SELECT * FROM temporaneo WHERE IdTemporaneo = ?");
                prst.setString(1,idtemp);
                rs = prst.executeQuery();
                rs.next();
                String ScrittoDa = rs.getString("Scritto_da");
                        
                prst_2 = conn.prepareStatement("DELETE FROM `bloccodoc` WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst_3 = conn.prepareStatement("UPDATE `modifiche` SET `TipoModifica`=\"Conflitto_Risolto\",`VersioneGenerata`=?,`AutoreCancellatore`=? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst_4 = conn.prepareStatement("CALL eliminaBlocco(?,?,?)");
                
                System.out.println("6");
                
                prst_4.setString(1,iddoc);
                prst_4.setString(2,nextVersion);
                prst_4.setString(3,iddef);
                                
                                
                prst_2.setString(1,iddef);
                prst_2.setString(2,iddoc);
                prst_2.setString(3,nextVersion);
                                
                System.out.println("7");
                
                prst_3.setString(1,version);
                prst_3.setString(2,ScrittoDa);
                prst_3.setString(3,iddef);
                prst_3.setString(4,iddoc);
                prst_3.setString(5,nextVersion);
                
                System.out.println("8");
                                
                prst_4.executeQuery();
                prst_2.executeUpdate();
                prst_3.executeUpdate();
                                
                System.out.println("CALL eliminaBlocco("+iddoc+","+nextVersion+","+iddef+")");
                System.out.println("DELETE FROM bloccodoc WHERE IdDefinitivo = "+iddef+" AND IdDocumento = "+iddoc+" AND Versione = "+nextVersion);
                System.out.println("UPDATE modifiche SET `TipoModifica`=CANCELLATO,`VersioneGenerata`="+version+",`AutoreCancellatore`="+ScrittoDa+" WHERE IdDefinitivo = "+iddef+" AND IdDocumento = "+iddoc+" AND Versione = "+nextVersion); 
                  
            }else if(canc.equals("false")){ /* RISOLUZIONE MODIFICA */
                
                System.out.println("######### CONFLITTO DB RISOLUZIONE MODIFICA ######");
                
                this.AvanzaVersione(conn,iddoc,version);
                
                /* Recupero il precedente e il successivo del blocco che verrà sostituito */
                prst = conn.prepareStatement("SELECT Def_Pre, Def_Post FROM bloccodoc WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst.setString(1,iddef);
                prst.setString(2,iddoc);
                prst.setString(3,version);
                rs = prst.executeQuery();
                rs.next();
                
                String def_pre = rs.getString("Def_Pre");
                String def_post = rs.getString("Def_Post");
                
                System.out.println("9");
                
                prst = conn.prepareStatement("SELECT * FROM temporaneo WHERE IdTemporaneo = ?");
                prst.setString(1,idtemp);
                rs = prst.executeQuery();
                rs.next();
                String ScrittoDa = rs.getString("Scritto_da");

                prst = conn.prepareStatement("SELECT inserimentoInBloccoDoc(?,?,?,?,?,?) AS LastKey");
                prst.setString(1,rs.getString("Testo"));
                prst.setString(2,ScrittoDa);
                prst.setString(3,iddoc);
                prst.setString(4,nextVersion);
                prst.setString(5, def_pre);
                prst.setString(6, def_post);
                System.out.println("CALL inserimentoInBloccoDoc("+rs.getString("Testo")+","+rs.getString("Scritto_da")+","+iddoc+","+nextVersion+","+def_pre+","+def_post);
                rs = prst.executeQuery();
                rs.next();
                String LastKey = rs.getString("LastKey");
                

                prst = conn.prepareStatement("UPDATE modifiche SET TipoModifica = 'CONFLITTO_RISOLTO', VersioneGenerata = ? , HaSovrascritto = ? , AutoreCancellatore = ? WHERE IdDocumento = ? AND Versione = ? AND IdDefinitivo = ?");
                prst.setString(1,version);
                prst.setString(2,iddef);
                prst.setString(3,ScrittoDa);
                prst.setString(4,iddoc);
                prst.setString(5,nextVersion);
                prst.setString(6,LastKey);
                prst.executeUpdate(); 
                
                System.out.println("UPDATE modfiche SET TipoModifica = CONFLITTO_RISOLTO, VersioneGenerata ="+version+","+"HaSovrascritto ="+iddef+","+"AutoreCancellatore ="+ScrittoDa+"WHERE IdDocumento = "+iddoc+"AND Versione = "+version+"AND IdDefinitivo ="+LastKey);
                
                prst = conn.prepareStatement("UPDATE bloccodoc SET Def_Post = ? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst.setString(1,LastKey);
                prst.setString(2,def_pre);
                prst.setString(3,iddoc);
                prst.setString(4,nextVersion);
                prst.executeUpdate(); 
                
               System.out.println("UPDATE bloccodoc SET Def_Post = ? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                
                prst = conn.prepareStatement("UPDATE bloccodoc SET Def_Pre = ? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst.setString(1,LastKey);
                prst.setString(2,def_post);
                prst.setString(3,iddoc);
                prst.setString(4,nextVersion);
                prst.executeUpdate();  
                
                 System.out.println("UPDATE bloccodoc SET Def_Pre = ? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                
                
                prst = conn.prepareStatement("DELETE FROM bloccodoc WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst.setString(1,iddef);
                prst.setString(2,iddoc);
                prst.setString(3,nextVersion);
                prst.executeUpdate(); 
                
                System.out.println("DELETE FROM bloccodoc WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                
                prst = conn.prepareStatement("DELETE FROM modifiche WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                prst.setString(1,iddef);
                prst.setString(2,iddoc);
                prst.setString(3,nextVersion);
                prst.executeUpdate(); 
                
                System.out.println("DELETE FROM modifiche WHERE IdDefinitivo = "+iddef+"AND IdDocumento="+iddoc+"AND Versione="+nextVersion);
            }
        }
        
        
        prst = conn.prepareStatement("DELETE FROM Temporaneo WHERE IdDefinitivo = ?");
        prst.setString(1,iddef);
        prst.executeUpdate();
        
        System.out.println("DELETE FROM Temporaneo WHERE IdDefinitivo =" + iddef);
        
        req.setAttribute("msg","Conflitto Risolto!");
        req.setAttribute("time","2");
        req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
        RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
        dispatcher.forward(req,res);
        
        /*Rilascio la connessione nel pool*/
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        }catch (ConnectionPoolException ex){
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }
        
        }catch(SQLException ex){
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return;
        }
       } 
    }
    
    /* Creo una nuova versione del documento */
    private void AvanzaVersione(java.sql.Connection conn, String id_document, String version){
                    try{
                        PreparedStatement prst = conn.prepareStatement("CALL inserimentoDoc(?,?)");
                        prst.setString(1,id_document);
                        prst.setString(2,version);
                        prst.executeQuery();
                        System.out.println("CALL inserimentoDoc("+id_document+","+version+")");
                
                
                        /* Duplica tutti i blocchi doc e tutte le modfiche */
                        prst = conn.prepareStatement("CALL duplicaDocModifica(?,?)");
                        prst.setString(1,id_document);
                        prst.setString(2,version);
                        System.out.println("CALL duplicaDocModifica("+id_document+","+version+")");
                        prst.executeQuery();   
                    
                    }catch(SQLException ex){
                     Logger.getLogger(ConflittoDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
    }
}
