import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;


public class AutoriMancanti extends HttpServlet{

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        Connection conn = null;
        ResultSet rs;
        String iddoc = (String)req.getParameter("iddoc");
        String idAutore = (String)req.getSession().getAttribute("id");
        
        if(idAutore == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        if(iddoc == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa risorsa direttamente.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        
        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return; 
        }
        
        
        
        try{
                
               /* Controlla che l'autorie sia effettivamente proprietario del documento */
              PreparedStatement prst = conn.prepareStatement("SELECT * FROM documento WHERE Creato_da = ? AND IdDocumento = ?");
              prst.setString(1,idAutore);
              prst.setString(2,iddoc);
              rs = prst.executeQuery();
              if(!rs.next()){
                    req.setAttribute("msg","Non sei proprietario di questo documento");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                     /* Rilascio la connessione del pool*/
                    try{
                        ConnectionPool.getConnectionPool().releaseConnection(conn);
                    } catch (ConnectionPoolException ex) {
                     req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                     req.setAttribute("time","4");
                     req.setAttribute("redirect","index.jsp");
                     dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                     dispatcher.forward(req,res);
                     return; 
                    }
                    return; 
              }
            
            
                prst = conn.prepareStatement("SELECT B.IdAutore , B.Username FROM autore B WHERE (B.IdAutore,B.Username) NOT IN (SELECT A.IdAutore , A.Username FROM ewriter E JOIN autore A WHERE E.IdAutore = A.IdAutore AND E.IdDocumento = ? )");
                prst.setString(1,iddoc);
                rs=prst.executeQuery();
                
                /* Eseguita la query passa il controllo Documenti.jsp che stampa *
                 * il risultatao della query in una tabella*/
                req.setAttribute("rs",rs);
                req.setAttribute("iddoc",iddoc);
                RequestDispatcher dispatcher = req.getRequestDispatcher("AggiungiAutori.jsp");
                dispatcher.forward(req,res); 

        }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        /* Rilascio la connessione */
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        } catch (ConnectionPoolException ex) {
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }   
    }
    
}
