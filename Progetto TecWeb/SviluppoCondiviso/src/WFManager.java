import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class WFManager extends HttpServlet{
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        String id_utente = null;
        String action = null;
        String id_documento = null;
        String versione = null;
        String innerHTML = null;
        Connection conn = null;
        
        /* Verifico che l'utente sia ancora loggato */
        HttpSession session = req.getSession();
        id_utente = (String)session.getAttribute("id");
        if(id_utente == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }
        
        /* Estraggo i parametri dalla richiesta */
        action= req.getParameter("action");
        id_documento = req.getParameter("iddoc");
        versione = req.getParameter("versione");
        innerHTML = req.getParameter("innerHTML");
        
        System.out.println("id_documento versione innerHTML action");
        System.out.println(id_documento+versione+innerHTML+action);
        
        if(action == null){
                    /*Nel caso in cui la servlet venga richiamata dall'esterno senza settare i parametri */
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa risorsa direttamente");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
        }
        
        
         /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        if(action.equals("save")){
            
            this.saveWf(id_utente,id_documento,versione,innerHTML,req,res);
        
        }else if(action.equals("delete")){
        
            /* caso cancellazione da WF */
        
        }
        
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        } catch (ConnectionPoolException ex) {
            Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    
    private void saveWf(String id_utente, String id_documento, String versione, String innerHTML,HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        
        if( (id_documento == null) || (versione == null) || (innerHTML == null) ){
            /*Nel caso in cui la servlet venga richiamata dall'esterno senza settare i parametri */
            req.setAttribute("msg","Spiacente, non puoi richiamare questa risorsa direttamente");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return;
        }
        
        System.out.println("id_documento versione innerHTML");
        System.out.println(id_documento+versione+innerHTML);
        
        /* Ottengo una connessione libera dal pool di connessioni*/
            Connection conn = null;
            try{
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try{
                PreparedStatement prst = conn.prepareStatement("SELECT * FROM ewriter  WHERE idDocumento = ? AND idAutore =?");
                prst.setString(1,id_documento);
                prst.setString(2,id_utente);
                ResultSet rs=prst.executeQuery();
            
                 if(!rs.next()){
                    req.setAttribute("msg","Spiacente, non hai diritto a memorizzare questo documento");
                    req.setAttribute("time","3");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                try{
                    ConnectionPool.getConnectionPool().releaseConnection(conn);
                } catch (ConnectionPoolException ex) {
                    Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
                 }
                return;
                }   
           
                prst = conn.prepareStatement("INSERT INTO `workingfolder`(`Proprietario`, `IdDocumentoRif`, `VersioneRif`, `innerHTML`) VALUES (?,?,?,?)");
                prst.setString(1,id_utente);
                prst.setString(2,id_documento);
                prst.setString(3,versione);
                prst.setString(4,innerHTML);
                
                prst.executeUpdate();
                
                req.setAttribute("msg","Documento memorizzato in Working Folder");
                req.setAttribute("time","3");
                req.setAttribute("redirect","/SviluppoCondiviso/WorkingFolder");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                
                
                
            
            }
            catch(com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException ex){
                
                
                try{/*Se il documento è già presente in WF viene sovrascritto */
                    PreparedStatement prst = conn.prepareStatement("UPDATE `workingfolder`SET `innerHTML` = ? WHERE `Proprietario` = ? AND `IdDocumentoRif` = ? AND `VersioneRif` = ?");
                
                    prst.setString(1,innerHTML); 
                    prst.setString(2,id_utente);
                    prst.setString(3,id_documento);
                    prst.setString(4,versione);
                    prst.executeUpdate();
                    
                    req.setAttribute("msg","Il Documento è stato sovrascritto in Working Folder");
                    req.setAttribute("time","3");
                    req.setAttribute("redirect","/SviluppoCondiviso/WorkingFolder");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    
                }catch(SQLException e){
                  Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, e);
                }
            
            
            
            }
            catch(SQLException ex){
                Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        
            
            /* Rilascio la connessione ottenuta dal pool*/            
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(WFManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        
  
    }
}
