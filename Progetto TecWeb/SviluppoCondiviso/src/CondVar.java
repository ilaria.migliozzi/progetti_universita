public class CondVar {
    
    public static CondVar condvar = null;
    
    public static synchronized CondVar getCondVar(){
        if( condvar == null)
                condvar = new CondVar();
        
        return condvar;
    }
    
    private CondVar(){};
}
