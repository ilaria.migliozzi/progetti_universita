import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import javax.servlet.*;
import java.io.*;
import javax.servlet.http.*;
import java.sql.*;

public class ApriInEditor extends HttpServlet{

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        Connection conn = null;
        String idUtente = null;
        String username = null;
        PreparedStatement prst = null;
        ResultSet rs = null;
        
        String idDocumento = null;
        String versione = null;
        String working_folder = "";
        String corpo = "";
        
        /*Controllo che la sessione sia ancora attiva ed allora recupero lo userid dell'utente,
        *in caso contrario l'utente viene reindirizzato alla pagina di login */  
        HttpSession session = req.getSession();
        idUtente = (String)session.getAttribute("id");
        username = (String)session.getAttribute("username");
        if(idUtente == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }
        
        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return; 
        }
                 
        /* Estraggo id del documento inviato dalla tabella repository e cerco tale documento nel DB */
        idDocumento = (String)req.getParameter("iddoc");
        versione = (String)req.getParameter("versione");
        working_folder = (String) req.getParameter("working_folder");
        
        /* Controllo in caso in cui la servlet venga richiamata direttamente */
        if(idDocumento == null || versione == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non puoi richiamare questa pagina direttamente.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         } 
         
        
        /* Contollare che il documento esiste ancora e che l'utente sia effettivamanete proprietario del id documento*/
        try{
               /* Controllo se il documento esiste ancora */
              prst = conn.prepareStatement( "SELECT * FROM `documento` WHERE IdDocumento = ?" );
              prst.setString(1 ,idDocumento);
              rs = prst.executeQuery();
             
             if(!rs.next()){/* Il documento non esiste più*/
                    req.setAttribute("msg","Il documento richiesto è stato cancellato");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                     /* Rilascio la connessione del pool*/
                    try{
                        ConnectionPool.getConnectionPool().releaseConnection(conn);
                    } catch (ConnectionPoolException ex) {
                        req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                        req.setAttribute("time","4");
                        req.setAttribute("redirect","index.jsp");
                        dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(req,res);
                return; 
                    }
                    return; 
             }
            
              prst = conn.prepareStatement("SELECT * FROM ewriter E WHERE E.IdAutore = ? AND E.IdDocumento = ?");
              prst.setString(1 , idUtente);
              prst.setString(2 , idDocumento);
              rs = prst.executeQuery();
              if(!rs.next()){
                    req.setAttribute("msg","Non sei writer di questo documento");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                     /* Rilascio la connessione del pool*/
                    try{
                        ConnectionPool.getConnectionPool().releaseConnection(conn);
                    } catch (ConnectionPoolException ex) {
                     req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                     req.setAttribute("time","4");
                     req.setAttribute("redirect","index.jsp");
                     dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                     dispatcher.forward(req,res);
                    return; 
                    }
                    return; 
              }
            
        }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }  
        
        
        
         
         try{
             
              /* Altrimenti Recupero i blocchi associati al documento */
              prst = conn.prepareStatement( "SELECT D.IdDefinitivo, D.Testo, A.IdAutore, A.Username, B.Def_Pre, B.Def_Post "
                                                            + "FROM `Definitivo` D join `bloccodoc` B join `autore` A "
                                                            + "WHERE D.IdDefinitivo = B.IdDefinitivo AND "
                                                            + "D.Scritto_da = A.IdAutore AND "
                                                            + "B.IdDocumento= ? AND B.Versione= ?");
              prst.setString(1 ,idDocumento);
              prst.setString(2 ,versione);
              rs = prst.executeQuery();
                
              if(rs.next()){/* Il documento è presente e ed ho recuperato tutti i suoi blocchi*/
                
                  Map<Integer,Blocco> mappa = new HashMap<Integer,Blocco>();
                  Blocco primo = null;
                  do{
                      /* Prendo il primo e lo metto da parte (quello che ha precedente = NULL)*/
                      if(rs.getString("Def_Pre") == null ){ 
                          primo = new Blocco(rs.getInt("IdDefinitivo"),rs.getString("Testo"), rs.getInt("IdAutore"), rs.getString("Username"), rs.getInt("Def_Pre"), rs.getInt("Def_Post"));
                          continue; 
                      }
                      
                      /* Inserisco gli altri in una Map*/
                      mappa.put(rs.getInt("IdDefinitivo"),new Blocco(rs.getInt("IdDefinitivo"),rs.getString("Testo"), rs.getInt("IdAutore"), rs.getString("Username"), rs.getInt("Def_Pre"), rs.getInt("Def_Post")));
                  
                  }while(rs.next());
                  
                 
                   int id;
                   
                 
                    do{       
                        corpo = corpo + "<div id=\""+ primo.getIdDefinitivo()+ "\" title=\""+primo.getUsername()+"\" data-id_autore=\""+ primo.getIdAutore() +"\" data-def_pre=\""+primo.getDef_Pre()+"\" data-def_post=\""+primo.getDef_Post()+"\" onkeydown=\"if(event.keyCode==13 || event.keyCode==8 || event.keyCode==38 || event.keyCode==40) keypressed()\">"+primo.getTesto()+"</div>";
                        id = primo.getDef_Post();
                        primo = mappa.get(id);
                    }while(primo != null);
                  
                  
                
              }else{// Il Documento è vuoto
                    
                    corpo = "<div id=\"new_block0\" title=\""+username+"\" data-type=\"added\" data-id_autore=\""+idUtente+"\" ></div>";
              }
                System.out.println("CORPO:"+working_folder);
                /* Invio i blocchi recuperati all'editor */
                 if(working_folder==null){
                     System.out.println("if wf=null");
                     req.setAttribute("corpo",corpo);
                 }else{
                    System.out.println("else wf != null");
                    req.setAttribute("corpo",working_folder);
                    req.setAttribute("openinwf",corpo);
                 }
                  req.setAttribute("id_document",idDocumento);
                  req.setAttribute("version",versione);
                  RequestDispatcher dispatcher = req.getRequestDispatcher("Editor.jsp");
                  dispatcher.forward(req,res);
                  
              
              
            }catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }          
                        

         /* Rilascio la connessione del pool*/
          try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }         
    }
}