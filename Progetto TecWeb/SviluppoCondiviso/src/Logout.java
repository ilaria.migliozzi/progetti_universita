import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Logout extends HttpServlet{
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        HttpSession session = req.getSession(false);
        if(session != null)
            req.getSession(false).invalidate();
        req.setAttribute("msg","Logout effettuato!");
        req.setAttribute("time","2");
        req.setAttribute("redirect","index.jsp");
        RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
        dispatcher.forward(req,res);
    } 
}
