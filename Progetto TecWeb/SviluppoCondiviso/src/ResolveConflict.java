import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class ResolveConflict extends HttpServlet{
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        PreparedStatement prst = null;
        Connection conn = null;
        ResultSet rs = null;
        
        /* Recupero i parametri */
        String id_originale= req.getParameter("iddef");
        String testo = req.getParameter("testo");
        String titolo = req.getParameter("titolo");
        String id_documento = req.getParameter("iddocumento");
        
        HttpSession session = req.getSession();
        String idUtente = (String)session.getAttribute("id");
        if(idUtente == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }

        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
                req.setAttribute("msg","Si è verifica un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        
        try{
            /* Eseguo la query, poi passo il controllo a ResolveConflict.jsp che stampa *
             * il risultatao della query in una tabella*/
            prst = conn.prepareStatement( "Select * FROM temporaneo T JOIN autore A WHERE T.Scritto_da = A.IdAutore AND IdDefinitivo=?");
            prst.setString(1,id_originale);
            rs = prst.executeQuery();
            
            /*Richiamo la ResolveConflict.jsp */
            req.setAttribute("rs",rs); /* temporanei */
            req.setAttribute("id_definitivo",id_originale);
            req.setAttribute("def_testo",testo);
            req.setAttribute("id_documento",id_documento);
            RequestDispatcher dispatcher = req.getRequestDispatcher("ResolveConflict.jsp");
            dispatcher.forward(req,res);
        
        }catch(SQLException ex){
                req.setAttribute("msg","Si è verifica un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        /*Rilascio la connessione nel pool*/
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        }catch (ConnectionPoolException ex){
                req.setAttribute("msg","Si è verifica un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
    }
}
