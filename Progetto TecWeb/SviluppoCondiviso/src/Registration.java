import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class Registration extends HttpServlet{

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            Connection conn = null;
            String username;
            String password;
            
            
            /*Ottengo una connessione libera dal pool di connesione*/
            try {
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }
            
            
            
            username = req.getParameter("Username");
            password = req.getParameter("Password");
            
            try{
                PreparedStatement prst = conn.prepareStatement("INSERT INTO `autore`(`Username`, `Password`) VALUES (?,?)");
                prst.setString(1 , username);
                prst.setString(2 , password);
                prst.executeUpdate();
                
           /*redirect alla pagina di login. */
                res.sendRedirect(res.encodeRedirectURL("Login.html"));

            }catch(com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException e){
                   req.setAttribute("msg","Ops! Un utente ha occupato lo username che avevi scelto prima di te...");
                   req.setAttribute("time","2");
                   req.setAttribute("redirect","Registration.html");
                   RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                   dispatcher.forward(req,res);               
            }
            catch(SQLException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }          
        
       /* Rilascio il pull di connessione */     
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
            }
        
    }
}
