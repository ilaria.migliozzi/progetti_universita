import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class VersionCheck extends HttpServlet{

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
            Connection conn = null;
            String id_document;
            PrintWriter out;           
            
            /*Ottengo una connessione libera dal pool di connesione*/
            try {
                conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(VersionCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            /* estraggo l'id del documento di cui bisogna controllare la versione */
            id_document = req.getParameter("idDoc");

            try{
                PreparedStatement prst = conn.prepareStatement("SELECT MAX(`Versione`) AS ver FROM `documento` WHERE IdDocumento=?");
                prst.setString(1 , id_document);
                ResultSet rs = prst.executeQuery();
                
                res.setContentType("text/xml");
                out=res.getWriter();

                /* se ho ottenuto un risiltato invio il numero della versione corrente*/
                if(rs.next()){ 
                   out.print(rs.getString("ver"));
                }
                else{/* altrimenti invio una stringa ad indicare che la query non è riuscita!*/
                   out.print("ko");
                }
             /*Chiudo lo stream d'uscita*/
                out.close();
                   
                
            }catch(SQLException ex){
                Logger.getLogger(VersionCheck.class.getName()).log(Level.SEVERE, null, ex); 
            }          
                        
            try{
                ConnectionPool.getConnectionPool().releaseConnection(conn);
            } catch (ConnectionPoolException ex) {
                Logger.getLogger(VersionCheck.class.getName()).log(Level.SEVERE, null, ex);; 
            }
        
    }
}
