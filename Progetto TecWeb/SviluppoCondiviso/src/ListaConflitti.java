import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;

public class ListaConflitti extends HttpServlet{
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
 
        PreparedStatement prst = null;
        Connection conn = null;
        ResultSet rs = null;
        
        HttpSession session = req.getSession();
        String idUtente = (String)session.getAttribute("id");
        if(idUtente == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }

        /*Ottengo una connessione libera dal pool di connesione*/
        try{
             conn=(Connection) ConnectionPool.getConnectionPool().getConnection();
        } catch (ConnectionPoolException ex){
                req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                req.setAttribute("time","4");
                req.setAttribute("redirect","index.jsp");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
                return; 
        }
        
        
        
        try{
            /* Eseguo la query, poi passo il controllo a ListaConflitti.jsp che stampa *
             * il risultatao della query in una tabella*/
            prst = conn.prepareStatement( "Select DISTINCT doc.Titolo,t.IdDefinitivo,d.Testo AS TestoOriginario, doc.IdDocumento FROM temporaneo t left join definitivo d ON t.IdDefinitivo=d.IdDefinitivo left join bloccodoc b ON t.IdDefinitivo=b.IdDefinitivo left join documento doc ON b.IdDocumento=doc.IdDocumento left join autore a ON t.Scritto_da=a.IdAutore where d.Scritto_da=?");
            prst.setString(1,idUtente);
            rs = prst.executeQuery();
            
            /*Richiamo la ListaConflitti.jsp */
            req.setAttribute("rs",rs);
            RequestDispatcher dispatcher = req.getRequestDispatcher("ListaConflitti.jsp");
            dispatcher.forward(req,res);
        
        }catch(SQLException ex){
             req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
             req.setAttribute("time","4");
             req.setAttribute("redirect","index.jsp");
             RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
             dispatcher.forward(req,res);
             return; 
        }
        
        
        /*Rilascio la connessione nel pool*/
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        }catch (ConnectionPoolException ex){
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }       
    }
}
