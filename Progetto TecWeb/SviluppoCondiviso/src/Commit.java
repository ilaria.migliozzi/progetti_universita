import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import java.util.*;
import javax.servlet.http.*;
import java.io.*;
import java.sql.*;
import org.jsoup.select.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
        
public class Commit extends HttpServlet{
    	
    /*Questo metodo è sincronized perchè ci sono letture e scritture in DB che potrebbero gerenerare race condiction*/
    @Override
    public synchronized void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
        
        synchronized(CondVar.getCondVar()){
            
        
        
        java.sql.Connection conn = null;
        PreparedStatement prst;
        ResultSet rs;
        Element el = null;
        Map<String,String> map = new HashMap<String,String>();
        
        String idAutore = (String)req.getSession().getAttribute("id");
        String deleted_para = (String) req.getParameter("deleted");
        String added_para = (String)req.getParameter("added");
        String modified_para= (String)req.getParameter("modified");
        String id_document = (String)req.getParameter("id_document");
        String version = (String)req.getParameter("version");
        String rel = (String)req.getParameter("relationship");
        String nextVersion = ((Integer)(Integer.parseInt(version)+1)).toString();
        
        /* Controlla che la sessione non sia scaduta */
        if(idAutore == null){
                   /*Nel caso in cui la sessione è scaduta mostro un messaggio di errore e reindirizzo alla pagina
                    *di login.*/
                    req.setAttribute("msg","Spiacente, non risulti più loggato, rieffetua il log-in.");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","Login.html");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }
         
         /* Controlla che la servlet non venga richiamata direttamente */
         if(deleted_para == null || added_para == null || modified_para == null || id_document == null || version == null || rel == null){
                    req.setAttribute("msg","Spiacente non puoi accedere direttamente a questa risorsa");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                    return;
         }

        /* Recupero una connessione libera dal Pool */
        try{
            conn=(java.sql.Connection) ConnectionPool.getConnectionPool().getConnection();
        }catch (ConnectionPoolException ex) {
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }
        
        try{
            
            /* Controllo che il documento esista ancora */
            prst = conn.prepareStatement( "SELECT D.IdDocumento FROM `documento` D WHERE IdDocumento = ? Group By IdDocumento");
            prst.setString(1,id_document);
            rs = prst.executeQuery();
            if(!rs.next()){ /* il docimento non esiste più */
                req.setAttribute("msg","Il documento richiesto è stato cancellato");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                     /* Rilascio la connessione del pool*/
                    try{
                        ConnectionPool.getConnectionPool().releaseConnection(conn);
                    } catch (ConnectionPoolException ex) {
                        req.setAttribute("msg","Si è verifica un problema di comunicazione con il DB Server. Riprovare in seguito");
                        req.setAttribute("time","4");
                        req.setAttribute("redirect","index.jsp");
                        dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(req,res);
                return; 
                    }
                    return; 
            }
            
            /* Controllo che l'autore abbia effettivamente diritti di modifica sul documento  */
            prst = conn.prepareStatement( "SELECT * FROM `ewriter` WHERE IdDocumento = ? AND IdAutore = ?");
            prst.setString(1,id_document);
            prst.setString(2,idAutore);
            rs = prst.executeQuery();
            if(!rs.next()){ /* l'autore non ha diritti di modifica del documento */
                    req.setAttribute("msg","Non hai diritti di modifica sul documento");
                    req.setAttribute("time","4");
                    req.setAttribute("redirect","index.jsp");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                    dispatcher.forward(req,res);
                     /* Rilascio la connessione del pool*/
                    try{
                        ConnectionPool.getConnectionPool().releaseConnection(conn);
                    } catch (ConnectionPoolException ex) {
                        req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
                        req.setAttribute("time","4");
                        req.setAttribute("redirect","index.jsp");
                        dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(req,res);
                        return; 
                    }
                    return; 
            }
 
            /* Verifico se la versione inviata dal client è obsoleta */
            prst = conn.prepareStatement( "SELECT MAX(D.Versione) as Versione FROM documento D WHERE D.IdDocumento = ?");
            prst.setString(1,id_document);
            rs = prst.executeQuery();
            rs.next();
            
            if(rs.getInt("Versione") > Integer.parseInt(version)){
                /* La versione è obsoleta */
                req.setAttribute("msg","Si è tentanto di effettuare un commit di una versione obsoleta! "
                                     + "Per tanto il commit è stato rifiutato. Si prega di recuperare l'ultima versione del documento");
                req.setAttribute("time","5");
                req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
                RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
                dispatcher.forward(req,res);
            
            }else{
                /* La versione è la stessa */
                System.out.println("#ARRIVATI DALL'EDITOR:");
                System.out.println("ID doc:"+id_document+"-\n");
                System.out.println("V:"+version+"-\n\n");
                System.out.println("Added:"+added_para+"\n\n");
                System.out.println("Modified:"+modified_para+"\n\n");
                System.out.println("Deleted:"+deleted_para+"\n\n");
                System.out.println("Rel:"+rel+"\n\n");
                
                 /*Effettuo il parsing dell'codice HTML dei nodi modificati*/
                Document modified_doc = Jsoup.parse(modified_para);
                /*Effettuo il parsing dell'codice HTML dei nodi cancellati*/
                Document deleted_doc = Jsoup.parse(deleted_para);
                
                /* Creo una nuova versione solo quando necessario */
                System.out.println(!(added_para.equals("<body></body>")));
                System.out.println(!(modified_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()));
                System.out.println(!(deleted_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()));
                if( !(added_para.equals("<body></body>")) || !(modified_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()) || !(deleted_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()) ){
 
                    this.AvanzaVersione(conn,id_document,version);
                }
/******************************************Gestione dei nodi aggiunti ***************************************************/
                 System.out.println("\n\n#GESTIONE BLOCCHI AGGIUNTI:");
                 if(!added_para.equals("<body></body>")){
                    /*Effettuo il parsing dell'codice HTML dei nodi aggiunti*/
                    Document added_doc = Jsoup.parse(added_para);
            
                    /*Estraggo il primo blocco aggiunto*/
                    el = added_doc.body().child(0);
                    prst = conn.prepareStatement("SELECT inserimentoInBloccoDoc(?,?,?,?,?,?) AS LastKey");
                    
                    while(el!=null){
                            prst.setString(1,el.html());
                            prst.setString(2,idAutore);
                            prst.setString(3,id_document);
                            prst.setString(4,nextVersion);
                    
                            if(el.attr("data-def_pre").equals("0"))
                                prst.setNull(5,Types.INTEGER);
                            else
                                prst.setString(5,el.attr("data-def_pre"));
                    
                            if(el.attr("data-def_post").equals("0"))
                                prst.setNull(6,Types.INTEGER);
                            else
                                prst.setString(6,el.attr("data-def_post"));
                    
                            rs = prst.executeQuery();
                            System.out.println("CALL inserimentoInBloccoDoc("+el.html()+","+idAutore+","+id_document+","+nextVersion+","+el.attr("data-def_pre")+","+el.attr("data-def_post"));
                            rs.next();
                            map.put(el.attr("id"),rs.getString("LastKey"));
                            el = el.nextElementSibling();
                      }
                   
                  }
                
                 if( !(added_para.equals("<body></body>")) || !(modified_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()) || !(deleted_doc.getElementsByAttributeValue("data-is_conflict","false").isEmpty()) ){
                    this.AggiornaPosizione(conn,rel,id_document,nextVersion, map);
                 }
                
/*******************************     GESTIONE BLOCCHI MODIFICATI    ***********************************************/
                System.out.println("\n\n#GESTIONE BLOCCHI MODIFICATI:");
                  /*Gestione dei nodi modificati */
                 if(!modified_para.equals("<body></body>")){
                       PreparedStatement prst_1;
                       PreparedStatement prst_2;
                      
                       /*Estraggo il primo blocco cancellato*/
                       el = modified_doc.body().child(0);
                       
                       prst_1 = conn.prepareStatement("INSERT INTO `temporaneo`(`Testo`, `DataCreazione`, `Scritto_da`, `IdDefinitivo`) VALUES (?,NOW(),?,?)");
                       prst_2 = conn.prepareStatement("CALL modificaBlocco(?,?,?,?,?,?,?)");
                       
                        while(el!=null){
                            
                            if(el.attr("data-id_autore").equals(idAutore)){/*Sono proprietario del blocco*/
                                
                                prst_2.setString(1,el.attr("id"));
                                prst_2.setString(2,el.html());
                                prst_2.setString(3,id_document);
                                prst_2.setString(4,version);
                                prst_2.setString(5,idAutore);
                                if(el.attr("data-def_pre").equals("0"))
                                    prst_2.setNull(6,Types.INTEGER);
                                else
                                    prst_2.setString(6,el.attr("data-def_pre"));
                                
                                 if(el.attr("data-def_post").equals("0"))
                                    prst_2.setNull(7,Types.INTEGER);
                                 else
                                    prst_2.setString(7,el.attr("data-def_post"));
                            
                                prst_2.executeQuery();
                                System.out.println("CALL modificaBlocco("+el.attr("id")+","+el.html()+","+id_document+","+version+","+idAutore+","+el.attr("data-def_pre")+","+el.attr("data-def_post")+")");

                            }else{/*CONFLITTO*/
                                prst_1.setString(1,el.html());
                                prst_1.setString(2,idAutore);
                                prst_1.setString(3,el.attr("id"));
                                prst_1.executeUpdate();
                                System.out.println("INSERT INTO TEMPORANEO (`Testo`, `DataCreazione`, `Scritto_da`, `IdDefinitivo`) VALUES("+el.html()+",NOW(),"+idAutore+","+el.attr("id")+")");
                            }
                            
                            el=el.nextElementSibling();
                        }
            }

            
/*********************************** Gestione dei nodi cancellati******************************************************/
                 System.out.println("\n\n#GESTIONE BLOCCHI CONCELLATI:"); 
                 if(!deleted_para.equals("<body></body>")){ //Crea conflitto

                       Elements not_conflict;
                       Elements conflict;
                       
                       PreparedStatement prst_1;
                       PreparedStatement prst_2;
                       PreparedStatement prst_3;
                       PreparedStatement prst_4;
                       
                        /*Estraggo il primo blocco cancellato*/
                        not_conflict = deleted_doc.body().getElementsByAttributeValue("data-is_conflict","false");
                        conflict = deleted_doc.body().getElementsByAttributeValue("data-is_conflict","true");
                        
                        prst_1 = conn.prepareStatement("INSERT INTO `temporaneo`(`Testo`, `DataCreazione`, `Scritto_da`, `IdDefinitivo`) VALUES (?,NOW(),?,?)");
                        prst_2 = conn.prepareStatement("DELETE FROM `bloccodoc` WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                        prst_3 = conn.prepareStatement("UPDATE `modifiche` SET `TipoModifica`=\"CANCELLATO\",`VersioneGenerata`=?,`AutoreCancellatore`=? WHERE IdDefinitivo = ? AND IdDocumento = ? AND Versione = ?");
                        prst_4 = conn.prepareStatement("CALL eliminaBlocco(?,?,?)");

                        for(Element elm:not_conflict){
                            
                            /*Sono proprietario del blocco*/
                                
                                prst_4.setString(1,id_document);
                                prst_4.setString(2,nextVersion);
                                prst_4.setString(3,elm.attr("id"));
                                
                                
                                prst_2.setString(1,elm.attr("id"));
                                prst_2.setString(2,id_document);
                                prst_2.setString(3,nextVersion);
                                
                                
                                prst_3.setString(1,version);
                                prst_3.setString(2,idAutore);
                                prst_3.setString(3,elm.attr("id"));
                                prst_3.setString(4,id_document);
                                prst_3.setString(5,nextVersion);
                                
                                
                                prst_4.executeQuery();
                                prst_2.executeUpdate();
                                prst_3.executeUpdate();
                                
                                System.out.println("CALL eliminaBlocco("+id_document+","+nextVersion+","+elm.attr("id") +")");
                                System.out.println("DELETE FROM bloccodoc WHERE IdDefinitivo = "+elm.attr("id")+" AND IdDocumento = "+id_document+" AND Versione = "+version);
                                System.out.println("UPDATE modifiche SET `TipoModifica`=CANCELLATO,`VersioneGenerata`="+version+",`AutoreCancellatore`="+idAutore+" WHERE IdDefinitivo = "+elm.attr("id")+" AND IdDocumento = "+id_document+" AND Versione = "+version);
                                
                                /* Elimino eventuali conflitti sospesi su quel blocco */
                                prst = conn.prepareStatement("DELETE FROM Temporaneo WHERE IdDefinitivo = ?");
                                prst.setString(1,elm.attr("id"));
                                prst.executeUpdate();
                                System.out.println("DELETE FROM Temporaneo WHERE IdDefinitivo =" + elm.attr("id"));
                        }
                        
                        
                         for(Element elem:conflict){/*CONFLITTO*/ 
                            prst_1.setNull(1, Types.VARCHAR);
                            prst_1.setString(2,idAutore);
                            prst_1.setString(3,elem.attr("id"));
                            System.out.println("INSERT INTO TEMPORANEO (`Testo`, `DataCreazione`, `Scritto_da`, `IdDefinitivo`) VALUES(NULL"+",NOW(),"+idAutore+","+elem.attr("id")+")");
                            prst_1.executeUpdate();
                        }    
                  }
                    
            
            req.setAttribute("msg","Commit avvenuto con successo!");
            req.setAttribute("time","2");
            req.setAttribute("redirect","/SviluppoCondiviso/Documenti");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);    
        
         }
        }catch(SQLException ex){
            req.setAttribute("msg","Si è verifica un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }
        
        /*Rilascio la connessione nel pool*/
        try{
            ConnectionPool.getConnectionPool().releaseConnection(conn);
        }catch (ConnectionPoolException ex){
            req.setAttribute("msg","Si è verificato un problema di comunicazione con il DB Server. Riprovare in seguito");
            req.setAttribute("time","4");
            req.setAttribute("redirect","index.jsp");
            RequestDispatcher dispatcher = req.getRequestDispatcher("ErrorPage.jsp");
            dispatcher.forward(req,res);
            return; 
        }
             
        System.out.println("Connessione rilasciata");
    }
    }
    
    /* Creo una nuova versione del documento */
    private void AvanzaVersione(java.sql.Connection conn, String id_document, String version){
                    try{
                        PreparedStatement prst = conn.prepareStatement("CALL inserimentoDoc(?,?)");
                        prst.setString(1,id_document);
                        prst.setString(2,version);
                        prst.executeQuery();
                        System.out.println("CALL inserimentoDoc("+id_document+","+version+")");
                
                
                        /* Duplica tutti i blocchi doc e tutte le modfiche */
                        prst = conn.prepareStatement("CALL duplicaDoc(?,?)");
                        prst.setString(1,id_document);
                        prst.setString(2,version);
                        System.out.println("CALL duplicaDoc("+id_document+","+version+")");
                        prst.executeQuery();
                    
                    }catch(SQLException ex){
                        Logger.getLogger(Commit.class.getName()).log(Level.SEVERE, null, ex);
                    }
    }
    
    /* Aggiorno i pre e i post dei blocchi */
    private void AggiornaPosizione(java.sql.Connection conn,String rel,String id_document, String nextVersion, Map<String,String> map){
                System.out.println("\n\n#AGGIORNAMENTO PRE E POST:");
                PreparedStatement prst=null;
                
                
                Document rel_doc = Jsoup.parse(rel);
                Element el = rel_doc.body().child(0);
                try{
                    prst = conn.prepareStatement("UPDATE bloccodoc SET Def_Pre = ?, Def_Post = ? WHERE IdDocumento = ? AND Versione = ? AND IdDefinitivo = ?");
                    while(el!=null){
                        prst.setString(3,id_document);
                        prst.setString(4,nextVersion);
                        prst.setString(5,el.attr("id"));
                    
                        if(el.attr("data-def_pre").equals("0")){ 
                            prst.setNull(1,Types.INTEGER);
                            System.out.println("Pre Settanto NULL");
                        }
                        else{
                            if(el.attr("data-def_pre").contains("new_block")){
                                prst.setString(1,map.get(el.attr("data-def_pre")));
                            System.out.println("Pre="+map.get(el.attr("id")));
                            }
                            else{
                                prst.setString(1,el.attr("data-def_pre"));
                                System.out.println("Pre="+el.attr("data-def_pre"));
                            }
                        }
                    
                    
                        if(el.attr("data-def_post").equals("0")){
                            prst.setNull(2,Types.INTEGER);
                            System.out.println("Post=NULL");
                        }
                        else{
                            if(el.attr("data-def_post").contains("new_block")){
                                prst.setString(2,map.get(el.attr("data-def_post")));
                                System.out.println("Post="+ map.get(el.attr("id")));
                            }
                            else{
                                prst.setString(2,el.attr("data-def_post"));
                                System.out.println("Post="+el.attr("data-def_post"));
                            }
                        }
                    
                        prst.executeUpdate();
                        System.out.println("UPDATE bloccodoc SET Pre e Post WHERE IdDocumento = "+id_document+" AND Versione = "+nextVersion+" AND IdDefinito ="+el.attr("id"));
                        el = el.nextElementSibling();
                    }
                }catch(SQLException ex){
                    Logger.getLogger(Commit.class.getName()).log(Level.SEVERE, null, ex);
                }
    
    }
}