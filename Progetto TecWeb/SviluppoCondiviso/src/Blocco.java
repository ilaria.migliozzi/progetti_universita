

public class Blocco {
	
	private int IdDefinitivo;
	private String Testo;
	private int IdAutore;
	private String Username;
	private int Def_Pre;
	private int Def_Post;
	
	
	public Blocco(int IdDef, String T, int IdAut, String User, int Pre, int Post){
		this.IdDefinitivo=IdDef;
		this.Testo=T;
		this.IdAutore=IdAut;
		this.Username=User;
		this.Def_Pre=Pre;
		this.Def_Post=Post;
	}
        
        public int getIdDefinitivo(){
            return IdDefinitivo;
        }
	
        public String getTesto(){
            return Testo;
        }
        
	public int getIdAutore(){
            return IdAutore;
        }
        
	public String getUsername(){
            return Username;
        }
        
	public int getDef_Pre(){
            return Def_Pre;
        }
        
	public int getDef_Post(){
            return Def_Post;
        }
}