<!DOCTYPE html>
<html>
    <head>
    	<title> Pagine Evento </title>
    	<meta http-equiv="refresh" content="<%= request.getAttribute("time") %>;URL=<%= response.encodeURL((String)request.getAttribute("redirect")) %>"/>
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
    </head>
    <body>
        <div id="container" >
            <%@include file="elem/Header.html" %>
            <div id="content">
                <div class="fcenter">
                <p><strong>Si &egrave verificato il segguente evento:</strong></p>
                <p><span class="important"> <%= request.getAttribute( "msg" ) %></span></p>
                <p><strong>Attendi, verrai ridirezionato automaticamente...</strong></p>	
                </div>
            </div>
            <%@include file="elem/Footer.html" %>
        </div>
    </body>
 </html>