<%@page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,projection" />
	<title>Sviluppo Condiviso - Lista Conflitti</title>
        
        <% 
              String id_aut = (String)session.getAttribute("id");
              ResultSet rs = (ResultSet)request.getAttribute("rs");
                    
              if(id_aut == null){ 
                        request.setAttribute("msg","Spiacente, la tue sessione � scaduta, rieffetua il log-in.");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","Login.html");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }
              if(rs == null){ 
                        request.setAttribute("msg","Spiacente, non puoi accedere direttamente a questa pagina");
                        request.setAttribute("time","4");
                        request.setAttribute("redirect","index.jsp");
                        RequestDispatcher dispatcher = request.getRequestDispatcher("ErrorPage.jsp");
                        dispatcher.forward(request,response);
                        return;
              }             
                %>
                <script>
                    function Apri(){
                      var id= event.target.getAttribute("data-id_input");
                      var id_doc= event.target.getAttribute("data-iddoc");
                      var version= event.target.getAttribute("data-versione_rif");
                      var wf=document.getElementById(id).innerHTML;
                      document.getElementById("working_folder").setAttribute("value",wf);
                      document.getElementById("iddoc").value=id_doc;
                      document.getElementById("versione").value=version;
                      document.getElementById("form").submit();  
                    }
                </script>
</head>
<body>
<div id="container" >
	<%@include file="elem/Header.html" %>
	 <jsp:include page="elem/NavigationBar.jsp">
            <jsp:param name="selected" value="WorkingFolder" />
         </jsp:include>
	  <div id="content">
              <%
                if(!rs.next()){%>
                <p>La tua working folder &egrave; vuota</p>
                <%}else{
                %>
		
            <table cellspacing='0'> <!-- cellspacing='0' � importante, deve rimanere-->
                    <tr><th>Titolo</th><th>Versione</th><th>Operazioni</th></tr> <!-- Table Header -->
                    <%
                    int i = 1;       
                    do{
                        if(i%2 == 0){%>
                            <tr>
                    <% }else{ %>
                            <tr class='even'>
                    <% } %>        
                        <td><%= rs.getString("Titolo") %></td><td hidden><%= rs.getString("IdDocumentoRif") %></td><td><%= rs.getString("VersioneRif") %></td><td id="<%=i%>+a" hidden><%= rs.getString("innerHTML") %></td><td><input type="button" value="Apri" data-id_input="<%=i%>+a" data-iddoc="<%= rs.getString("IdDocumentoRif") %>" data-versione_rif="<%= rs.getString("VersioneRif") %>" onclick="Apri()"/></td></tr>
                     <%
                        i++;
                    }while(rs.next()); %>                   
</table>     
<% } %>
            <form method="post" id="form" action="<%= response.encodeURL("/SviluppoCondiviso/ApriInEditor")%>" hidden>
                <input id="working_folder" name="working_folder" hidden />
                <input id="iddoc" name="iddoc" hidden/>
                <input id="versione" name="versione" hidden/>
            </form>
	</div>
	<%@include file="elem/Footer.html" %>
</div>
</body>
</html>