-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Gen 22, 2015 alle 15:54
-- Versione del server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `controlloversione`
--

DELIMITER $$
--
-- Procedure
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `duplicaDoc`(IN `IdDoc` INT, IN `Vers` INT)
    DETERMINISTIC
Begin
DECLARE a INT(4);
DECLARE b INT(4);
DECLARE c INT(4);
DECLARE d varchar(50);
DECLARE e INT(4);
DECLARE l INT(4);
DECLARE m INT(4);
DECLARE cnt INT(4);
DECLARE i int(50) default 0;
DECLARE j int(4) default 0;
DECLARE done int default 0;
DECLARE verss int;
DECLARE cur1 CURSOR FOR 
SELECT B.IdDefinitivo,B.IdDocumento,B.Versione,M.TipoModifica,M.VersioneGenerata,M.HaSovrascritto,M.AutoreCancellatore from bloccodoc B join modifiche M where B.IdDocumento=M.IdDocumento AND B.IdDefinitivo=M.IdDefinitivo AND B.Versione=M.Versione AND B.IdDocumento=IdDoc AND B.Versione=Vers;
SELECT COUNT(*) FROM bloccodoc WHERE IdDocumento=IdDoc AND Versione=Vers INTO cnt;
OPEN cur1;
ciclo: loop
  if(i < cnt) then
  fetch cur1 INTO a,b,c,d,e,l,m;
  	INSERT INTO `bloccodoc`(`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES (a,b,c+1,null,null);
    INSERT INTO `modifiche`(`IdDocumento`,`Versione`, `IdDefinitivo`,     `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES(b,c+1,a,d,e,l,m);
    set i=i+1;
   else
   leave ciclo;
  end if;
end loop ciclo;
 CLOSE cur1;
End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `duplicaDocModifica`(IN `IdDoc` INT, IN `Vers` INT)
    DETERMINISTIC
Begin
DECLARE a INT(10);
DECLARE b INT(10);
DECLARE c INT(10);
DECLARE d varchar(50);
DECLARE e INT(10);
DECLARE f INT(10);
DECLARE g INT(10);
DECLARE l INT(10);
DECLARE m INT(10);
DECLARE cnt INT(4);
DECLARE i int(50) default 0;
DECLARE j int(4) default 0;
DECLARE done int default 0;
DECLARE verss int;
DECLARE cur1 CURSOR FOR 
SELECT B.IdDefinitivo,B.IdDocumento,B.Versione,M.TipoModifica,M.VersioneGenerata,B.Def_Pre,B.Def_Post,M.HaSovrascritto,M.AutoreCancellatore from bloccodoc B join modifiche M where B.IdDocumento=M.IdDocumento AND B.IdDefinitivo=M.IdDefinitivo AND B.Versione=M.Versione AND B.IdDocumento=IdDoc AND B.Versione=Vers;
SELECT COUNT(*) FROM bloccodoc WHERE IdDocumento=IdDoc AND Versione=Vers INTO cnt;
OPEN cur1;
ciclo: loop
  if(i < cnt) then
  fetch cur1 INTO a,b,c,d,e,f,g,l,m;
  	INSERT INTO `bloccodoc`(`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES (a,b,c+1,f,g);
    INSERT INTO `modifiche`(`IdDocumento`,`Versione`, `IdDefinitivo`,     `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES(b,c+1,a,d,e,l,m);
    set i=i+1;
   else
   leave ciclo;
  end if;
end loop ciclo;
 CLOSE cur1;
End$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `duplicaDocRipristino`(IN `IdDoc` INT, IN `VersOld` INT)
Begin
DECLARE a INT(10);
DECLARE b INT(10);
DECLARE c INT(10);
DECLARE d varchar(50);
DECLARE e INT(10);
DECLARE f INT(10);
DECLARE g INT(10);
DECLARE l INT(10);
DECLARE m INT(10);
DECLARE vers INT(10);
DECLARE cnt INT(4);
DECLARE i int(50) default 0;
DECLARE j int(4) default 0;
DECLARE done int default 0;
DECLARE verss int;
DECLARE cur1 CURSOR FOR 
SELECT B.IdDefinitivo,B.IdDocumento,B.Versione,M.TipoModifica,M.VersioneGenerata, B.Def_Pre,B.Def_Post,M.HaSovrascritto,M.AutoreCancellatore from bloccodoc B join modifiche M where B.IdDocumento=M.IdDocumento AND B.IdDefinitivo=M.IdDefinitivo AND B.Versione=M.Versione AND B.IdDocumento=IdDoc AND B.Versione=VersOld;
SELECT COUNT(*) FROM bloccodoc WHERE IdDocumento=IdDoc AND Versione=VersOld INTO cnt;

SELECT MAX(Versione) FROM Documento WHERE IdDocumento=IdDoc INTO vers;
call inserimentoDoc(IdDoc,vers);
OPEN cur1;
ciclo: loop
  if(i < cnt) then
  fetch cur1 INTO a,b,c,d,e,f,g,l,m;
  	INSERT INTO `bloccodoc`(`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES (a,b,vers+1,f,g);
    INSERT INTO `modifiche`(`IdDocumento`,`Versione`, `IdDefinitivo`,     `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES(b,vers+1,a,d,e,l,m);
    set i=i+1;
   else
   leave ciclo;
  end if;
end loop ciclo;
 CLOSE cur1;
End$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminaBlocco`(IN `idDoc` INT, IN `Vers` INT, IN `idDef` INT)
    NO SQL
BEGIN
DECLARE a int(10);
DECLARE b int(10);

Select Def_Pre INTO a From bloccodoc Where IdDocumento=idDoc AND Versione=Vers AND IdDefinitivo=idDef;
Select Def_Post INTO b From bloccodoc Where IdDocumento=idDoc AND Versione=Vers AND IdDefinitivo=idDef;

UPDATE bloccodoc SET Def_Post=b WHERE IdDocumento=idDoc AND Versione=Vers AND IdDefinitivo=a;
UPDATE bloccodoc SET Def_Pre=a WHERE IdDocumento=idDoc AND Versione=Vers AND IdDefinitivo=b;

End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `inserimentoDoc`(IN `IdDoc` INT, IN `Vers` INT)
    NO SQL
Begin
DECLARE a varchar(50);
DECLARE b INT(4);
DECLARE cur1 CURSOR FOR 
SELECT Titolo,Creato_da from documento where IdDocumento=IdDoc AND Versione=Vers;
OPEN cur1;
  fetch cur1 INTO a,b;
  	INSERT INTO `documento`(`IdDocumento`, `Versione`, `Titolo`, `DataCreazione`, `Creato_da`) VALUES (IdDoc,Vers+1,a,now(),b);
 CLOSE cur1;
End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `modificaBlocco`(IN `BloccoSovr` INT(10), IN `testo` VARCHAR(1000), IN `idDoc` INT(10), IN `vers` INT(10), IN `autore` INT(10), IN `Pre` INT(10), IN `Post` INT(10))
    NO SQL
begin
declare maxdef int(20);
INSERT INTO `definitivo`(`Testo`, `DataCreazione`, `Scritto_da`) VALUES (testo,now(),autore);
SELECT MAX(`IdDefinitivo`)INTO maxdef FROM `definitivo` WHERE 1;
INSERT INTO `bloccodoc`(`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES (maxdef,idDoc,vers+1,Pre,Post);
DELETE FROM `bloccodoc` WHERE IdDefinitivo=BloccoSovr AND Versione= vers+1;
if(Pre is NOT NULL) then
UPDATE `bloccodoc` SET `Def_Post`=maxdef WHERE IdDefinitivo=Pre AND IdDocumento=idDoc AND Versione=vers+1;
end if;
if(Post is NOT NULL) then
UPDATE `bloccodoc` SET `Def_Pre`=maxdef WHERE IdDefinitivo=Post AND IdDocumento=idDoc AND Versione=vers+1;
end if;
INSERT INTO `modifiche`(`IdDocumento`, `Versione`, `IdDefinitivo`, `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES (idDoc,vers+1,maxdef,"RISCRITTO",vers+1,BloccoSovr,null);
DELETE FROM `modifiche` WHERE IdDefinitivo=BloccoSovr AND Versione=vers+1;
End$$

--
-- Funzioni
--
CREATE DEFINER=`root`@`localhost` FUNCTION `inserimentoInBloccoDoc`(`Testo` TEXT, `Autore` INT, `IdDoc` INT, `Vers` INT, `Def_Pre` INT, `Def_Post` INT) RETURNS int(11)
    NO SQL
Begin
declare massimo int;
declare rit int;

INSERT INTO `definitivo`(`Testo`, `DataCreazione`, `Scritto_da`) VALUES (Testo,now(),Autore);

select max(IdDefinitivo) INTO massimo from definitivo where 1;

INSERT INTO `bloccodoc`(`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES (massimo,IdDoc,Vers,Def_Pre,Def_Post);

INSERT INTO `modifiche`(`IdDocumento`, `Versione`, `IdDefinitivo`, `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES (IdDoc,Vers,massimo,"aggiunto",Vers,null,null);

Select Max(IdDefinitivo) into rit From definitivo Where 1; 

return rit;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `autore`
--

CREATE TABLE IF NOT EXISTS `autore` (
`IdAutore` int(11) NOT NULL,
  `Username` varchar(15) NOT NULL,
  `Password` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `autore`
--

INSERT INTO `autore` (`IdAutore`, `Username`, `Password`) VALUES
(1, 'Luca', 'e30deadb80f9832622fd58f1def4e3a9'),
(2, 'Mirko', 'e30deadb80f9832622fd58f1def4e3a9'),
(3, 'Ilaria', 'e30deadb80f9832622fd58f1def4e3a9');

-- --------------------------------------------------------

--
-- Struttura della tabella `avviso`
--

CREATE TABLE IF NOT EXISTS `avviso` (
`IdAvviso` int(11) NOT NULL,
  `Testo` text,
  `Visualizzato` varchar(10) DEFAULT NULL,
  `Destinatario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `bloccodoc`
--

CREATE TABLE IF NOT EXISTS `bloccodoc` (
`IdDefinitivo` int(11) NOT NULL,
  `IdDocumento` int(11) NOT NULL,
  `Versione` int(11) NOT NULL,
  `Def_Pre` int(11) DEFAULT NULL,
  `Def_Post` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `bloccodoc`
--

INSERT INTO `bloccodoc` (`IdDefinitivo`, `IdDocumento`, `Versione`, `Def_Pre`, `Def_Post`) VALUES
(1, 1, 1, NULL, NULL),
(1, 1, 2, NULL, 2),
(1, 1, 3, NULL, 2),
(2, 1, 2, 1, NULL),
(2, 1, 3, 1, 3),
(3, 1, 3, 2, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `definitivo`
--

CREATE TABLE IF NOT EXISTS `definitivo` (
`IdDefinitivo` int(11) NOT NULL,
  `Testo` text,
  `DataCreazione` datetime NOT NULL,
  `Scritto_da` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `definitivo`
--

INSERT INTO `definitivo` (`IdDefinitivo`, `Testo`, `DataCreazione`, `Scritto_da`) VALUES
(1, 'Nel mezzo del cammin', '2015-01-18 00:00:00', 2),
(2, 'di nostra vita', '2015-01-19 00:00:00', 1),
(3, 'mi ritrovai in una selva oscura', '2015-01-19 09:00:00', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
  `IdDocumento` int(11) NOT NULL,
  `Versione` int(11) NOT NULL,
  `Titolo` varchar(50) NOT NULL,
  `DataCreazione` datetime NOT NULL,
  `Creato_da` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `documento`
--

INSERT INTO `documento` (`IdDocumento`, `Versione`, `Titolo`, `DataCreazione`, `Creato_da`) VALUES
(1, 0, 'Divina Commedia', '2015-01-17 00:00:00', 2),
(1, 1, 'Divina Commedia', '2015-01-18 00:00:00', 2),
(1, 2, 'Divina Commedia', '2015-01-19 00:00:00', 2),
(1, 3, 'Divina Commedia', '2015-01-19 10:00:00', 2);

--
-- Trigger `documento`
--
DELIMITER //
CREATE TRIGGER `InserimentoAutomaticoWritersCreatore` AFTER INSERT ON `documento`
 FOR EACH ROW Begin
declare esiste integer(2);
SELECT COUNT(*) into esiste
FROM `ewriter`
WHERE ewriter.IdAutore = NEW.Creato_da and ewriter.IdDocumento = New.IdDocumento;
if esiste = 0 then
INSERT INTO `ewriter`(`IdDocumento`, `IdAutore`) VALUES (NEW.IdDocumento,NEW.Creato_da);
end if;
End
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `ewriter`
--

CREATE TABLE IF NOT EXISTS `ewriter` (
  `IdDocumento` int(11) NOT NULL,
  `IdAutore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ewriter`
--

INSERT INTO `ewriter` (`IdDocumento`, `IdAutore`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `modifiche`
--

CREATE TABLE IF NOT EXISTS `modifiche` (
  `IdDocumento` int(11) NOT NULL,
  `Versione` int(11) NOT NULL,
  `IdDefinitivo` int(11) NOT NULL,
  `TipoModifica` enum('AGGIUNTO','RISCRITTO','CANCELLATO','CONFLITTO_RISOLTO','INVARIATO') NOT NULL,
  `VersioneGenerata` int(11) NOT NULL,
  `HaSovrascritto` int(11) DEFAULT NULL,
  `AutoreCancellatore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `modifiche`
--

INSERT INTO `modifiche` (`IdDocumento`, `Versione`, `IdDefinitivo`, `TipoModifica`, `VersioneGenerata`, `HaSovrascritto`, `AutoreCancellatore`) VALUES
(1, 1, 1, 'AGGIUNTO', 1, NULL, NULL),
(1, 2, 1, 'AGGIUNTO', 1, NULL, NULL),
(1, 2, 2, 'AGGIUNTO', 2, NULL, NULL),
(1, 3, 1, 'AGGIUNTO', 1, NULL, NULL),
(1, 3, 2, 'AGGIUNTO', 2, NULL, NULL),
(1, 3, 3, 'AGGIUNTO', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `temporaneo`
--

CREATE TABLE IF NOT EXISTS `temporaneo` (
`IdTemporaneo` int(11) NOT NULL,
  `Testo` text,
  `DataCreazione` date NOT NULL,
  `Scritto_da` int(11) NOT NULL,
  `IdDefinitivo` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `workingfolder`
--

CREATE TABLE IF NOT EXISTS `workingfolder` (
  `Proprietario` int(11) NOT NULL,
  `IdDocumentoRif` int(11) NOT NULL,
  `VersioneRif` int(11) NOT NULL,
  `innerHTML` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autore`
--
ALTER TABLE `autore`
 ADD PRIMARY KEY (`IdAutore`), ADD UNIQUE KEY `Username` (`Username`);

--
-- Indexes for table `avviso`
--
ALTER TABLE `avviso`
 ADD PRIMARY KEY (`IdAvviso`), ADD KEY `Destinatario` (`Destinatario`);

--
-- Indexes for table `bloccodoc`
--
ALTER TABLE `bloccodoc`
 ADD PRIMARY KEY (`IdDefinitivo`,`IdDocumento`,`Versione`), ADD KEY `IdDocumento` (`IdDocumento`,`Versione`), ADD KEY `Def_Pre` (`Def_Pre`), ADD KEY `Del_Post` (`Def_Post`);

--
-- Indexes for table `definitivo`
--
ALTER TABLE `definitivo`
 ADD PRIMARY KEY (`IdDefinitivo`), ADD KEY `Scritto_da` (`Scritto_da`);

--
-- Indexes for table `documento`
--
ALTER TABLE `documento`
 ADD PRIMARY KEY (`IdDocumento`,`Versione`), ADD KEY `Creato_da` (`Creato_da`);

--
-- Indexes for table `ewriter`
--
ALTER TABLE `ewriter`
 ADD PRIMARY KEY (`IdDocumento`,`IdAutore`), ADD KEY `IdAutore` (`IdAutore`), ADD KEY `IdDocumento` (`IdDocumento`), ADD KEY `IdAutore_2` (`IdAutore`);

--
-- Indexes for table `modifiche`
--
ALTER TABLE `modifiche`
 ADD PRIMARY KEY (`IdDocumento`,`Versione`,`IdDefinitivo`), ADD KEY `IdDocumento` (`IdDocumento`), ADD KEY `Versione` (`Versione`), ADD KEY `IdDefinitivo` (`IdDefinitivo`), ADD KEY `HaSovrascritto` (`HaSovrascritto`), ADD KEY `AutoreCancellatore` (`AutoreCancellatore`);

--
-- Indexes for table `temporaneo`
--
ALTER TABLE `temporaneo`
 ADD PRIMARY KEY (`IdTemporaneo`), ADD KEY `IdDefinitivo` (`IdDefinitivo`), ADD KEY `Scritto_da` (`Scritto_da`);

--
-- Indexes for table `workingfolder`
--
ALTER TABLE `workingfolder`
 ADD PRIMARY KEY (`Proprietario`,`IdDocumentoRif`,`VersioneRif`), ADD KEY `Proprietario` (`Proprietario`), ADD KEY `IdDocumentoRif` (`IdDocumentoRif`), ADD KEY `VersioneRif` (`VersioneRif`), ADD KEY `IdDocumentoRif_2` (`IdDocumentoRif`,`VersioneRif`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autore`
--
ALTER TABLE `autore`
MODIFY `IdAutore` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `avviso`
--
ALTER TABLE `avviso`
MODIFY `IdAvviso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bloccodoc`
--
ALTER TABLE `bloccodoc`
MODIFY `IdDefinitivo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `definitivo`
--
ALTER TABLE `definitivo`
MODIFY `IdDefinitivo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `temporaneo`
--
ALTER TABLE `temporaneo`
MODIFY `IdTemporaneo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `documento`
--
ALTER TABLE `documento`
ADD CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`Creato_da`) REFERENCES `autore` (`IdAutore`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `ewriter`
--
ALTER TABLE `ewriter`
ADD CONSTRAINT `ewriter_ibfk_1` FOREIGN KEY (`IdAutore`) REFERENCES `autore` (`IdAutore`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `ewriter_ibfk_2` FOREIGN KEY (`IdDocumento`) REFERENCES `documento` (`IdDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `modifiche`
--
ALTER TABLE `modifiche`
ADD CONSTRAINT `modifiche_ibfk_1` FOREIGN KEY (`IdDefinitivo`) REFERENCES `definitivo` (`IdDefinitivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `modifiche_ibfk_2` FOREIGN KEY (`IdDocumento`, `Versione`) REFERENCES `documento` (`IdDocumento`, `Versione`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `modifiche_ibfk_3` FOREIGN KEY (`HaSovrascritto`) REFERENCES `definitivo` (`IdDefinitivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `modifiche_ibfk_4` FOREIGN KEY (`AutoreCancellatore`) REFERENCES `autore` (`IdAutore`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `temporaneo`
--
ALTER TABLE `temporaneo`
ADD CONSTRAINT `temporaneo_ibfk_1` FOREIGN KEY (`Scritto_da`) REFERENCES `autore` (`IdAutore`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `temporaneo_ibfk_2` FOREIGN KEY (`IdDefinitivo`) REFERENCES `definitivo` (`IdDefinitivo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `workingfolder`
--
ALTER TABLE `workingfolder`
ADD CONSTRAINT `workingfolder_ibfk_1` FOREIGN KEY (`Proprietario`) REFERENCES `autore` (`IdAutore`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `workingfolder_ibfk_2` FOREIGN KEY (`IdDocumentoRif`, `VersioneRif`) REFERENCES `documento` (`IdDocumento`, `Versione`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
